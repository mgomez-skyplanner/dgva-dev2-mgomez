## Description

This is the private source repository for **DGVA International Bakery** Salesforce organization.

## Contents

This source repository includes the following:

* Apex Classes
* Apex Components
* Standard and Custom Objects
* Apex Pages
* Static Resources
* Apex Triggers
* Web Links (Custom Buttons)

## Setting up the repo

To use this repository please follow the instructions below:

* Fork the repo
* Clone the created fork with Source Tree

## Contributors

This source repository was created by SkyPlanner LLC (http://www.theskyplanner.com)
