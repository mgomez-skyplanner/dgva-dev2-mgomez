<!--
Provides a confirmation template so clients
can confirm order by email.
@author Fernando Gomez, SkyPlanner LLC
@date May 9th, 2017
@version
    1.0.0 Initial version.
-->
<messaging:emailTemplate subject="Please, confirm your order with Protano's Bakery" 
    relatedToType="Order">
<messaging:htmlEmailBody >
<div style="background-color: #fff;
        border: none;
        font-family: Arial, Helvetica, sans-serif;
        color: #333;
        padding: 20px;
        font-size: 13px;
        max-width: 600px;">
    <p style="text-align: center;">
        <apex:image width="234px" height="106px" 
            alt="Protano's Bakery Logo"
            value="{! $Setup.OrderConfirmationSettings__c.OrdersSiteUrl__c 
            }/servlet/servlet.ImageServer?id={! 
            $Setup.OrderConfirmationSettings__c.LogoDocumentId__c }&oid={! 
            $Organization.Id }" />
    </p>
    <h1 style="text-align: center;
            text-transform: uppercase;">
        <apex:outputText value="{! $Label.OrderConfirmationEmailTitle }" />
    </h1>
    <p style="margin-bottom: 30px;">
        <apex:outputText value="{!
            $Label.OrderConfirmationEmailGreeting }" /><br/><br/>
        <apex:outputText value="{!
            $Label.OrderConfirmationEmailContent }" />
    </p>
    
    <p style="margin-bottom: 30px;">
        <b>Order No.: </b><apex:outputText value="{!relatedTo.OrderNumber }" /><br/>
        <b>Delivery Date: </b><apex:outputText value="{0,date,MM/dd/yyyy}">
                                   <apex:param value="{! relatedTo.EndDate }" />
                               </apex:outputText>
        <br/>
        <apex:outputText rendered="{!relatedTo.PoNumber != null}">
        <b>PO Number: </b><apex:outputText value="{!relatedTo.PoNumber }" /><br/>
        </apex:outputText>
    </p>
    <table style="width: 100%;
            border-spacing: 0;
            font-size: 14px;
            margin-bottom: 40px;">
        <thead>
            <th style="width: 40%;
                    border: 1px solid #aaa;
                    padding: 5px 10px;
                    background-color: #b22020;
                    color: #fff;">
                <apex:outputText value="{!
                    $Label.OrderConfirmationEmailProductHeader }" />            
            </th>
            <th style="width: 20%;
                    border-top: 1px solid #aaa;
                    border-right: 1px solid #aaa;
                    border-bottom: 1px solid #aaa;
                    padding: 5px 10px;
                    background-color: #b22020;
                    color: #fff;">
                <apex:outputText value="{!
                    $Label.OrderConfirmationEmailQuantityHeader }" />           
            </th>
            <th style="width: 20%;
                    border-top: 1px solid #aaa;
                    border-right: 1px solid #aaa;
                    border-bottom: 1px solid #aaa;
                    padding: 5px 10px;
                    background-color: #b22020;
                    color: #fff;">
                <apex:outputText value="{!
                    $Label.OrderConfirmationEmailPackageHeader }" />
            </th>   
            <th style="width: 20%;
                    border-top: 1px solid #aaa;
                    border-right: 1px solid #aaa;
                    border-bottom: 1px solid #aaa;
                    padding: 5px 10px;
                    background-color: #b22020;
                    color: #fff;">
                <apex:outputText value="{!
                    $Label.OrderConfirmationEmailTotalHeader }" />
            </th>
        </thead>
        <tbody>
        <apex:repeat var="item" value="{! relatedTo.OrderItems }">
            <tr>
                <td style="border-bottom: 1px solid #aaa;
                        border-left: 1px solid #aaa;
                        border-right: 1px solid #aaa;
                        padding: 5px 10px;
                        background-color: #eee;">
                    <apex:outputField value="{! item.Product2.Name }" />
                </td>
                <td style="border-bottom: 1px solid #aaa;
                        border-right: 1px solid #aaa;
                        padding: 5px 10px;
                        text-align: right;">
                    <apex:outputField value="{! item.Quantity }" />
                </td>
                <td style="border-bottom: 1px solid #aaa;
                        border-right: 1px solid #aaa;
                        padding: 5px 10px;
                        text-align: right;">
                    <apex:outputField value="{! item.Product2.Units_Pk__c }" />
                </td>   
                <td style="border-bottom: 1px solid #aaa;
                        border-right: 1px solid #aaa;
                        padding: 5px 10px;
                        text-align: right;">
                    <apex:outputText value="{0, Number, Currency}">
                        <apex:param value="{! item.Quantity * item.UnitPrice }" />
                    </apex:outputText>
                </td>
            </tr>
        </apex:repeat>
            <tr>
                <td colspan="3"
                    style="border-bottom: 1px solid #aaa;
                        border-left: 1px solid #aaa;
                        border-right: 1px solid #aaa;
                        padding: 5px 10px;
                        text-align: right;">
                    <b><apex:outputText value="{!
                        $Label.OrderConfirmationEmailTotalHeader }" /></b>
                </td>
                <td style="border-bottom: 1px solid #aaa;
                        border-right: 1px solid #aaa;
                        padding: 5px 10px;
                        text-align: right;">
                    <b><apex:outputField value="{! relatedTo.Order_Total__c }" /></b>
                </td>
            </tr>
        </tbody>
    </table>
    <p style="text-align: center;
            margin-bottom: 30px;">
        <apex:outputLink target="_blank"
            style="background-color: #b22020;
                padding: 20px 40px;
                color: #fff;
                font-size: 16px;
                display: inline-block;
                text-decoration: none;
                border-radius: 5px;"
            value="{! $Setup.OrderConfirmationSettings__c.OrdersSiteUrl__c  
                }/apex/OrderConfirmation?order={! relatedTo.Id }">
            <b><apex:outputText value="{!
                    $Label.OrderConfirmationEmailLink }"/></b>
        </apex:outputLink>
    </p>
    <p style="margin-bottom: 30px;">
        <apex:outputText value="{!
            $Label.OrderConfirmationEmailFarewell }"/><br/>
        <b><apex:outputText value="{!
            $Label.OrderConfirmationEmailSignature }"/></b>
    </p>
</div>  
</messaging:htmlEmailBody>
</messaging:emailTemplate>