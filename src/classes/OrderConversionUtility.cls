/**
* @author    	 - Marcel Gomez
* @date  	     - 06/07/2017
* @description 	 - This class manage the sync process from an Order object to a Sales Order object (Accounting Seed Package)
*/

public with sharing class OrderConversionUtility {

	private static List<Order_Conversion_Fields__c> orderConversionFields_List;
	private static List<Order_Product_Conversion_Fields__c> orderProductConversionFields_List;
	private static Map<String, String> orderStatusConversionValues_Map;

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Load values of custom settings Order_Conversion_Fields__c, Order_Product_Conversion_Fields__c and Order_Status_Conversion_Values__c
	*/
	@TestVisible
	private static void LoadCustomSettings() {
		if(orderConversionFields_List == NULL) {
			orderConversionFields_List = Order_Conversion_Fields__c.getall().values();
		}

		if(orderProductConversionFields_List == NULL) {
			orderProductConversionFields_List = Order_Product_Conversion_Fields__c.getall().values();
		}

		if(orderStatusConversionValues_Map == NULL) {
			orderStatusConversionValues_Map = new Map<String, String>();

			List<Order_Status_Conversion_Values__c> orderStatusConversionValues = Order_Status_Conversion_Values__c.getall().values();

			for(Order_Status_Conversion_Values__c orderStatus : orderStatusConversionValues) {
				orderStatusConversionValues_Map.put(orderStatus.Name, orderStatus.Sales_Order_Status__c);
			}
		}
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Loads the Order with all fields involved in sync
	* @param       	 - The Order to be reloaded
	*/
	@TestVisible
	private static Order ReloadOrder(Order order) {
		if(orderConversionFields_List == NULL || orderConversionFields_List.size() == 0)
			LoadCustomSettings();

		//Build the query with Sales Order fields involved in the conversion process
		String query = 'SELECT Id, Sales_Order__c, ';

		for(Order_Conversion_Fields__c field : orderConversionFields_List) {
			query += field.Name + ', ';
		}

		//Include the Sales Order Line fields
		query += ' (SELECT Id, PriceBookEntry.Product2Id, PriceBookEntryId, ';
		for(Order_Product_Conversion_Fields__c field : orderProductConversionFields_List) {
			query += field.Name + ', ';
		}

		if(query.endsWith(', '))
			query = query.removeEnd(', ');
		
		query += ' FROM OrderItems) ';
		query += ' FROM Order WHERE Id = \'' + order.Id + '\'';

		system.debug('------------------------------------ Reload Order query: ' + query);

		List<Order> result = Database.query(query);

		//if an Order was found, then return it
		if(result.size() > 0) {
			system.debug('------------------------------------ Reloaded Order: ' + result[0]);

			return result[0];
		}

		//if a Sales Order was NOT found, then create a new one
		return order;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Syncs an Order instance to a Sales Order instance (Accounting Seed Package)
	* @param       	 - The Order instance to be converted. If the Order already has a reference to a Sales Order, then that Sales Order is updated, otherwise a Sales Order is created.
	* @return        - An instance of AcctSeedERP__Sales_Order__c created based on the parameter Order.
	*/
	public static AcctSeedERP__Sales_Order__c Sync_Order_SalesOrder(Order order) {
		system.debug('------------------------------------ SYNCING ORDER WITH SALES ORDER....');
		system.debug(order);

		LoadCustomSettings();

		Boolean salesOrderIsNew = false;

		//if Order Items or the reference to the Sales Order are not loaded on the Order,
		//then reload Order with all involved fields and the related Order Items
		if(order.OrderItems == NULL || order.OrderItems.size() == 0 || order.Sales_Order__c == NULL) {
			order = ReloadOrder(order);
		}

		//load the Sales Order from Database if Order already has a reference to one Sales Order
		//Marcel Gomez - 09/13/2017
		//always remove the existing Sales Order in order to generate a new one
		if(order.Sales_Order__c != NULL) {
			deleteSalesOrder(order);
		}

		AcctSeedERP__Sales_Order__c salesOrder = new AcctSeedERP__Sales_Order__c();
		salesOrderIsNew = true;

		//Sync the Order with the Sales Order
		salesOrder = CopyOrderFields(order, salesOrder);
		salesOrder.Order__c = order.Id;

		upsert salesOrder;

		system.debug('-----------------**********--------------- Sales Order synced: ' + salesOrder);

		//update reference from Sales Order to Order
		//using a wrapper Order instance to avoid System.FinalException
		Order orderWrapper = new Order();
		orderWrapper.Id = order.Id;
		orderWrapper.Sales_Order__c = salesOrder.Id;
		update orderWrapper;

		Sync_OrderItems_SalesOrderLines(order, salesOrder);

		system.debug('-----------------**********--------------- Sales Order INSERTED!!! ' + salesOrder);

		system.debug('------------------------------------ FINISHED SYNCING ORDER WITH SALES ORDER....');

		return salesOrder;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Syncs an OrderItems of an Order instance with the corresponding Sales Order Lines of a Sales Order instance (Accounting Seed Package)
	* @param       	 - The Order with Order Items to be synced
	* @param       	 - The Sales Order with Sales Order Lines to be synced
	*/
	public static void Sync_OrderItems_SalesOrderLines(Order order, AcctSeedERP__Sales_Order__c salesOrder) {

		//Sync the Order Items with the Sales Order Lines
		Map<Id, Decimal> product_Quantity = getOrderProductsWithQuantityGT0_Map(order);

		List<AcctSeedERP__Sales_Order_Line__c> salesOrderLines_toInsert = new List<AcctSeedERP__Sales_Order_Line__c>();

		for(OrderItem orderItem : order.OrderItems) {
			if(orderItem.Quantity > 0) {
				String productId = orderItem.PricebookEntry.Product2Id;

				AcctSeedERP__Sales_Order_Line__c newSalesOrderLine = new AcctSeedERP__Sales_Order_Line__c();

				newSalesOrderLine = CopyOrderProductFields(orderItem, newSalesOrderLine);

				newSalesOrderLine.AcctSeedERP__Quantity_Ordered__c = product_Quantity.get(productId);
				newSalesOrderLine.AcctSeedERP__Sales_Order__c = salesOrder.Id;

				salesOrderLines_toInsert.add(newSalesOrderLine);
			}
		}

		system.debug('------------------------------ Sales Order Lines to be INSERTED: ');
		system.debug(salesOrderLines_toInsert);

		if(salesOrderLines_toInsert.size() > 0) {
			insert salesOrderLines_toInsert;
		}
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/13/2017
	* @description 	 - Creates a Map with every Product and the total Quantity minus voided Quantity
	* @param       	 - The Order to extract Order Items
	* @return        - A Map where:
	* 					Key = ProductId
	* 					Value = Quantity not voided
	*/
	private static Map<Id, Decimal> getOrderProductsWithQuantityGT0_Map(Order order) {
		Map<Id, Decimal> product_Quantity = new Map<Id, Decimal>();

		//calculate the remaining of each product after summing up negative quantities
		for(OrderItem oi : order.OrderItems) {
			Decimal quantity = oi.Quantity;
			String productId = oi.PricebookEntry.Product2Id;

			if(product_Quantity.containsKey(productId)) {
				quantity = product_Quantity.get(productId) + oi.Quantity;
			}
			
			product_Quantity.put(productId, quantity);
		}

		return product_Quantity;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Copy fields from the Order to the related Sales Order
	* @param       	 - The value of field Order.Status
	* @return        - The corresponding value of field AcctSeedERP__Sales_Order__c.AcctSeedERP__Status__c (SalesOrder.Status)
	*/
	private static AcctSeedERP__Sales_Order__c CopyOrderFields(Order order, AcctSeedERP__Sales_Order__c salesOrder) {
		system.debug('---------------------------- BEFORE Sales Order: ' + salesOrder);

		for(Order_Conversion_Fields__c field : orderConversionFields_List) {
			String orderField = field.Name;
			String salesOrderField = field.Sales_Order_Field__c;

			if(orderField == 'Status') {				
				salesOrder.AcctSeedERP__Status__c = ConvertOrderStatus_To_SalesOrderStatus(order.Status);
			}
			else {
				system.debug('---------------------------- copying field: SalesOrder.' + salesOrderField + ' = Order.' + orderField + ' (' + order.get(orderField) + ')');
				salesOrder.put(salesOrderField, order.get(orderField));
			}
		}

		system.debug('---------------------------- AFTER Sales Order: ' + salesOrder);

		return salesOrder;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Copy fields from the Order Product to the related Sales Order Line
	* @param       	 - The value of field Order.Status
	* @return        - The corresponding value of field AcctSeedERP__Sales_Order__c.AcctSeedERP__Status__c (SalesOrder.Status)
	*/
	public static AcctSeedERP__Sales_Order_Line__c CopyOrderProductFields(OrderItem orderProduct, AcctSeedERP__Sales_Order_Line__c salesOrderLine) {
		system.debug('---------------------------- BEFORE Sales Order Line: ' + salesOrderLine);

		for(Order_Product_Conversion_Fields__c field : orderProductConversionFields_List) {
			String orderProductField 	= field.Name;
			String salesOrderLineField 	= field.Sales_Order_Line_Field__c;
			
			salesOrderLine.put(salesOrderLineField, orderProduct.get(orderProductField));
		}
		
		//set the Product on the Sales Order Line if changed on the Order Item
		PriceBookEntry pbe = [SELECT 
								Id,
								Product2Id
							FROM
								PriceBookEntry
							WHERE
								Id =: orderProduct.PriceBookEntryId];

		salesOrderLine.AcctSeedERP__Product__c = pbe.Product2Id;

		system.debug('---------------------------- AFTER Sales Order Line: ' + salesOrderLine);

		return salesOrderLine;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/13/2017
	* @description 	 - Delete a Sales Order, if exists, and all its references
	* @param       	 - The Order related to the Sales Order
	*/
	public static void deleteSalesOrder(Order order) {
		delete [SELECT Id FROM AcctSeedERP__Sales_Order__c WHERE Order__c =: order.Id];
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Converts the value of field Order.Status to a value for field AcctSeedERP__Sales_Order__c.AcctSeedERP__Status__c (SalesOrder.Status)
	* @param       	 - The value of field Order.Status
	* @return        - The corresponding value of field AcctSeedERP__Sales_Order__c.AcctSeedERP__Status__c (SalesOrder.Status)
	*/
	@TestVisible
	private static String ConvertOrderStatus_To_SalesOrderStatus(String orderStatus) {
		if(orderStatusConversionValues_Map.containsKey(orderStatus))
			return orderStatusConversionValues_Map.get(orderStatus);

		return 'Open';
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/27/2017
	* @description 	 - If Order.Status = 'Delivered', then sets the Order.RecordType = 'Locked'
	* @param       	 - The Order
	*/
	public static void LockOrder(Order order) {
		if(order == NULL)
			return;

		if(order.Status == 'Delivered') {
			//avoid System.FinalException
			Order newOrder = new Order();
			newOrder.Id = order.Id;
    		newOrder.RecordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Locked').getRecordTypeId();

    		try {
    			update newOrder;

				system.debug('------------------------------------ Order locked successfully: ' + newOrder);
    		}
			catch (Exception e) {
				system.debug('-----------------**********--------------- Error attempting to change Order Record Type to "Locked"!!!: ' + e.getMessage());
			}
		}
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/27/2017
	* @description 	 - Emulates removing a Product from an Activated Order by adding a new Order Item of the same Product with Quantity = -1 * OrderItem.Quantity
	* @param       	 - The OrderItem to be "removed" from the Order
	*/
	public static void VoidOrderProduct(Order order, List<OrderProductVoid_Wrapper> orderItemsToVoid) {
		List<OrderItem> negativeOrderItems = new List<OrderItem>();

		String previousStatus = '';

		system.debug('--------------- Order:');
		system.debug(order);

		if(order.Status == 'Activated' || order.Status == 'Delivered') {
			previousStatus = order.Status;

			//set Order.Status = Draft to add new Order Items
			order.Status = 'Draft';
			TriggerControlClass.stopOrderTriggers = true;
			update order;
			TriggerControlClass.stopOrderTriggers = false;
		}

		system.debug('--------------- Order:');
		system.debug(order);

		//create the Order Items with negative Quantity
		for(OrderProductVoid_Wrapper opWrapper : orderItemsToVoid) {
			OrderItem orderItem 	= opWrapper.OrderProduct;
			
			if(orderItem != NULL && orderItem.PriceBookEntryId != NULL){

				OrderItem cloneOrderItem = new OrderItem(OrderId 			= order.Id,
				                                         PricebookEntryId 	= orderItem.PricebookEntryId,
				                                         UnitPrice 			= orderItem.UnitPrice,
				                                         Quantity 			= -1 * opWrapper.QuantityToVoid,
				                                         Void_Explanation__c = opWrapper.Explanation);

				negativeOrderItems.add(cloneOrderItem);
			}
		}

		system.debug('--------------- negativeOrderItems:');
		system.debug(negativeOrderItems);

		if(negativeOrderItems.size() > 0) {
			insert negativeOrderItems;
		}

		Boolean updateOrder = false;
		if(!String.isBlank(previousStatus)) {
			order.Status = previousStatus;
			updateOrder = true;
		}
		
		if(updateOrder) {
			TriggerControlClass.stopOrderTriggers = true;
	  		TriggerControlClass.stopOrderItemTriggers = true;
			
			order.Status = 'Prepared';
			order.IgnoreMinimumAmount__c = true;
			update order;

			//restore Order.Status = Delivered to leave the Order to its original Status
			order.Status = previousStatus;
			update order;
	  		
	  		OrderConversionUtility.Sync_Order_SalesOrder(order);
  			BillingManager.GenerateBillingFromOrder(order, true);

			TriggerControlClass.stopOrderTriggers = false;
	  		TriggerControlClass.stopOrderItemTriggers = false;
		}
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/27/2017
	* @description 	 - Indicates whether a field involved in the sync between Order and Sales Order changed, or not
	* @param       	 - The Order that changed
	* @return      	 - True: if a field involved in the sync between Order and Sales Order changed
	* 				   False: if a field involved in the sync between Order and Sales Order DO NOT changed
	*/
	public static Boolean Order_SalesOrder_SyncFieldsChanged(Order order, Order oldOrder) {
		List<Order_Conversion_Fields__c> fieldsList = Order_Conversion_Fields__c.getall().values();

		for(Order_Conversion_Fields__c field : fieldsList) {
			String orderField = field.Name;

			if(order.get(orderField) != oldOrder.get(orderField))
				return true;
		}

		return false;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/27/2017
	* @description 	 - Indicates whether a field involved in the sync between Order Product and Sales Order Line changed, or not
	* @param       	 - The Order Product that changed
	* @return      	 - True: if a field involved in the sync between Order Product and Sales Order Line changed
	* 				   False: if a field involved in the sync between Order Product and Sales Order Line DO NOT changed
	*/
	public static Boolean OrderProduct_SalesOrderLine_SyncFieldsChanged(OrderItem orderItem, OrderItem oldOrderItem) {
		List<Order_Product_Conversion_Fields__c> fieldsList = Order_Product_Conversion_Fields__c.getall().values();

		for(Order_Product_Conversion_Fields__c field : fieldsList) {
			String orderItemField = field.Name;

			if(orderItem.get(orderItemField) != oldOrderItem.get(orderItemField))
				return true;
		}

		return false;
	}

	public class OrderProductVoid_Wrapper {
		public OrderItem OrderProduct { get; set; }
		public Decimal QuantityToVoid { get; set; }
		public String Explanation { get; set; }
	}
}