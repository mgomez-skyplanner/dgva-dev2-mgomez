/**
* <p> 
* Version log:
* <ul>
* <li>1.00 07/11/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 07/11/2017
* @description 	 - Test class for class PaymentBatchController (93% - 06/29/2017)
*/
 
@isTest
public with sharing class PaymentBatchController_Test {

	@testSetup
	static void setup() {
		TestClass_Utility.createDefault_StripeSettings();
		TestClass_Utility.CreateDefaultSettingsAccountingSeed();
		TestClass_Utility.createCustomSettingsData();

		Route__c route 	= TestClass_Utility.createRoute('Test Route');
		insert route;

		Account account = TestClass_Utility.createAccount('Test Account', route);
		insert account;

		Opportunity opp = TestClass_Utility.createOpportunity(account);
		insert opp;

		Order order 			= TestClass_Utility.createOrder(account, opp, 'Draft');
		insert order;

		Product2 product1 		= TestClass_Utility.createProduct('Test Product 1');
		insert product1;

		Id standardPriceBookId = Test.getStandardPricebookId();
		PricebookEntry pbe 		= [SELECT Id FROM PricebookEntry WHERE Product2Id =: product1.Id AND PriceBook2Id =: standardPriceBookId];

		OrderItem orderProduct1 = TestClass_Utility.createOrderProduct(order, pbe);
		insert orderProduct1;

		TestClass_Utility.CreateDefaultDataForBillings(account, order, product1);
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - Test method for constructor of class PaymentBatchController
	*/
	@isTest
	static void Test_Constructor() {
		Test.startTest();

		Account account = new Account();
		ApexPages.StandardController stdController = new ApexPages.StandardController(account);

		PaymentBatchController pbc = new PaymentBatchController(stdController);

		PaymentBatchController.PaymentBatchSearchInitialValues initialValues = PaymentBatchController.LoadInitialValues();
		
		Test.stopTest();
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/07/2017
	* @description 	 - Test method for processing a payment based on a Payment_Batch__c and a Payment_Batch_Entry__c
	*/
	@isTest
	static void test_processPayments() {		
		Test.startTest();

		Order order = [SELECT Id, Status FROM Order];

		order.Status = 'Prepared';
		update order;

		//stop Order triggers and generate the Billing manually to ensure it is created when requested
		TriggerControlClass.stopOrderTriggers = true;

		order.Status = 'Activated';
		update order;
		
		TriggerControlClass.stopOrderTriggers = false;

		//reload order
		BillingManager.LoadCustomSettings();
		order = BillingManager.ReloadOrder(order.Id);

		AcctSeed__Billing__c billing = BillingManager.GenerateBillingFromOrder(order, true);

		List<PaymentBatchController.AccountRecordWrapper> aWrappers = PaymentBatchController.searchAccounts('', String.valueOf(order.EndDate) + 'T00:00:00', '', '');
		system.assertEquals(1, aWrappers.size());

		String result = PaymentBatchController.processPayments(new List<String> { billing.Id });
		system.assertEquals('1', result);

		Test.stopTest();

		Payment_Batch_Entry__c entry = [SELECT 
											Id,
											Status__c,
											Payment_Batch__c
										FROM 
											Payment_Batch_Entry__c
										WHERE
											Billing__c =: billing.Id];

		Payment_Batch__c paymentBatch = [SELECT
											Id,
											Status__c
										FROM 
											Payment_Batch__c
										WHERE
											Id =: entry.Payment_Batch__c];

		system.assertEquals('Completed', paymentBatch.Status__c);
		system.assertEquals('Success', entry.Status__c);
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/07/2017
	* @description 	 - Test method for processing a payment based on a Payment_Batch__c and a Payment_Batch_Entry__c
	*/
	@isTest
	static void test_processPaymentButton() {		
		Test.startTest();

		Order order = [SELECT Id, Status FROM Order];

		order.Status = 'Prepared';
		update order;
		order.Status = 'Activated';
		update order;

		//stop Order triggers and generate the Billing manually to ensure it is created when requested
		TriggerControlClass.stopOrderTriggers = true;

		order.Status = 'Delivered';
		update order;
		
		TriggerControlClass.stopOrderTriggers = false;

		//reload order
		BillingManager.LoadCustomSettings();
		order = BillingManager.ReloadOrder(order.Id);

		AcctSeed__Billing__c billing = BillingManager.GenerateBillingFromOrder(order, true);

		billing = [SELECT
						Id,
						AcctSeed__Customer__c,

						AcctSeed__Balance__c,
						AcctSeed__Total__c,
						AcctSeed__Received_Amount__c,
						AcctSeed__Cash_Application_Adjustment_Amount__c,
						AcctSeed__Credit_Memo_Applied_Amount__c,
						AcctSeed__Accounting_Period__c,
						(
							SELECT
								Id,
								AcctSeed__Hours_Units__c,
								AcctSeed__Rate__c
							FROM 
								AcctSeed__Project_Billing_Lines__r
						)
					FROM
						AcctSeed__Billing__c
					WHERE
						Id =: billing.Id];

		system.debug('---------------------------- Billing Balance:');
		system.debug(billing.AcctSeed__Balance__c);

		Payment_Batch__c paymentBatch = new Payment_Batch__c();
		paymentBatch.Status__c = 'Pending';
		insert paymentBatch;

		Payment_Batch_Entry__c entry = new Payment_Batch_Entry__c();
		entry.Status__c = 'Pending';
		entry.Billing__c = billing.Id;
		entry.Amount__c = billing.AcctSeed__Balance__c;
		entry.Payment_Batch__c = paymentBatch.Id;
		insert entry;

		String result = PaymentBatchController.processPaymentButton(paymentBatch.Id);
		system.assertEquals('1', result);

		Test.stopTest();

		entry = [SELECT 
					Id,
					Status__c,
					Payment_Batch__c
				FROM 
					Payment_Batch_Entry__c
				WHERE
					Billing__c =: billing.Id];

		paymentBatch = [SELECT
							Id,
							Status__c
						FROM 
							Payment_Batch__c
						WHERE
							Id =: entry.Payment_Batch__c];

		system.assertEquals('Completed', paymentBatch.Status__c);
		system.assertEquals('Success', entry.Status__c);
	}
}