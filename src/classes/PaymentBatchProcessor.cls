/**
* <p>
* Version log:
* <ul>
* <li>1.00 06/28/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 07/07/2017
* @description 	 - This class represents a batch to create a Stripe Charge of multiple Billings.
* 				   These Billings are represented by Payment Batch Entries contained in a List of Payment_Batch_Entry__c records
*/
 
global with sharing class PaymentBatchProcessor implements Database.Batchable<sObject>, Database.AllowsCallouts {

	global List<Payment_Batch_Entry__c> PaymentBatchEntries { get; private set; }

	private List<CashReceiptBillingData_Wrapper> CashReceiptsData_List;

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/07/2017
	* @description 	 - constructor: creates an instance of this class to process one Payment_Batch_Entry__c record
	*/
	global PaymentBatchProcessor(Payment_Batch_Entry__c paymentBatchEntry) {
		this.PaymentBatchEntries = new List<Payment_Batch_Entry__c> { paymentBatchEntry };
		this.CashReceiptsData_List = new List<CashReceiptBillingData_Wrapper>();
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/07/2017
	* @description 	 - constructor: creates an instance of this class to process a list of Payment_Batch__c records
	*/
	global PaymentBatchProcessor(List<Payment_Batch_Entry__c> paymentBatchEntries) {
		this.PaymentBatchEntries = paymentBatchEntries;
		this.CashReceiptsData_List = new List<CashReceiptBillingData_Wrapper>();
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/07/2017
	* @description 	 - Loads all Payment_Batch__c records passed to the constructor, and their entries, if they are pending to be processed
	*/
 	global Database.QueryLocator start(Database.BatchableContext BC) {
 		//update status of Payment Batch records to "Processing"
 		this.SetPaymentBatchesToStatus('Processing');

 		//update status of Payment Batch Entry records to "Processing"
 		this.SetPaymentBatchEntriesToProcessing();
 		
		String inSet = '';
 		for(Payment_Batch_Entry__c entry : this.PaymentBatchEntries) {
 			inSet += '\'' + entry.Id + '\',';
 		}

 		if(inSet.endsWith(','))
 			inSet = inSet.removeEnd(',');

 		return Database.getQueryLocator('SELECT ' + 
						 					'Id, ' + 
						 					'Payment_Batch__c, ' +
						 					'Amount__c, ' + 
						 					'Error_Message__c, ' + 
						 					'Status__c, ' + 
						 					'Billing__c, ' + 
						 					'Billing__r.AcctSeed__Customer__c, ' + 
						 					'Billing__r.AcctSeed__Balance__c, ' + 
						 					'Billing__r.AcctSeed__Accounting_Period__c, ' + 
						 					'Billing__r.Payment_Batch_Entry__c, ' +
						 					'Billing__r.Payment_Batch_Entry__r.Status__c ' +
										'FROM ' + 
											'Payment_Batch_Entry__c ' + 
										'WHERE ' + 
											'Id IN (' + inSet + ')');
 	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/07/2017
	* @description 	 - For each Payment_Batch_Entry__c performs a Charge on Stripe and, if success, stores data to create a Chas Receipt for each Billing charged
	*/
 	global void execute(Database.BatchableContext BC, List<Payment_Batch_Entry__c> paymentBatchEntries) {

		system.debug('---------------------------- Payment Batch Entries TO PROCESS: ' + paymentBatchEntries.size());

 		List<Payment_Batch_Entry__c> entries2Update = new List<Payment_Batch_Entry__c>();
 		List<CashReceiptBillingData_Wrapper> cashReceiptsData_List = new List<CashReceiptBillingData_Wrapper>();

 		for(Payment_Batch_Entry__c entry : paymentBatchEntries) {

			system.debug('---------------------------- ABOUT TO PROCESS Billing:');
			system.debug('---------------------------- Billing.Id = ' + entry.Billing__c);
			system.debug('---------------------------- Billing Payment Batch Entry:' + entry.Billing__r.Payment_Batch_Entry__c);
			system.debug('---------------------------- Billing Payment Batch Entry Status:' + entry.Status__c);

			//perform the Charge on Stripe
			StripeCharge charge = WebManager_Stripe.CreateCharge(entry.Billing__c);

			system.debug('------------------------ charge: ');
			system.debug(charge);

			if(charge.error != NULL) {
				entry.Status__c = 'Failed';
				entry.Error_Message__c = charge.error.stripeType + ':' + charge.error.message;
			}
			else {
				entry.Status__c = 'Success';
				entry.Payment_Reference__c = charge.id;

				//create the wrapper instance to store the Cash Receipt data for further processing
				CashReceiptBillingData_Wrapper cashReceiptData = new CashReceiptBillingData_Wrapper();

				cashReceiptData.AccountId 	= entry.Billing__r.AcctSeed__Customer__c;
				cashReceiptData.CustomerId 	= charge.customer_id;
				cashReceiptData.BillingId 	= entry.Billing__c;
				cashReceiptData.BillingAccountingPeriodId = entry.Billing__r.AcctSeed__Accounting_Period__c;
				cashReceiptData.BillingBalance = entry.Billing__r.AcctSeed__Balance__c;

				cashReceiptsData_List.add(cashReceiptData);
			}

			entries2Update.add(entry);
		}

 		update entries2Update;
 		this.PaymentBatchEntries = entries2Update;

		InsertCashReceipts(entries2Update, cashReceiptsData_List);
 	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/07/2017
	* @description 	 - Updates the status and completed datetime of Payment_Batch__c records. Also creates the corresponding Cash Receipts and Billing Cash Receipts
	*/
  	global void finish(Database.BatchableContext BC) {  		
 		//update status of Payment Batch records to "Completed"
 		this.SetPaymentBatchesToStatus('Completed');
  	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/07/2017
	* @description 	 - Creates the corresponding Cash Receipts and Billing Cash Receipts based on successfully charged Billings
	*/
  	private void InsertCashReceipts(List<Payment_Batch_Entry__c> paymentBatchEntries, List<CashReceiptBillingData_Wrapper> cashReceiptsData_List) {
  		if(cashReceiptsData_List.size() > 0) {
  			
  			//key: Billing.Id, Value: corresponding Cash_Receipt record
  			Map<Id, AcctSeed__Cash_Receipt__c> billingId_CashReceipt_Map = new Map<Id, AcctSeed__Cash_Receipt__c>();

  			//list of Cash Receipts records to insert
  			List<AcctSeed__Cash_Receipt__c> cashReceipts2Insert = new List<AcctSeed__Cash_Receipt__c>();

  			//create the Cash Receipt record
  			for(CashReceiptBillingData_Wrapper cashReceiptData : cashReceiptsData_List) {
				AcctSeed__Cash_Receipt__c cashReceipt = WebManager_Stripe.CreateCashReceipt(cashReceiptData.AccountId, cashReceiptData.CustomerId, cashReceiptData.BillingBalance);

				billingId_CashReceipt_Map.put(cashReceiptData.BillingId, cashReceipt);
			}

  			//insert the Cash Receipt records to get Ids
			insert billingId_CashReceipt_Map.values();

			system.debug('------------------------ Chas Receipts Inserted: ');
			system.debug(billingId_CashReceipt_Map.values());


  			//list of Billing Cash Receipt records to insert
			List<AcctSeed__Billing_Cash_Receipt__c> billingCashReceipts2Insert = new List<AcctSeed__Billing_Cash_Receipt__c>();

			//loop the Payment Batch Entries to get the corresponding Billing
 			for(Payment_Batch_Entry__c entry : paymentBatchEntries) {

 				//find the Billing in the Map to get the corresponding Cash Receipt
 				if(billingId_CashReceipt_Map.containsKey(entry.Billing__c)) {
 					AcctSeed__Cash_Receipt__c cashReceipt = billingId_CashReceipt_Map.get(entry.Billing__c);

 					//create the Billing Cash Receipt
					AcctSeed__Billing_Cash_Receipt__c billingCashReceipt = WebManager_Stripe.CreateBillingCashReceipt(entry.Billing__c,
					                                                                                                  entry.Billing__r.AcctSeed__Accounting_Period__c,
					                                                                                                  cashReceipt);
					billingCashReceipts2Insert.add(billingCashReceipt);
 				}
 			}

 			//insert the Billing Cash Receipt records
 			insert billingCashReceipts2Insert;

			system.debug('------------------------ Chas Receipts Billing Inserted: ');
			system.debug(billingCashReceipts2Insert);
  		}
  	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/07/2017
	* @description 	 - Set the Payment Batch status to "Processing"
	*/
  	private void SetPaymentBatchesToStatus(String status) {
  		Set<Id> paymentBatchIds = new Set<Id>();

 		for(Payment_Batch_Entry__c entry : this.PaymentBatchEntries) {
 			paymentBatchIds.add(entry.Payment_Batch__c);
 		}

		//load Payment_Batch__c records 
		List<Payment_Batch__c> parents = [SELECT 
											Status__c,
											Started_On__c,
											Completed_On__c
										FROM
											Payment_Batch__c
										WHERE
											Id IN: paymentBatchIds];

		//update Payment Batch processing fields
		for(Payment_Batch__c paymentBatch : parents) {
			if(status == 'Processing')			
				paymentBatch.Started_On__c = Datetime.now();
			else
				paymentBatch.Completed_On__c = Datetime.now();

			paymentBatch.Status__c = status;
		}
		
		update parents;
  	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/07/2017
	* @description 	 - Set the Payment Batch Entries status to "Processing"
	*/
  	private void SetPaymentBatchEntriesToProcessing() {
  		Set<Id> paymentBatchEntriesIds = new Set<Id>();

 		for(Payment_Batch_Entry__c entry : this.PaymentBatchEntries) {
 			paymentBatchEntriesIds.add(entry.Id);
 		}

		List<Payment_Batch_Entry__c> entries = [SELECT
													Id,
													Status__c
												FROM
													Payment_Batch_Entry__c
												WHERE
													Id IN: paymentBatchEntriesIds];

		for(Payment_Batch_Entry__c entry : entries) {
			entry.Status__c = 'Processing';
		}

		update entries;
  	}

  	/**
	* <p> 
	* Version log:
	* <ul>
	* <li>1.00 07/07/2017 Marcel Gomez: Initial version</li>
	* </ul>
	* <p>
	* @author	     - Marcel Gomez
	* @date  	     - 07/07/2017
	* @description 	 - This class is a wrapper to store data related to Cash Receipts and Billing Cash Receipts
	*/
  	global class CashReceiptBillingData_Wrapper {
  		public String AccountId { get; set; }
  		public String CustomerId { get; set; }

  		public String BillingId { get; set; }
  		public Decimal BillingBalance { get; set; }
  		public String BillingAccountingPeriodId { get; set; }
  	}
}