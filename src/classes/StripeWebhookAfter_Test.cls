/**
* <p> 
* Version log:
* <ul>
* <li>1.00 07/06/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 07/06/2017
* @description 	 - This class is intended to test trigger StripeWebhookAfter
*/

@isTest
private class StripeWebhookAfter_Test {
	
	@isTest static void test_method_one() {
		Stripe_Settings__c setting = new Stripe_Settings__c();
		setting.Delayed_Webhook_Processor_ID__c = '';
		setting.Is_Live_Environment__c = false;
		
		setting.Stripe_Publishable_Live_Key__c = '1';
		setting.Stripe_Secret_Live_Key__c = '1';

		setting.Stripe_Publishable_Test_Key__c = '1';
		setting.Stripe_Secret_Test_Key__c = '1';

		insert setting;

		Stripe_Webhook__c webhook = new Stripe_Webhook__c();
		webhook.Webhook_Type__c = 'charge.succeeded';
		webhook.Webhook_Data__c = 'just some data';
		webhook.Attempts__c = 0;
		webhook.Status__c = 'Pending';

		insert webhook;
	}	
}