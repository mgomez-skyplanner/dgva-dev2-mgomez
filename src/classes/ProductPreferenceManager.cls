/**
 * Handles all interaction with the Product Manager object.
 * This object relates accounts and products, specifying
 * which the preferences that an account has selected in regards
 * of that product. 
 * @author Fernando Gomez
 * @date   May 11, 2017
 * @version	1.0	
 */
public class ProductPreferenceManager {
	private Id accountId;
	private Map<Id, ProductPreference__c> preferences;

	/**
	 * Main constructor	
	 * @author Fernando Gomez
	 * @date   May 11, 2017
	 */
	public ProductPreferenceManager(Id accountId) {
		this(accountId, null);
	}

	/**
	 * Builds the manager, but only buffers 
	 * information about the specified products.
	 * @author Fernando Gomez
	 * @date May 11, 2017
	 * @param accountId
	 * @param productIds
	 */
	public ProductPreferenceManager(Id accountId, Set<Id> productIds) {
		this.accountId = accountId;
		preferences = null;
		setup(productIds);
	}

	/**
	 * Returns true if the specified product
	 * has been tagged as a favorite of the account.
	 * @author Fernando Gomez
	 * @date May 11, 2017 
	 * @param productId
	 */
	public Boolean isFavorite(Id productId) {
		ProductPreference__c p;
		// obtain the preference
		p = get(productId);
		// to be a favorite, the preference must exists,
		// and the favorite field must be checked:
		// this is because favorites are false by default.
		return p != null && p.IsFavorite__c != null && p.IsFavorite__c;
	}

	/**
	 * Sets the specified values (isFavorite) for the
	 * product, in the current account.
	 * @author Fernando Gomez
	 * @date May 11, 2017 
	 * @param productId
	 */
	public void setFavorite(Id productId, Boolean isFavorite) {		
		ProductPreference__c p;
		// obtain the preference
		p = get(productId);
		// we change only the value for the isFavorite field
		save(p, isFavorite, p == null ? true : p.ShowByDefault__c, productId);
	}

	/**
	 * Returns true if the specified product
	 * should be shown in the default product
	 * list for the account.
	 * @author Fernando Gomez
	 * @date May 11, 2017 
	 * @param productId
	 */
	public Boolean isShownByDefault(Id productId) {
		ProductPreference__c p;
		// obtain the preference
		p = get(productId);
		return preferences.containsKey(productId) ?
			(preferences.get(productId).ShowByDefault__c != null ? 
				preferences.get(productId).ShowByDefault__c : false) : true;
	}

	/**
	 * Sets the specified values (isShownByDefault) for the
	 * product, in the current account.
	 * @author Fernando Gomez
	 * @date May 11, 2017 
	 * @param productId
	 */
	public void setShowByDefault(Id productId, Boolean isShownByDefault) {		
		ProductPreference__c p;
		// obtain the preference
		p = get(productId);
		// we change only the value for the shown by default field
		save(p, p == null ? false : p.IsFavorite__c, 
				isShownByDefault, productId);
	}

	/**
	 * Adds the specified product preference 
	 * specification to the specified account.
	 * @author Fernando Gomez
	 * @date   May 11, 2017
	 * @param accountId
	 * @param productId
	 * @param isFavorite
	 * @param showByDefault
	 */
	private void save(ProductPreference__c preference,
			Boolean isFavorite, Boolean showByDefault, Id productId) {
		if (preference == null)
			insert new ProductPreference__c(
				Account__c = accountId,
				Product__c = productId,
				IsFavorite__c = isFavorite,
				ShowByDefault__c = showByDefault
			);
		else {
			preference.IsFavorite__c = isFavorite;
			preference.ShowByDefault__c = showByDefault;
			update preference;
		}
	}

	/**
	 * If the product is already buffered in our list,
	 * we return the buffered version, if not,
	 * we look in the database... if none is found, null is returned.
	 * @author Fernando Gomez
	 * @date   June 9th, 2017
	 * @param productId
	 */
	private ProductPreference__c get(Id productId) {
		return preferences != null ?
			preferences.get(productId) : getPreference(productId);
	}

	/**
	 * Return an existence preference record or null
	 * if none is found.
	 * @author Fernando Gomez
	 * @date   May 11, 2017
	 * @param productId
	 */
	private ProductPreference__c getPreference(Id productId) {
		try {
			return [
				SELECT 
					Id,
					Account__c,
					Product__c,
					IsFavorite__c,
					ShowByDefault__c
				FROM ProductPreference__c
				WHERE Account__c = :accountId
				AND Product__c = :productId
			];
		} catch (QueryException ex) {
			return null;
		}
	}

	/**
	 * Creates a map woth all product preferences
	 * related to the specified account.
	 * @author Fernando Gomez
	 * @date   May 11, 2017
	 * @param accountId
	 * @param productId
	 */
	private void setup(Set<Id> productIds) {
		if (productIds != null) { 
			preferences = new Map<Id, ProductPreference__c>();
			for (ProductPreference__c p : [
						SELECT
							Id,
							Account__c,
							Product__c,
							IsFavorite__c,
							ShowByDefault__c
						FROM ProductPreference__c
						WHERE Account__c = :accountId
						AND Product__c IN :productIds
					])
				// we map the preference to the product
				preferences.put(p.Product__c, p);
		}
	}
}