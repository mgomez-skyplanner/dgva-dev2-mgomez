/**
 * Handles all interaction with the Add-On 
 * and Order Product Addon objects.
 * This object represents customization added
 * to cakes when orders areplaced.
 * of that product. 
 * @author Fernando Gomez
 * @date   July 10, 2017
 * @version	1.0	
 */
public class AddonManager {
	/**
	 * Main constructor	
	 * @author Fernando Gomez
	 * @date   July 10, 2017
	 */
	public AddonManager() {
		
	}

	/**
	 * Returns all categories that have add-ons that applies to both 
	 * the specified product familiy and subcategory. Each cateogory
	 * returned mapps the amount of addons that are currently availabel.
	 * @author Fernando Gomez
	 * @date July 11, 2017
	 * @param family
	 * @param subcategory
	 * @return 
	 */
	public List<AggregateResult> getCategories(
			String family, String subcategory) {
		return [
			SELECT 
				Category__c category,
				COUNT(Id) addonCount
			FROM AddOn__c
			WHERE AppliesToFamily__c INCLUDES (:family)
			AND AppliesToSubcategory__c INCLUDES (:subcategory)
			GROUP BY Category__c
		];
	}

	/**
	 * Return all available addons mapped by category.
	 * @author Fernando Gomez
	 * @date July 10, 2017
	 * @param family
	 * @param subcategory
	 * @param category
	 * @return Map<String, List<AddOn__c>>
	 */
	public List<Addon> getAddons(Id priceBookId,
			String family, String subcategory, String category) {
		List<Addon> result;
		Addon single;
		Map<Id, Addon> prices;

		// we build the wrappers
		prices = new Map<Id, Addon>();
		result = new List<Addon>();
		// we obtain the addons
		for (AddOn__c a : [
					SELECT
						Id,
						Category__c,
						Name,						
						ProductPrice__c,
						Product__c,
						Product__r.IsActive,
						Product__r.SKU__c,
						Product__r.Name,
						SupportsMultipleQuantities__c
					FROM AddOn__c
					WHERE AppliesToFamily__c INCLUDES (:family)
					AND AppliesToSubcategory__c INCLUDES (:subcategory)
					AND Category__c = :category
					ORDER BY Name
					LIMIT 50000
				]) {
			// we create the single wrapper
			single = new Addon(a.Id, a.Name, a.Category__c, a.ProductPrice__c, null,
					a.Product__c, a.Product__r.SKU__c, a.Product__r.Name, 
					a.SupportsMultipleQuantities__c);
						
			if (a.Product__c == null)
				// if no product, we simply add it to the return list
				result.add(single);
			else if (a.Product__r.IsActive)
				// if related to an active product...
				// we add the price to a map we will use to obtain
				// the actual prices from the specified pricebook
				prices.put(a.Product__c, single);
				
		}

		// now, we fetch the actual price in the pricebook specified
		for (PriceBookEntry pbe : [
					SELECT 
						Id,
						Product2Id,
						UnitPrice
					FROM PriceBookEntry
					WHERE PriceBook2Id = :priceBookId
					AND Product2Id IN :prices.keySet()
					AND IsActive = true
				]) {
			single = prices.get(pbe.Product2Id);
			// we override the existing price, and keep the one in the pricebook
			single.pricebookEntryId = pbe.Id;
			single.price = pbe.UnitPrice;
			// since we found a pricebook entry, we can now
			// add the product
			result.add(single);
		}

		// we sort all addons
		result.sort();

		// finally, we return the addons
		return result;
	}

	/**
	 * Wraps information about addons.. native objects cannot be used directly
	 * as the actual price must be obtained from the pricebook.
	 * @author Fernando Gomez
	 * @date Aug 9, 2017
	 */
	public class Addon implements Comparable {
		public Id addonId { get; private set; }
		public String name { get; private set; }
		public String category { get; private set; }
		public Id pricebookEntryId { get; private set; }
		public Decimal price { get; private set; }
		public Decimal quantiity { get; private set; }
		public Id productId { get; private set; }
		public String productSku { get; private set; }
		public String productName { get; private set; }
		public Boolean supportsMultiple { get; private set; }

		/**
		 * Constructor
		 * @author Fernando Gomez
		 * @date Aug 9, 2017
		 * @param addonId
		 * @param category
		 * @param price
		 * @param productId
		 * @param supportsMultiple
		 */
		private Addon(Id addonId, String name, String category, Decimal price,
				Id pricebookEntryId, Id productId, String productSku,
				String productName, Boolean supportsMultiple) {
			this.addonId = addonId;
			this.name = name;
			this.category = category;
			this.price = price;
			this.pricebookEntryId = pricebookEntryId;
			this.productId = productId;
			this.productSku = productSku;
			this.productName = productName;
			this.supportsMultiple = supportsMultiple;
		}

		/**
		 * Provides comparison by name
		 */
		public Integer compareTo(Object obj) {
			return name.compareTo(((Addon)obj).name);
		}
	}
}