/**
* <p> 
* Version log:
* <ul>
* <li>1.00 06/30/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 06/30/2017
* @description 	 - Test class for class OrderController (93% - 08/17/2017)
*/
 
@isTest
public with sharing class OrderController_Test {

	@testSetup
	static void setup() {
		TestClass_Utility.createCustomSettingsData();

		Route__c route 	= TestClass_Utility.createRoute('Test Route');
		insert route;

		Account account = TestClass_Utility.createAccount('Test Account', route);
		insert account;

		Opportunity opp = TestClass_Utility.createOpportunity(account);
		insert opp;

		Order order 			= TestClass_Utility.createOrder(account, opp, 'Draft');
		insert order;

		Product2 product1 		= TestClass_Utility.createProduct('Test Product 1');
		insert product1;

		Id standardPriceBookId = Test.getStandardPricebookId();
		PricebookEntry pbe 		= [SELECT Id FROM PricebookEntry WHERE Product2Id =: product1.Id AND PriceBook2Id =: standardPriceBookId];

		OrderController.OrderProductRemover_Wrapper oprWrapper = OrderController.getOrderProducts(order.Id);
		system.assertEquals(OrderController.Error_NoProductFoundInOrder, oprWrapper.Error);

		OrderItem orderProduct1 = TestClass_Utility.createOrderProduct(order, pbe);
		insert orderProduct1;

		TestClass_Utility.CreateDefaultDataForBillings(account, order, product1);
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - Test method for constructor of class OrderController
	*/
	@isTest
	static void Test_Constructor() {
		Test.startTest();

		Order order = [SELECT Id, Status FROM Order];

		ApexPages.StandardController stdController = new ApexPages.StandardController(order);
		OrderController orderController = new OrderController(stdController);

		PageReference pRef = Page.VoidOrderProducts;
		Test.setCurrentPage(pRef);

		pRef.getParameters().put('id', order.Id);

		orderController = new OrderController();

		Test.stopTest();
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - Test method for methods voidOrderProduct and getOrderProducts
	*/
	@isTest
	static void test_voidOrderProduct() {
		Test.startTest();

		//Mandatory setting for Accounting Seed
		AcctSeed__Billing_Format__c pdfFormat = TestClass_Utility.createPDFFormat('Test PDF Format', 'Billing', 'AccountingHome', 'OrderConfirmation');
		insert pdfFormat;

		Order order = [SELECT 
							Id,
							Status,
							(
								SELECT 
									Id
								FROM
									OrderItems
							)
						FROM
							Order];

		order.Status = 'Prepared';
		update order;
		order.Status = 'Activated';
		update order;

		Test.stopTest();

		//order id is empty
		OrderController.OrderProductRemover_Wrapper oprWrapper = OrderController.getOrderProducts('');
		system.assertEquals(OrderController.Error_OrderIdEmpty, oprWrapper.Error);

		//order id is empty
		oprWrapper = OrderController.voidOrderProducts('', new List<String>(), new List<Decimal>(), new List<String>());
		system.assertEquals(OrderController.Error_OrderIdEmpty, oprWrapper.Error);

		//no order product passed in param
		oprWrapper = OrderController.voidOrderProducts(order.Id, new List<String>(), new List<Decimal>(), new List<String>());
		system.assertEquals(OrderController.Error_NoOrderProductAsParam, oprWrapper.Error);

		//all good
		oprWrapper = OrderController.getOrderProducts(order.Id);
		
		oprWrapper = OrderController.voidOrderProducts(order.Id, new List<String> { order.OrderItems[0].Id }, new List<Decimal> { 1 }, new List<String> {'explanation'});
		system.assertEquals('', oprWrapper.Error);

		//just invoke the default constructor of this class to increase code coverage
		OrderController.OrderProduct_Wrapper opWrapper = new OrderController.OrderProduct_Wrapper();
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 08/17/2017
	* @description 	 - Test method for methods setOrderAsDelivered
	*/
	@isTest
	static void test_setOrderAsDelivered() {
		Test.startTest();

		Order order = [SELECT Id, Status FROM Order];

		order.Status = 'Prepared';
		update order;
		order.Status = 'Activated';
		update order;

		String result = OrderController.setOrderAsDelivered(order.Id);

		Test.stopTest();

		order = [SELECT Id, Status FROM Order];

		system.assertEquals('1', result);
		system.assertEquals('Delivered', order.Status);

		//increase code coverage
		result = OrderController.setOrderAsDelivered('');
		system.assertEquals(OrderController.Error_OrderIdEmpty, result);

		//increase code coverage
		result = OrderController.setOrderAsDelivered('12345678');
		system.assertEquals(OrderController.Error_OrderNotFound, result);
	}
}