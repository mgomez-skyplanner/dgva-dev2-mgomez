/**
 * Test class for RecurringOrderManager
 * @author  William Garcia
 * @date    06/12/2017
 */
@isTest (seeAllData = false)
public class RecurringOrderManagerTest {
	
	public static DateTime now = DateTime.now();
    public static Date nowD = now.date();

    /**
     * Init test data
     * @author William Garcia
     * @date   05/10/2017
     */
    @testSetup
    public static void init() {
        
        Route__c route1 = new Route__c(Name = 'route1');
        insert route1;

        Account acc1 = new Account(
            Name = 'Account1',
            BillingCountry = 'C1',
            BillingCity = 'C2',
            Total_Balance__c = 50,
            Route__c = route1.Id
        );
        insert acc1;

        Product2 prod1 = new Product2(
            Name = 'Product1',
            IsActive = true,
            Price__c = 10,
            Units_Pk__c = 'DOZ',
            SKU__c = '123'
        );
        insert prod1;

        PricebookEntry pbe1 = [SELECT Id 
                                FROM PricebookEntry 
                                WHERE Pricebook2Id = :[SELECT Price_Book__c 
                                                        FROM Account 
                                                        WHERE Id = :acc1.Id][0].Price_Book__c 
                                AND Product2Id = :prod1.Id];

        List<Order> orders = new List<Order>();

        Order order1 = new Order (
            AccountId = acc1.Id,
            Status = 'Draft', 
            EffectiveDate = nowD,
            EndDate = nowD,
            Type = Constants.ORDER_TYPE_RECURRING
        );
        insert order1;

        insert new List<OrderItem> {
            new OrderItem(
                EndDate = nowD,
                OrderId = order1.Id,
                PricebookentryId = pbe1.Id,
                Quantity = 2,
                Total_Pieces__c = 2,
                SKU__c = '123',
                UnitPrice = 10,
                Delivery_Days__c = 'Monday;Tuesday;Wednesday;Thursday;Friday;Saturday'
            ),
            new OrderItem(
                EndDate = nowD,
                OrderId = order1.Id,
                PricebookentryId = pbe1.Id,
                Quantity = 4,
                Total_Pieces__c = 4,
                SKU__c = '345',
                UnitPrice = 20,
                Delivery_Days__c = 'Monday;Tuesday;Wednesday;Thursday;Friday;Saturday'
            )};
       }

    /**
     * Test recurring order process batch
     * @author William Garcia
     * @date   05/10/2017
     */
    static testMethod void testCreateOrdersFromRecurrings() {
        RecurringOrderManager manager = new RecurringOrderManager();
		manager.createOrders();

        system.assert([SELECT Id 
                        FROM Order 
                        WHERE Type = :Constants.ORDER_TYPE_REGULAR].size() == 1);
        system.assert([SELECT Id 
                        FROM Order 
                        WHERE Type = :Constants.ORDER_TYPE_RECURRING
                        AND Last_Recurring_Order_Generation_Message__c = 'SUCCESS'].size() == 1);
        system.assert([SELECT Id FROM OrderItem].size() == 4);
    }

}