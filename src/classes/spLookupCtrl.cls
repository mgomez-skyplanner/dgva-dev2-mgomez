/**
 * 
 * @author	Fernando Gomez
 * @version	1.0
 * @date	May 23, 2016
 */
global class spLookupCtrl {
	/**
	 * Main constructor
	 * @author	Fernando Gomez
	 * @version	1.0
	 * @date	May 23, 2016
	 */        
    global spLookupCtrl() {
    	
    }
    
    /**
	 * 
	 * @author	Fernando Gomez
	 * @version	1.0
	 * @date	May 23, 2016
	 * @param	objectName
	 * @param	checkFields
	 * @param	returnFields
	 * @param	criteria
	 */
	@RemoteAction
    global static List<sObject> lookup(
    		String objectName,
    		String checkFields,
    		String returnFields,
    		String criteria,
    		Map<String, String> filtersMap) {	
    	List<String> comps = new List<String>(), filters = new List<String>();   	
    	
    	// critria constrains		
    	for (String f : checkFields.split(','))
    		comps.add(
    			String.escapeSingleQuotes(f) + ' LIKE \'%' + 
    				String.escapeSingleQuotes(criteria) + '%\'');
    				
    	// filter constrains
    	if (filtersMap != null)
    		for (String f : filtersMap.keySet())
    			filters.add(String.escapeSingleQuotes(f) + ' = ' +
    				(filtersMap.get(f) == null ? ' null ' :  
    					'\'' + String.escapeSingleQuotes(filtersMap.get(f)) + '\''));
    			
    	return Database.query(
			' SELECT Id, ' +
				String.escapeSingleQuotes(returnFields) + 
			' FROM ' + String.escapeSingleQuotes(objectName) + 
			' WHERE ( ' + String.join(comps, ' OR ') + ' ) ' +  
			(filters.isEmpty() ? '' : ' AND ( ' + 
				String.join(filters, ' AND ') + ' ) ' ) +
			' ORDER BY ' + String.escapeSingleQuotes(returnFields) +
			' LIMIT 100 '
		);
    }
}