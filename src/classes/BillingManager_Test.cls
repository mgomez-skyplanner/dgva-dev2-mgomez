/**
* <p> 
* Version log:
* <ul>
* <li>1.00 06/29/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 06/29/2017
* @description 	 - Test class for class BillingManager (93% - 06/29/2017)
*/
 
@isTest
public with sharing class BillingManager_Test {

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Performs assertions for test method test_ConvertOrder_To_SalesOrder
	*/
	static void performOrder2BillingAssertions(Order order, AcctSeed__Billing__c billing) {
		List<Order_to_Billing_Fields__c> fieldsList = Order_to_Billing_Fields__c.getall().values();

		for(Order_to_Billing_Fields__c field : fieldsList) {
			String orderField 	= field.Name;
			String billingField = field.Billing_Field__c;

			system.debug('--------------- Order field: ' + orderField);
			system.debug('--------------- Billing field: ' + billingField);
			
			system.assertEquals(order.get(orderField), billing.get(billingField));
		}
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - Test method for constructor of class BillingManager
	*/
	@isTest
	static void Test_Constructor() {
		AcctSeed__Billing__c billing = new AcctSeed__Billing__c();
		ApexPages.StandardController stdController = new ApexPages.StandardController(billing);

		BillingManager bm = new BillingManager(stdController);
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - Test method for method BillingManager.GenerateBillingFromOrder
	*/
	@isTest
	static void test_GenerateBillingFromOrder() {
		Test.startTest();

		TestClass_Utility.createCustomSettingsData();

		Route__c route 	= TestClass_Utility.createRoute('Test Route');
		insert route;

		Account account = TestClass_Utility.createAccount('Test Account', route);
		insert account;

		Opportunity opp = TestClass_Utility.createOpportunity(account);
		insert opp;

		Order order 			= TestClass_Utility.createOrder(account, opp, 'Draft');
		insert order;

		Product2 product1 		= TestClass_Utility.createProduct('Test Product 1');
		insert product1;

		Id standardPriceBookId = Test.getStandardPricebookId();
		PricebookEntry pbe 		= [SELECT Id FROM PricebookEntry WHERE Product2Id =: product1.Id AND PriceBook2Id =: standardPriceBookId];

		OrderItem orderProduct1 = TestClass_Utility.createOrderProduct(order, pbe);
		insert orderProduct1;

		TestClass_Utility.CreateDefaultDataForBillings(account, order, product1);

		//this line is to provoke throwing a ExceptionHelper.BillingPDFFormatMissing_Exception
		//just to increase code coverage
		try {
			BillingManager.GenerateBillingFromOrder(order, true);
		}
		catch (Exception e) { }

		//Mandatory setting for Accounting Seed
		AcctSeed__Billing_Format__c pdfFormat = TestClass_Utility.createPDFFormat('Test PDF Format', 'Billing', 'AccountingHome', 'OrderConfirmation');
		insert pdfFormat;

		order.Status = 'Prepared';
		update order;
		order.Status = 'Activated';
		update order;

		Test.stopTest();

		//reload order
		BillingManager.LoadCustomSettings();
		order = BillingManager.ReloadOrder(order.Id);

		AcctSeed__Billing__c billing = BillingManager.GenerateBillingFromOrder(order, true);

		performOrder2BillingAssertions(order, billing);

		String result = BillingManager.GenerateBillingFromOrder(order.Id, true);

		BillingManager.UnpostBilling(new List<AcctSeed__Billing__c> { billing });
	}
}