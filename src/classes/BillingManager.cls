/**
* <p> 
* Version log:
* <ul>
* <li>1.00 06/09/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 06/09/2017
* @description 	 - This class provides methods to create a Billing (AcctSeed__Billing__c) based on an Order.
*/
 
global with sharing class BillingManager {
	
	private static List<Order_to_Billing_Fields__c> order2BillingFields_List;
	private static List<Order_Product_to_Billing_Line_Fields__c> orderProduct2BillingLineFields_List;

	public BillingManager(ApexPages.StandardController stdController) {
		
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Load values of custom settings Order_to_Billing_Fields__c and Order_Product_to_Billing_Line_Fields__c
	*/
	@TestVisible
	private static void LoadCustomSettings() {
		if(order2BillingFields_List == NULL) {
			order2BillingFields_List = Order_to_Billing_Fields__c.getall().values();
		}

		if(orderProduct2BillingLineFields_List == NULL) {
			orderProduct2BillingLineFields_List = Order_Product_to_Billing_Line_Fields__c.getall().values();
		}
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Generates a Billing from an Order
	*/
	public static AcctSeed__Billing__c GenerateBillingFromOrder(Order order, Boolean postBilling) {
		system.debug('-----------------**********--------------- GenerateBillingFromOrder');
		system.debug('-----------------**********--------------- Order: ' + order);

		if(order.Status != 'Activated' && order.Status != 'Delivered')
			throw new ExceptionHelper.InvalidOrderStatusException('Order Status Category must be "Activated" to be able to create a Billing.');

		LoadCustomSettings();

		//reload Order in case the Sales Order, Billing and OrderItems haven't been loaded from DB
		if(order.Sales_Order__c == NULL || order.OrderItems.size() == 0 || order.Billing__c == NULL) {
			order = ReloadOrder(order.Id);
		}

		if(order.Sales_Order__c != NULL) {
			system.debug('-----------------**********--------------- Order Billing: ' + order.Billing__c);

			//delete current Billing to create a new one
			if(order.Billing__c != NULL) {
				system.debug('-----------------**********--------------- Attempting to delete Order Billing...');
				deleteOrderBilling(order);
				system.debug('-----------------**********--------------- Order Billing DELETED...');
			}

			//copy fields from Order to new Billing object according to custom setting "Order to Billing Fields"
			AcctSeed__Billing__c billing = CreateBilling(order);

			//set mandatory fields for Billing
			billing.AcctSeedERP__Sales_Order__c 	= order.Sales_Order__c;
			//billing.AcctSeed__Date__c 				= order.EffectiveDate;
			billing.AcctSeed__Customer__c 			= order.AccountId;

			insert billing;

			system.debug('-----------------**********--------------- Billing INSERTED!!! ' + billing);

			List<AcctSeed__Billing_Line__c> billingLines = CreateBillingLines(order, billing);

			insert billingLines;

			//update reference from Sales Order to Order
			//using a wrapper Order instance to avoid System.FinalException
			Order orderWrapper 		= new Order();
			orderWrapper.Id 		= order.Id;
			orderWrapper.Billing__c = billing.Id;

			//stop recursive invokations of Order triggers
	  		TriggerControlClass.stopOrderTriggers = true;
	  		TriggerControlClass.stopOrderItemTriggers = true;

			update orderWrapper;

	  		TriggerControlClass.stopOrderTriggers = false;
	  		TriggerControlClass.stopOrderItemTriggers = false;

			system.debug('-----------------**********--------------- Order to Billing lookup updated!!! ' + order);

			if(postBilling) {
				PostBilling(new List<AcctSeed__Billing__c> { billing });
			}

			return billing;
		}

		return NULL;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/03/2017
	* @description 	 - Utility method to Post a Billing
	*/
	public static List<AcctSeed.PostResult> PostBilling(List<AcctSeed__Billing__c> billings) {
		// Call the post billings service
		List<AcctSeed.PostResult> postResults = AcctSeed.BillingPostService.postBillings(billings);

		// Loop through post results
		for (AcctSeed.PostResult theResult : postResults) {
		    if (theResult.isSuccess) {
		        System.debug('Successfully posted billing: ' + theResult.id);
		    } 
		    else {
		        System.debug('Error posting billing ' + theResult.id);

		        for (AcctSeed.PostResult.PostErrorResult errorResult: theResult.errors) {
		            System.debug('Error status code ' + errorResult.statusCode);
		            System.debug('Error message ' + errorResult.message);
		        }
		    }
		}

		return postResults;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/03/2017
	* @description 	 - Utility method to Unpost a Billing
	*/
	public static List<AcctSeed.PostResult> UnpostBilling(List<AcctSeed__Billing__c> billings) {
		// Call the post billings service
		List<AcctSeed.PostResult> postResults = AcctSeed.BillingPostService.unpostBillings(billings);

		// Loop through post results
		for (AcctSeed.PostResult theResult : postResults) {
		    if (theResult.isSuccess) {
		        System.debug('Successfully unposted billing: ' + theResult.id);
		    } 
		    else {
		        System.debug('Error unposting billing ' + theResult.id);

		        for (AcctSeed.PostResult.PostErrorResult errorResult: theResult.errors) {
		            System.debug('Error status code ' + errorResult.statusCode);
		            System.debug('Error message ' + errorResult.message);
		        }
		    }
		}

		return postResults;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Reloads necessary fields of an Order from DB
	*/
	@TestVisible
	private static Order ReloadOrder(Id orderId) {
		//Build the query with Sales Order fields involved in the conversion process
		String query = 'SELECT Id, Status, Billing__c, '; //EndDate field removed so it must be set up in custom setting Order_to_Billing_Fields__c

		//this Boolean is a trick to check if field Order.EndDate is setup in the custom setting Order_to_Billing_Fields__c
		Boolean endDateFound = false;

		for(Order_to_Billing_Fields__c field : order2BillingFields_List) {
			query += field.Name + ', ';

			if(field.Name == 'EndDate')
				endDateFound = true;
		}

		//because field Order.EndDate is mandatory, if not present, then it is placed in code
		if(!endDateFound)
			query += 'EndDate, ';

		//Include the Sales Order Line fields
		query += ' (SELECT Id, PriceBookEntry.Product2Id, PriceBookEntryId, ';
		for(Order_Product_to_Billing_Line_Fields__c field : orderProduct2BillingLineFields_List) {
			query += field.Name + ', ';
		}

		if(query.endsWith(', '))
			query = query.removeEnd(', ');
		
		query += ' FROM OrderItems) ';
		query += ' FROM Order WHERE Id = \'' + orderId + '\'';

		system.debug('------------------------------------ Reload Order query: ' + query);

		List<Order> result = Database.query(query);

		//if an Order was found, then return it
		if(result.size() > 0) {
			system.debug('------------------------------------ Reloaded Order: ' + result[0]);

			return result[0];
		}

		//if a Sales Order was NOT found, then create a new one
		return NULL;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Creates a new Billing and copy fields from the Order to it
	* @param       	 - The base Order
	* @return        - The new Billing record
	*/
	private static AcctSeed__Billing__c CreateBilling(Order order) {
		AcctSeed__Billing__c billing = new AcctSeed__Billing__c();

		for(Order_to_Billing_Fields__c field : order2BillingFields_List) {
			String orderField = field.Name;
			String billingField = field.Billing_Field__c;
			
			system.debug('---------------------------- copying field: Billing.' + billingField + ' = Order.' + orderField + ' (' + order.get(orderField) + ')');
			billing.put(billingField, order.get(orderField));
		}

		billing.Order__c = order.Id;

		system.debug('---------------------------- AFTER Billing: ' + billing);

		return billing;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Creates a collection of Billing Lines based on the Order Items of an Order
	* @param       	 - The base Order
	* @param       	 - The Billing the new Billing Lines will belong to
	* @return        - The list of new Billing Lines records
	*/
	private static List<AcctSeed__Billing_Line__c> CreateBillingLines(Order order, AcctSeed__Billing__c billing) {
		//load Sales Order to match Order Items with Sales Order Lines and add the Sales Order Lines reference to the Billing Lines
		AcctSeedERP__Sales_Order__c salesOrder = [SELECT 
													Id,
													(
														SELECT
															Id,
															AcctSeedERP__Product__c
														FROM 
															AcctSeedERP__Sales_Order_Line__r
													)
												FROM
													AcctSeedERP__Sales_Order__c
												WHERE
													Order__c =: order.Id
												LIMIT 1];

		Map<OrderItem, AcctSeedERP__Sales_Order_Line__c> orderItem_SalesOrderLine_Map = getOrderProducts_Map(order, salesOrder);

		List<AcctSeed__Billing_Line__c> billingLines = new List<AcctSeed__Billing_Line__c>();

		for(OrderItem orderItem : order.OrderItems) {
			AcctSeed__Billing_Line__c billingLine = new AcctSeed__Billing_Line__c();

			//copy fields from Order Item to Billing Line according to custom setting Order_Product_to_Billing_Line_Fields__c
			for(Order_Product_to_Billing_Line_Fields__c field : orderProduct2BillingLineFields_List) {

				String orderItemField 	= field.Name;
				String billingLineField = field.BillingLine_Field__c;
				
				system.debug('---------------------------- copying field: BillingLine.' + billingLineField + ' = OrderItem.' + orderItemField + ' (' + orderItem.get(orderItemField) + ')');
				billingLine.put(billingLineField, orderItem.get(orderItemField));
			}

			//reference the Product
			billingLine.AcctSeed__Product__c = orderItem.PriceBookEntry.Product2Id;

			//reference the Sales Order Line
			AcctSeedERP__Sales_Order_Line__c salesOrderLine = orderItem_SalesOrderLine_Map.get(orderItem);
			billingLine.AcctSeedERP__Sales_Order_Line__c = salesOrderLine != NULL ? salesOrderLine.Id : NULL;

			//reference the Billing of the new Billing Line
			billingLine.AcctSeed__Billing__c = billing.Id;

			billingLines.add(billingLine);
		}

		system.debug('---------------------------- AFTER Billing: ' + billing);

		return billingLines;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/09/2017
	* @description 	 - Creates a Map to match Order Items with Sales Order Lines
	* @param       	 - The Order
	* @param       	 - The Sales Order
	* @return        - The Map:
	* 				   Key: Order Item, Value: Sales Order Line
	*/
	private static Map<OrderItem, AcctSeedERP__Sales_Order_Line__c> getOrderProducts_Map(Order order, AcctSeedERP__Sales_Order__c salesOrder) {
		Map<OrderItem, AcctSeedERP__Sales_Order_Line__c> result = new Map<OrderItem, AcctSeedERP__Sales_Order_Line__c>();

		system.debug('---------------------------- Order Items: ');
		system.debug(order.OrderItems);
		system.debug('---------------------------- Sales Order Lines: ');
		system.debug(salesOrder.AcctSeedERP__Sales_Order_Line__r);

		for(OrderItem orderItem : order.OrderItems) {
			Id productId = orderItem.PriceBookEntry.Product2Id;

			system.debug('---------------------------- Searching Product Id: ');
			system.debug(productId);

			if(salesOrder.AcctSeedERP__Sales_Order_Line__r != NULL && salesOrder.AcctSeedERP__Sales_Order_Line__r.size() > 0) {
				for(AcctSeedERP__Sales_Order_Line__c salesOrderLine : salesOrder.AcctSeedERP__Sales_Order_Line__r) {

					system.debug('---------------------------- Attempting to match with Sales Order Line Product Id: ');
					system.debug(salesOrderLine.AcctSeedERP__Product__c);

					//if Sales Order Line found with same Product, a match found and may be updated
					if(salesOrderLine.AcctSeedERP__Product__c == productId) {
						system.debug('---------------------------- Match found with same Product Id !!!');

						result.put(orderItem, salesOrderLine);
					}
				}
			}
		}

		return result;
	}
	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/09/2017
	* @description 	 - Delete an Order Billing, if exists, and all its references if no Billing Cash Receipt exists
	* @param       	 - The Order
	*/
	public static void deleteOrderBilling(Order order) {
		List<AcctSeed__Billing__c> billings = [SELECT Id, AcctSeed__Billing_Cash_Receipt_Count__c FROM AcctSeed__Billing__c WHERE Order__c =: order.Id];
		List<AcctSeed__Billing__c> billings2Delete = new List<AcctSeed__Billing__c>();

		for(AcctSeed__Billing__c billing : billings) {
			//validate the billing has no cash receipts
			if(billing.AcctSeed__Billing_Cash_Receipt_Count__c == 0) {

				//unpost the billing
				UnpostBilling(new List<AcctSeed__Billing__c> { billing });
				
				billings2Delete.add(billing);
			}
		}
		
		delete billings2Delete;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/09/2017
	* @description 	 - Method to invoke GenerateBillingFromOrder from a Lightning action
	* @param       	 - The Order Id
	*/
    @AuraEnabled
    webservice static String GenerateBillingFromOrder(String orderId, Boolean postBilling) {
    	LoadCustomSettings();

    	Order order = BillingManager.ReloadOrder(orderId);

    	if(order != NULL) {

			try {
    			BillingManager.GenerateBillingFromOrder(order, postBilling);
			}
			catch(Exception e) {
				return 'Error attempting to create a Billing from an Order: ' + e.getMessage() + '. Stack trace: ' + e.getStackTraceString();
			}
    	}

    	return '1';
    }
}