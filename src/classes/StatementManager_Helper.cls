/**
* <p> 
* Version log:
* <ul>
* <li>1.00 09/29/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 09/29/2017
* @description 	 - This class provides utility methods related to bussines logic of Statement__c object and Statements generation
* @version 
* 	1.01		 10/26/2017 William Garcia: Statement develoment, continuation...
*/

global with sharing class StatementManager_Helper {

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/29/2017
	* @description 	 - Represents the types of activities presented in a statement
	*/
	public enum StatementActivity {
		Debit,
		Credit,
		Payment,
		Billed,
		Paid
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/29/2017
	* @description 	 - Represents the types of statements
	*/
	public enum StatementType {
		Outstanding,
		Activity
	}

	/* STATEMENTS METHODS */

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/29/2017
	* @description 	 - Retrieves all statements by Account
	* 				   Key: Account.id
	* 				   Value: List of Account statements
	*/
	public static Map<Id, List<Statement__c>> GetAllStatements(List<Id> accountIds) {
		List<Statement__c> statements = [SELECT 
											Id,
											Account__c,
											Account__r.Statement_Frequency__c,
											Account__r.Statement_Day__c,
											Type__c,
											Balance__c,
											CreatedDate,
											Period_Start_Date__c,
											Period_End_Date__c
										FROM
											Statement__c
										WHERE 
											Account__c IN: accountIds
										ORDER BY 
											CreatedDate ASC];

		return convertListToMap(statements);
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/29/2017
	* @description 	 - Retrieves the inmediate previous Account statement generated before pStartDate
	* 				   Key: Account.id
	* 				   Value: Account statement generated inmediately before pStartDate
	*/
	public static Map<Id, Statement__c> GetPreviousStatement(List<Id> accountIds, Date pStartDate) {
		List<Statement__c> statements = [SELECT 
											Id,
											Type__c,
											Balance__c,
											CreatedDate,
											Period_Start_Date__c,
											Period_End_Date__c,

											Account__c,
											Account__r.Statement_Frequency__c,
											Account__r.Statement_Day__c
										FROM
											Statement__c
										WHERE
											Account__c IN: accountIds AND 
											CreatedDate <=: pStartDate
										ORDER BY 
											CreatedDate DESC];

		Map<Id, Statement__c> result = new Map<Id, Statement__c>();

		if(statements.size() > 0) {
			for(Statement__c statement : statements) {
				if(!result.containsKey(statement.Account__c))
					result.put(statement.Account__c, statement);
				else {
					Statement__c currentStatement = result.get(statement.Account__c);

					//if Account is matched with a Statement already, then take the lattest based on date CreatedDate
					if(statement.CreatedDate > currentStatement.CreatedDate)
						result.put(statement.Account__c, statement);
				}
			}
		}

		return result;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/29/2017
	* @description 	 - Retrieves all statements of type sType
	* 				   Key: Account.id
	* 				   Value: List of Account statements of type = sType
	*/
	public static Map<Id, List<Statement__c>> GetStatementsBy_Type(List<Id> accountIds, StatementType sType) {
		String strType = String.valueOf(sType);

		List<Statement__c> statements = [SELECT 
											Id,
											Account__c,
											Account__r.Statement_Frequency__c,
											Account__r.Statement_Day__c,
											Type__c,
											Balance__c,
											CreatedDate,
											Period_Start_Date__c,
											Period_End_Date__c
										FROM
											Statement__c
										WHERE 
											Account__c IN: accountIds AND 
											Type__c =: strType
										ORDER BY 
											CreatedDate ASC];

		return convertListToMap(statements);
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/29/2017
	* @description 	 - Retrieves all statements in the period of time between pStartDate and pEndDate
	* 				   Key: Account.id
	* 				   Value: List of Account statements in the period of time between pStartDate and pEndDate
	*/
	public static Map<Id, List<Statement__c>> GetStatementsBy_Period(List<Id> accountIds, Date pStartDate, Date pEndDate) {
		List<Statement__c> statements = [SELECT 
											Id,
											Account__c,
											Account__r.Statement_Frequency__c,
											Account__r.Statement_Day__c,
											Type__c,
											Balance__c,
											CreatedDate,
											Period_Start_Date__c,
											Period_End_Date__c
										FROM
											Statement__c
										WHERE 
											Account__c IN: accountIds AND 
											Period_Start_Date__c >=: pStartDate AND 
											Period_End_Date__c <=: pEndDate
										ORDER BY 
											CreatedDate ASC];

		return convertListToMap(statements);
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/29/2017
	* @description 	 - Retrieves all statements in the period of time between pStartDate and pEndDate with type = sType
	* 				   Key: Account.id
	* 				   Value: List of Account statements in the period of time between pStartDate and pEndDate with type = sType
	*/
	public static Map<Id, List<Statement__c>> GetStatementsBy_Type_Period(List<Id> accountIds, StatementType sType, Date pStartDate, Date pEndDate) {
		String strType = String.valueOf(sType);

		List<Statement__c> statements = [SELECT 
											Id,
											Account__c,
											Account__r.Statement_Frequency__c,
											Account__r.Statement_Day__c,
											Type__c,
											Balance__c,
											CreatedDate,
											Period_Start_Date__c,
											Period_End_Date__c
										FROM
											Statement__c
										WHERE 
											Account__c IN: accountIds AND 
											Type__c =: strType AND 
											Period_Start_Date__c >=: pStartDate AND 
											Period_End_Date__c <=: pEndDate
										ORDER BY 
											CreatedDate ASC];

		return convertListToMap(statements);
	}

	/* END STATEMENTS METHODS */


	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/05/2017
	* @description 	 - Verifies if a Statement exists with the same type and in the same period of time
	*/
	public static Boolean checkStatementsExists(Id accountId, StatementType sType, Date pStartDate, Date pEndDate) {
		Map<Id,List<Statement__c>> statements = GetStatementsBy_Type_Period(new List<Id> { accountId }, sType, pStartDate, pEndDate);

		return statements.keySet().size() > 0;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/09/2017
	* @description 	 - Attaches a File to one or more Statement__c records. This method is anotated as (callout=true) because, according to SF documentation, ContentVersion records are created via SOAP API
	*/
	@future(callout=true)
	public static void attachPDFToStatements_Future(List<Id> statementIds) {
		attachPDFToStatements(statementIds);
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/09/2017
	* @description 	 - Attaches a File to one or more Statement__c records. This method is anotated as (callout=true) because, according to SF documentation, ContentVersion records are created via SOAP API
	*/
	public static void attachPDFToStatements(List<Id> statementIds) {
		attachPDFToStatements([SELECT Id, 
									  Type__c, 
									  Period_Start_Date__c, 
									  Period_End_Date__c 
								FROM Statement__c 
								WHERE Id IN: statementIds]);
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/09/2017
	* @description 	 - Attaches a File to one or more Statement__c records. This method is anotated as (callout=true) because, according to SF documentation, ContentVersion records are created via SOAP API
	*/
	public static void attachPDFToStatements(List<Statement__c> statements) {
		Map<Id, ContentVersion> statementFile_Map = new Map<Id, ContentVersion>();

		//create the files for each Statement__c
		for(Statement__c st : statements) {
			Datetime sDate;
			if(st.Period_Start_Date__c != null)
				sDate = Datetime.newInstance(st.Period_Start_Date__c, Time.newInstance(0, 0, 0, 0));
			Datetime eDate = Datetime.newInstance(st.Period_End_Date__c, Time.newInstance(0, 0, 0, 0));

			//create the PageReference pointing to the PDF VF page based on value of Statement__c.Type__c
			PageReference pdfPage = st.Type__c == 'Outstanding' ? Page.OutstandingStatementPage : Page.ActivityStatementPage;

			//add parameter to page pointing to the Statement__c.Id
			pdfPage.getParameters().put('sid', st.Id);

			//create the File
			ContentVersion cont = new ContentVersion();

		    cont.Title = st.Type__c + ' Statement (' + (sDate == null ? '' : sDate.format('MM/dd/yyyy')) + 
		    	' - ' + eDate.format('MM/dd/yyyy') + ')';
		    cont.PathOnClient = cont.Title + '.pdf';

			system.debug('----------------- StatementManager_Helper attachPDFToStatements page params: ');
			system.debug(pdfPage.getParameters());
		    //If we're running tests then the getContent method is not permitted, so we use a generic blob
		    cont.VersionData = !Test.isRunningTest() ? pdfPage.getContent() : Blob.valueOf('UNIT.TEST');

		    cont.Origin = 'C';

		    statementFile_Map.put(st.Id, cont);
		}

		insert statementFile_Map.values();

		//get the Ids of just created ContentVersion records
		List<Id> cvIds = new List<Id>();
		for(Id stId : statementFile_Map.keySet()) {
			ContentVersion cont = statementFile_Map.get(stId);

			cvIds.add(cont.Id);
		}

		//get the ContentDocumentId of just created ContentVersion records
		Map<Id, ContentVersion> cvId_cv = new Map<Id, ContentVersion>([SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id IN: cvIds]);

		List<ContentDocumentLink> links = new List<ContentDocumentLink>();

		//create a ContentDocumentLink of just created ContentVersion records
		for(Statement__c st : statements) {
			ContentDocumentLink cdl = new ContentDocumentLink();

			Id contentVersionId = statementFile_Map.get(st.Id).Id;
			Id contentDocumentId = cvId_cv.get(contentVersionId).ContentDocumentId;

			cdl.ContentDocumentId = contentDocumentId;
			cdl.LinkedEntityId = st.Id;
			cdl.ShareType = 'V';

			links.add(cdl);
		}

		insert links;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/29/2017
	* @description 	 - Updates balance of a list of Statement_Row based on daily activities
	*/
	public static void UpdateCumulativeBalance(List<Statement_Row> rows, StatementType sType) {
		Decimal previousBalance = 0;

		system.debug('--------------------------------------- rows before: ');
		system.debug(rows);

		for(Statement_Row row : rows) {
			system.debug('----------------------- Statement_Row:');
			system.debug(row);

			// if(sType = StatementType.Outstanding){
			// 	row.balance = previousBalance + row.Balance;
			// }
			// else if(row.Activity == StatementActivity.Credit){
			// 	row.Balance = previousBalance - Math.abs(row.Balance);//decrease the Balance because a part of it has been paid by this credit
			// }
			// else if(row.Activity == StatementActivity.Payment) {
			// 	row.Balance = previousBalance - Math.abs(row.Balance);

			// 	system.debug('----------------------------- (Payment) row.Balance = ' + row.Balance);
			// }
			// else if(row.Activity == StatementActivity.Debit) {
			// 	row.Balance = previousBalance + row.Balance;//increase the Balance because something has been added to the cart

			// 	system.debug('----------------------------- (Debit) row.Balance = ' + row.Balance);
			// }

			row.balance = previousBalance + row.Balance;
			previousBalance = row.Balance;
		}

		system.debug('--------------------------------------- rows after: ');
		system.debug(rows);
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/29/2017
	* @description 	 - Utility method to create a Statement_Row based on a AcctSeed__Billing__c and a StatementActivity
	*/
	public static Statement_Row CreateRowFromBilling(AcctSeed__Billing__c billing, Decimal billingBalance, StatementActivity activityType) {
		Statement_Row row = new Statement_Row();

		row.AccountId 	 = billing.AcctSeed__Customer__c;
		row.ActivityDate = billing.AcctSeed__Date__c;
		row.SortingDate  = billing.AcctSeed__Date__c;
		//Credit memos don't show Due Dates.
		row.DueDate 	 = billing.AcctSeed__Type__c == 'Credit Memo' ? null : billing.AcctSeed__Due_Date2__c; 
		row.Activity 	 = activityType;
		row.ActivityType = billing.AcctSeed__Type__c;
		row.Reference 	 = billing.AcctSeed__Proprietary_Billing_Number__c; //Daniel: Reference for Billing should be the Propietary
		row.PONumber 	 = billing.AcctSeed__PO_Number__c;
		row.Total 		 = billingBalance ;//billing.AcctSeed__Total__c;
		row.BilledAmount = billingBalance;
		row.PaidAmount 	 = billing.AcctSeed__Type__c == 'Credit Memo' ? billingBalance : 0;
		row.Balance 	 = billingBalance;//balance must be re-calculated after sorting Statement_Row by date

		return row;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/29/2017
	* @description 	 - Utility method to create a Statement_Row based on a AcctSeed__Cash_Receipt__c and a StatementActivity
	*/
	public static Statement_Row CreateRowFromCashReceipt(AcctSeed__Cash_Receipt__c cashReceipt, Decimal amount, StatementActivity activityType) {
		Statement_Row row = new Statement_Row();

		List<AcctSeed__Billing_Cash_Receipt__c> billingCashReceipts = cashReceipt.AcctSeed__Billing_Cash_Receipts__r;

		row.AccountId 	 = cashReceipt.AcctSeed__Account__c;
		row.ActivityDate = cashReceipt.AcctSeed__Receipt_Date__c;
		row.SortingDate  = cashReceipt.AcctSeed__Receipt_Date__c;
		//Cash Receipts don't show Due Dates.
		row.DueDate 	 = null;
		row.Activity 	 = activityType;
		row.ActivityType = cashReceipt.AcctSeed__Purpose__c.replace(' Card', '')
														   .replace('Customer Receipt', 'Debit/Credit')
														   .replace('Electronic Deposit', 'E-Deposit');

		row.Reference 	 = cashReceipt.AcctSeed__Payment_Reference__c;
		
		row.PONumber 	 = billingCashReceipts.size() > 0 ? 
								billingCashReceipts[0].AcctSeed__Billing__r.AcctSeed__PO_Number__c :
								'';
		
		row.Total 		 = amount;
		row.BilledAmount = amount;
		row.PaidAmount 	 = (activityType == StatementActivity.Payment) ? -1 * cashReceipt.AcctSeed__Amount__c : cashReceipt.AcctSeed__Balance__c;
		row.Balance 	 = amount;//balance must be re-calculated after sorting Statement_Row by date

		return row;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/14/2017
	* @description 	 - Utility method to create a Statement_Row based on a AcctSeed__Billing_Credit_Memo__c and a StatementActivity
	*/
	public static Statement_Row CreateRowFromBillingCreditMemo(AcctSeed__Billing_Credit_Memo__c billingCreditMemo, StatementActivity activityType) {
		Statement_Row row = new Statement_Row();

		row.AccountId 	 = billingCreditMemo.AcctSeed__Billing_Credit_Memo__r.AcctSeed__Customer__c;
		row.ActivityDate = billingCreditMemo.CreatedDate;
		row.SortingDate  = billingCreditMemo.CreatedDate;
		row.DueDate 	 = billingCreditMemo.CreatedDate.date();
		row.Activity 	 = activityType;
		row.ActivityType = billingCreditMemo.AcctSeed__Billing_Credit_Memo__r.AcctSeed__Type__c;
		row.Reference 	 = billingCreditMemo.AcctSeed__Billing_Credit_Memo__r.AcctSeed__Proprietary_Billing_Number__c;
		row.PONumber 	 = billingCreditMemo.AcctSeed__Billing_Credit_Memo__r.AcctSeed__PO_Number__c;
		row.Total 		 = -Math.abs(billingCreditMemo.AcctSeed__Amount__c);
		row.BilledAmount = -Math.abs(billingCreditMemo.AcctSeed__Amount__c);
		row.PaidAmount 	 = -Math.abs(billingCreditMemo.AcctSeed__Amount__c);
		row.Balance 	 = -Math.abs(billingCreditMemo.AcctSeed__Amount__c);//balance must be re-calculated after sorting Statement_Row by date

		return row;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/14/2017
	* @description 	 - Utility method to create a Statement_Row based on a AcctSeed__Billing_Credit_Memo__c and a StatementActivity
	*/
	public static Statement_Row CreateRowFromBillingCashReceipt(AcctSeed__Billing_Cash_Receipt__c billingCashReceipt, StatementActivity activityType) {
		Statement_Row row = new Statement_Row();

		row.AccountId 	 = billingCashReceipt.AcctSeed__Cash_Receipt__r.AcctSeed__Account__c;
		row.ActivityDate = billingCashReceipt.CreatedDate;
		row.SortingDate  = billingCashReceipt.CreatedDate;
		row.DueDate 	 = billingCashReceipt.CreatedDate.date();
		row.Activity 	 = activityType;
		row.ActivityType = billingCashReceipt.AcctSeed__Cash_Receipt__r.AcctSeed__Purpose__c;
		row.Reference 	 = billingCashReceipt.AcctSeed__Cash_Receipt__r.AcctSeed__Payment_Reference__c;
		row.PONumber 	 = billingCashReceipt.AcctSeed__Billing__r.AcctSeed__PO_Number__c;
		row.Total 		 = -Math.abs(billingCashReceipt.AcctSeed__Applied_Amount__c);
		row.BilledAmount = -Math.abs(billingCashReceipt.AcctSeed__Applied_Amount__c);
		row.PaidAmount 	 = -Math.abs(billingCashReceipt.AcctSeed__Applied_Amount__c);
		row.Balance 	 = -Math.abs(billingCashReceipt.AcctSeed__Applied_Amount__c);//balance must be re-calculated after sorting Statement_Row by date

		return row;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/29/2017
	* @description 	 - Sorts a List of Statement_Row by date ascending
	*/
	public static List<Statement_Row> SortStatementsBubble(List<Statement_Row> rows) {
		Statement_Row temp = NULL;

		for (Integer i = 0; i < rows.size(); i++) {
		    for (Integer j = 0; j < rows.size() - 1; j++) {
		        if (rows[j].SortingDate > rows[j + 1].SortingDate) {
		            temp = rows[j + 1];
		            rows[j + 1] = rows[j];
		            rows[j] = temp;
		        }
		    }
		}

		return rows;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/16/2017
	* @description 	 - Sorts a List of Statement_Row by date ascending
	*/
	public static List<Statement_Row> SortStatementsQuickSort(List<Statement_Row> rows) {
		quicksort(rows, 0, rows.size() - 1);

		return rows;
	}

	public static void quicksort(List<Statement_Row> rows, Integer pStart, Integer pEnd){
        Integer low = pStart;
        Integer hi = pEnd;

        if (low >= hi) {
            return;
        }
		else if( low == hi - 1 ) {
            if (rows[low].SortingDate > rows[hi].SortingDate) {
                Statement_Row T = rows[low];
                rows[low] = rows[hi];
                rows[hi] = T;
            }

            return;
        }

        Statement_Row pivot = rows[(low + hi) / 2];
        rows[(low + hi) / 2] = rows[hi];
        rows[hi] = pivot;

        while(low < hi) {
            while (rows[low].SortingDate <= pivot.SortingDate && low < hi){
                low++;
            }

            while (pivot.SortingDate <= rows[hi].SortingDate && low < hi ){
                hi--;
            }

            if( low < hi ){
                Statement_Row T = rows[low];
                rows[low] = rows[hi];
                rows[hi] = T;
            }
        }

        rows[pEnd] = rows[hi];
        rows[hi] = pivot;
		
        quicksort(rows, pStart, low-1);
        quicksort(rows, hi+1, pEnd);
    }


	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/10/2017
	* @description 	 - Retrieves a Map where:
	* 				   Key: Account.Id
	* 				   Key: Account.AcctSeed__Billing_Contact__c
	*/
	public static Map<Id, Account> getBillingContacts(List<Id> accountId) {
		List<Account> accounts = [SELECT Id, AcctSeed__Billing_Contact__c, Last_Statement_Generation_Date__c FROM Account WHERE Id IN: accountId];

		Map<Id, Account> result = new Map<Id, Account>();

		for(Account account : accounts) {
			result.put(account.Id, account);
		}

		return result;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/10/2017
	* @description 	 - Utility method to calculate the next statement date of a list of Accounts
	*/
	//@InvocableMethod(label='Update Account Next Statement Date' description='Utility method to calculate the next statement date of a list of Accounts')
	public static void updateNextStatementDate(List<Account> accounts_List, Boolean updateDirectly) { //List<Id> accountIds
		/*
		List<Account> accounts_List = [SELECT 
											Id,
											Statement_Frequency__c,
											Statement_Day__c,											
											Next_Statement_Date__c
										FROM
											Account
										WHERE
											Id IN: accountIds];
		*/

		Map<String, Integer> day_number = new Map<String, Integer>();
		day_number.put('Sunday', 0);
		day_number.put('Monday', 1);
		day_number.put('Tuesday', 2);
		day_number.put('Wednesday', 3);
		day_number.put('Thursday', 4);
		day_number.put('Friday', 5);
		day_number.put('Saturday', 6);

		Date dToday = Date.today();

		for(Account account : accounts_List) {
			if(account.Statement_Frequency__c == 'Weekly') {
				if(account.Statement_Day__c != NULL) {
					String dayOfWeek = getDayOfWeek(dToday);

					Integer numberToday = day_number.get(dayOfWeek);
					Integer numberStatementDay = day_number.get(account.Statement_Day__c);

					Integer diff = numberToday - numberStatementDay;

					if(diff < 0) {
						account.Next_Statement_Date__c = dToday.addDays(Math.abs(diff));
					}
					else if(diff > 0) {
						account.Next_Statement_Date__c = dToday.addDays(7 - Math.abs(diff));
					}
					else {
						account.Next_Statement_Date__c = dToday;
					}
				}
			}
			else if(account.Statement_Frequency__c == 'Monthly') {
				if(account.Statement_Day__c != NULL) {
					
					Integer lastDay = Date.daysInMonth(dToday.year(), dToday.month());
					Integer parameterDay = Integer.valueOf(account.Statement_Day__c);
					
					Date nextDate = Date.newInstance(dToday.year(), dToday.month(), Math.Min(lastDay, parameterDay));

					if(nextDate < dToday) {
						//add 1 month to the calculated date
						Date nextMonthDate = nextDate.addMonths(1);

						lastDay = Date.daysInMonth(nextMonthDate.year(), nextMonthDate.month());
						//take the same year and month calculated and set the day to the value originally set
						nextDate = Date.newInstance(nextMonthDate.year(), nextMonthDate.month(), Math.Min(lastDay, parameterDay));
					}

					account.Next_Statement_Date__c = nextDate;
				}
			}
		}

		if(updateDirectly)
			update accounts_List;
	}

	/* UTILITY METHODS */

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/29/2017
	* @description 	 - Utility method to group a List<Statement__c> by Account
	* @returns 		 - Returns a map with format:
	* 				   Key: Account.Id
	* 				   Value: List of statements of the Account
	*/
	private static Map<Id, List<Statement__c>> convertListToMap(List<Statement__c> statements) {
		Map<Id, List<Statement__c>> result = new Map<Id, List<Statement__c>>();

		for(Statement__c statement : statements) {
			if(!result.containsKey(statement.Account__c))
				result.put(statement.Account__c, new List<Statement__c>());

			result.get(statement.Account__c).add(statement);
		}

		return result;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/29/2017
	* @description 	 - Utility method to group a List<AcctSeed__Billing__c> by Account
	* @returns 		 - Returns a map with format:
	* 				   Key: Account.Id
	* 				   Value: List of AcctSeed__Billing__c of the Account
	*/
	@TestVisible
	private static Map<Id, List<AcctSeed__Billing__c>> convertListToMap(List<AcctSeed__Billing__c> billings) {
		Map<Id, List<AcctSeed__Billing__c>> result = new Map<Id, List<AcctSeed__Billing__c>>();

		for(AcctSeed__Billing__c billing : billings) {
			if(!result.containsKey(billing.AcctSeed__Customer__c))
				result.put(billing.AcctSeed__Customer__c, new List<AcctSeed__Billing__c>());

			result.get(billing.AcctSeed__Customer__c).add(billing);
		}

		return result;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/29/2017
	* @description 	 - Utility method to group a List<AcctSeed__Cash_Receipt__c> by Account
	* @returns 		 - Returns a map with format:
	* 				   Key: Account.Id
	* 				   Value: List of AcctSeed__Cash_Receipt__c of the Account
	*/
	@TestVisible
	private static Map<Id, List<AcctSeed__Cash_Receipt__c>> convertListToMap(List<AcctSeed__Cash_Receipt__c> cashReceipts) {
		Map<Id, List<AcctSeed__Cash_Receipt__c>> result = new Map<Id, List<AcctSeed__Cash_Receipt__c>>();

		for(AcctSeed__Cash_Receipt__c cashRceipt : cashReceipts) {
			if(!result.containsKey(cashRceipt.AcctSeed__Account__c))
				result.put(cashRceipt.AcctSeed__Account__c, new List<AcctSeed__Cash_Receipt__c>());

			result.get(cashRceipt.AcctSeed__Account__c).add(cashRceipt);
		}

		return result;
	}

	public static String getDayOfWeek(Date pDate) {
		Datetime dt = Datetime.newInstance(pDate.year(), pDate.month(), pDate.day());

		return dt.format('EEEE');
	}

	public static Map<Id, AcctSeed__Billing__c> mergeMapsByKey(Map<Id, AcctSeed__Billing__c> map1, Map<Id, AcctSeed__Billing__c> map2) {
		if(map1 == NULL && map2 == NULL)
			return new Map<Id, AcctSeed__Billing__c>();
		else if(map1 == NULL && map2 != NULL)
			return map2;
		else if(map1 != NULL && map2 == NULL)
			return map1;

		Map<Id, AcctSeed__Billing__c> result = new Map<Id, AcctSeed__Billing__c>();

		for(Id billingId : map1.keySet()) {
			result.put(billingId, map1.get(billingId));
		}

		for(Id billingId : map2.keySet()) {
			result.put(billingId, map2.get(billingId));
		}

		return result;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/04/2017
	* @description 	 - 
	*/
	public static Date parseDate(String dt) {
		List<String> dstr = dt.split('/');

		return Date.newInstance(
			Integer.valueOf(dstr.get(0)),
			Integer.valueOf(dstr.get(1)),
			Integer.valueOf(dstr.get(2)));
	}

	public static Date parseDateByTicks(String ticks) {
		try {
			Datetime dt = Datetime.newInstance(Long.valueOf(ticks));

			return dt.date();
		}
		catch (Exception ex) { 
			System.debug('parseDateByTicks-Error-->> ' + ex.getMessage());
		}

		return NULL;
	}

	/* END UTILITY METHODS */


	public static List<Account> LoadAccounts(StatementManagement_Filters searchTerms) {
		String query = 'SELECT ' + 
							'Id, ' +
							'Name, ' +
							'Phone, ' +
							'ShippingStreet, ' +
							'ShippingCity, ' +
							'ShippingState, ' +
							'ShippingPostalCode, ' +
							'ShippingCountry, ' +
							
							'Route__c, ' +
							'Route__r.Name, ' + 
							'Balance__c, ' +
							'Level__c, ' +
							'Customer_Group__c, ' +
							'AcctSeed__Billing_Contact__c, ' +
							'( ' +
								'SELECT ' +
									'Id ' +
								'FROM ' +
									'Statements__r ' +
								'WHERE ' +
									'Type__c = \'' + searchTerms.StatementType + '\' ';

		if(searchTerms.StartDate != NULL) {
			String stDate = String.valueOf(searchTerms.StartDate);
			stDate = stDate.split(' ')[0];

			query += ' AND Period_Start_Date__c = ' + stDate;
		}
		
		if(searchTerms.EndDate != NULL) {
			String endDate = String.valueOf(searchTerms.EndDate);
			endDate = endDate.split(' ')[0];

			query += ' AND Period_End_Date__c = ' + endDate;
		}
		
		query += ') FROM Account ';

		String whereClause = ' Route__c != NULL AND Level__c != \'INACTIVE\' AND ';

		if(!String.isBlank(searchTerms.AccountId))
			whereClause += ' Id = \'' +  searchTerms.AccountId + '\'';
		else {
			if(!String.isBlank(searchTerms.PaymentTerm))
				whereClause += ' Payment_Terms__c =\'' +  searchTerms.PaymentTerm + '\' AND ';

			if(!String.isBlank(searchTerms.CustomerLevel))
				whereClause += ' Level__c =\'' +  searchTerms.CustomerLevel + '\' AND ';

			if(!String.isBlank(searchTerms.CustomerGroup))
				whereClause += ' Customer_Group__c =\'' +  searchTerms.CustomerGroup + '\' AND ';

			if(!String.isBlank(searchTerms.Route))
				whereClause += ' Route__r.Name =\'' +  searchTerms.Route + '\' AND ';

			if(!String.isBlank(searchTerms.RouteId))
				whereClause += ' Route__c =\'' +  searchTerms.RouteId + '\' AND ';
		}

		if(whereClause.endsWith('AND '))
			whereClause = whereClause.removeEnd('AND ');

		if(!String.isBlank(whereClause))
			query += 'WHERE ' + whereClause;

		system.debug('---------------------------------------- Statements Query: ' + query);
		//LastQuery = query;

		List<Account> result = Database.query(query);

		return result;
	}

	global class StatementManagement_Filters {
		public String AccountId { get; set; }
		public String PaymentTerm { get; set; }
		public String CustomerLevel { get; set; }
		public String CustomerGroup { get; set; }
		public String Route { get; set; }
		public String RouteId { get; set; }

		public String StatementType { get; set; }
		public Date StartDate { get; set; }
		public Date EndDate { get; set; }

		public String StatementId { get; set; }
		public Boolean OnlyNoEmailReady { get; set; }
		public Boolean GenerateStatements { get; set; }

		public StatementManagement_Filters() {
			this.OnlyNoEmailReady = false;
			this.GenerateStatements = false;
		}
	}

	public static StatementManagement_Filters ParsePageParams(Map<String, String> params) {
		StatementManagement_Filters result = new StatementManagement_Filters();

		if (params.containsKey('id')) {
			result.AccountId = params.get('id');
		}
		if(params.containsKey('sid')) {
			result.StatementId = params.get('sid');
		}
		if (params.containsKey('pt')) {
			result.PaymentTerm = params.get('pt');
		}
		if (params.containsKey('cl')) {
			result.CustomerLevel = params.get('cl');
		}
		if (params.containsKey('cg')) {
			result.CustomerGroup = params.get('cg');
		}
		if (params.containsKey('rid')) {
			result.RouteId = params.get('rid');
		}

		if (params.containsKey('st')) {
			result.StatementType = params.get('st');
		}
		if (params.containsKey('sdate')) {
			result.StartDate = StatementManager_Helper.parseDate(params.get('sdate'));
		}
		if (params.containsKey('edate')) {
			result.EndDate = StatementManager_Helper.parseDate(params.get('edate'));
		}

		if (params.containsKey('ner')) {
			result.OnlyNoEmailReady = params.get('ner') == '1';
		}
		if (params.containsKey('gs')) {
			result.OnlyNoEmailReady = params.get('gs') == '1';
		}

		return result;
	}

	public static List<Statement__c> getExistingStatements(List<Statement__c> stsToInsert, String sType) {
		String conds = '';
		for(Statement__c st : stsToInsert)
			conds += getDateCondition(st.Account__c, st.Period_Start_Date__c, st.Period_End_Date__c);
		
		return database.query(
			'SELECT Id FROM Statement__c WHERE Type__c = \'' + sType + '\' AND (' + conds.removeEnd('OR ') + ')');
	}

	private static String getDateCondition(String accId, Date i, Date f) {
		return (' ( Account__c = \'' + accId + '\' AND Period_Start_Date__c =  ' + i + 
			' AND Period_End_Date__c = ' + f + ' ) OR ').remove('00:00:00');
	}
}