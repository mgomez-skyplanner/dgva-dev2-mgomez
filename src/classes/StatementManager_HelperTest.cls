/**
* <p> 
* Version log:
* <ul>
* <li>1.00 10/02/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 10/02/2017
* @description 	 - Test class for class StatementManager_Helper
* @version
* 	1.01 10/26/2017 William Garcia: Statement develoment, continuation...
* 
*/

@isTest
public class StatementManager_HelperTest {
	
	@testSetup
	static void setup() {
		TestClass_Utility.createCustomSettingsData();

		Route__c route 	= TestClass_Utility.createRoute('Test Route');
		insert route;

		Account account = TestClass_Utility.createAccount('Test Account', route);
		insert account;

		Opportunity opp = TestClass_Utility.createOpportunity(account);
		insert opp;

		Order order = TestClass_Utility.createOrder(account, opp, 'Draft');
		insert order;

		Product2 product1 = TestClass_Utility.createProduct('Test Product 1');
		insert product1;

		Id standardPriceBookId = Test.getStandardPricebookId();
		PricebookEntry pbe 		= [SELECT Id FROM PricebookEntry WHERE Product2Id =: product1.Id AND PriceBook2Id =: standardPriceBookId];

		OrderItem orderProduct1 = TestClass_Utility.createOrderProduct(order, pbe);
		insert orderProduct1;

		TestClass_Utility.CreateDefaultDataForBillings(account, order, product1);

		//Mandatory setting for Accounting Seed
		AcctSeed__Billing_Format__c pdfFormat = TestClass_Utility.createPDFFormat('Test PDF Format', 'Billing', 'AccountingHome', 'OrderConfirmation');
		insert pdfFormat;

		order.Status = 'Prepared';
		update order;
		order.Status = 'Activated';
		update order;

		//reload order
		BillingManager.LoadCustomSettings();
		order = BillingManager.ReloadOrder(order.Id);

		AcctSeed__Billing__c billing = BillingManager.GenerateBillingFromOrder(order, true);

		AcctSeed__Accounting_Period__c accountingPeriod = [SELECT Id FROM AcctSeed__Accounting_Period__c LIMIT 1];

		AcctSeed__Cash_Receipt__c cashReceipt = TestClass_Utility.createCashReceipt(account.id, system.today(), 100);
		insert cashReceipt;

		insert TestClass_Utility.applyCashReceiptToBilling(cashReceipt, billing, 20);		
	}

	private static Account LoadAccount() {
		return [SELECT 
					Id,
					Statement_Frequency__c,
					Statement_Day__c,
					(
						SELECT
							Id,
							Balance__c,
							CreatedDate,
							Period_End_Date__c,
							Period_Start_Date__c,
							Type__c
						FROM
							Statements__r
					)
				FROM
					Account];
	}
	
	@isTest
	static void test_getStatementsMethods() {
		Test.startTest();

		Account account = LoadAccount();

		List<Id> accIds = new List<Id>{account.Id};

		List<Statement__c> statements = TestClass_Utility.generateStatementsForAccount(account.Id, StatementManager_Helper.StatementType.Outstanding);
		insert statements;

		Test.stopTest();

		Date startDate = statements[0].Period_Start_Date__c;
		Date createdDate = (Date)statements[0].CreatedDate;
		Date endDate = Date.today().addYears(100);

		system.debug('------------- start date: ' + startDate);
		system.debug('------------- end date: ' + endDate);


		Map<Id, List<Statement__c>> all = StatementManager_Helper.GetAllStatements(accIds);
		system.assertEquals(statements.size(), all.get(account.Id).size());

		Map<Id, Statement__c> previous = StatementManager_Helper.GetPreviousStatement(accIds, createdDate);
		system.assert(previous.get(account.Id) != NULL);

		Map<Id, List<Statement__c>> outstanding = StatementManager_Helper.GetStatementsBy_Type(accIds, StatementManager_Helper.StatementType.Outstanding);
		system.assertEquals(all.get(account.Id).size(), outstanding.get(account.Id).size());

		Map<Id, List<Statement__c>> inPeriod = StatementManager_Helper.GetStatementsBy_Period(accIds, startDate, endDate);
		system.assertEquals(all.get(account.Id).size(), inPeriod.get(account.Id).size());

		Map<Id, List<Statement__c>> typeInPeriod = StatementManager_Helper.GetStatementsBy_Type_Period(accIds, 
			StatementManager_Helper.StatementType.Outstanding, startDate, endDate);
		system.assertEquals(all.get(account.Id).size(), typeInPeriod.get(account.Id).size());

		account.Statement_Frequency__c = 'Weekly';
		account.Statement_Day__c = 'Monday';
		update account;

	}
	
	@isTest
	static void test_getStatementsMethods2() {
		
		List<Statement_Row> statementRows = new List<Statement_Row>();
		AcctSeed__Billing__c billing = [SELECT Name,
												CreatedDate,
												AcctSeed__Customer__c,
												AcctSeed__Date__c,
												AcctSeed__Proprietary_Billing_Number__c,
												AcctSeed__PO_Number__c,
												AcctSeed__Due_Date2__c,
												AcctSeed__Total__c,
												AcctSeed__Balance__c,
												AcctSeed__Type__c 
										from AcctSeed__Billing__c];
		Statement_Row fromBilling = StatementManager_Helper.CreateRowFromBilling(billing, 1, StatementManager_Helper.StatementActivity.Credit);
		statementRows.add(fromBilling);

		AcctSeed__Cash_Receipt__c cashReceipt = [SELECT Name,
														CreatedDate,
														AcctSeed__Account__c,
														AcctSeed__Balance__c,
														AcctSeed__Receipt_Date__c,
														AcctSeed__Amount__c,
														AcctSeed__Purpose__c,
														AcctSeed__Payment_Reference__c
												from AcctSeed__Cash_Receipt__c];

		Statement_Row fromCashReceipt = StatementManager_Helper.CreateRowFromCashReceipt(cashReceipt, 
																						 cashReceipt.AcctSeed__Amount__c,
																						 StatementManager_Helper.StatementActivity.Credit);
		statementRows.add(fromCashReceipt);


		AcctSeed__Billing_Cash_Receipt__c cashBillingReceipt = [SELECT Name,
																	AcctSeed__Cash_Receipt__r.AcctSeed__Account__c,
																	CreatedDate,
																	AcctSeed__Cash_Receipt__r.Name,
																	AcctSeed__Billing__r.AcctSeed__PO_Number__c,
																	AcctSeed__Applied_Amount__c,
																	AcctSeed__Cash_Receipt__r.AcctSeed__Payment_Reference__c,																
																	AcctSeed__Cash_Receipt__r.AcctSeed__Purpose__c																	
																from AcctSeed__Billing_Cash_Receipt__c];

		Statement_Row fromBillingCashReceipt = StatementManager_Helper.CreateRowFromBillingCashReceipt(cashBillingReceipt, StatementManager_Helper.StatementActivity.Credit);
		statementRows.add(fromBillingCashReceipt);

		StatementManager_Helper.quicksort(statementRows, 0, 2);
		StatementManager_Helper.SortStatementsBubble(new Statement_Row[]{fromCashReceipt, fromCashReceipt, fromBillingCashReceipt});

		StatementManager_Helper.convertListToMap(new AcctSeed__Billing__c[]{billing});
		StatementManager_Helper.convertListToMap(new AcctSeed__Cash_Receipt__c[]{cashReceipt});


		//Test merging maps
		
		StatementManager_Helper.mergeMapsByKey(new Map<Id, AcctSeed__Billing__c>{billing.id => billing}, 
											   new Map<Id, AcctSeed__Billing__c>{billing.id => billing});

		//Parse Dates
		Date todayParsed = StatementManager_Helper.parseDate(system.now().format('YYYY/MM/dd'));
		system.assertEquals(system.today(), todayParsed);

		//ParsePageParams
		Map<String, String> params = new Map<String, String>{

			'id' => '',
			'sid' => '',
			'pt' => '',
			'cl' => '',		
			'cg' => '',
			'rid' => '',
			'st' => 'Outstanding',
			'sdate' => system.now().format('YYYY/MM/dd'),
			'edate' => system.now().format('YYYY/MM/dd'),
			'ner' => '1',
			'gs' => '1'
		};
		StatementManager_Helper.ParsePageParams(params);
	}
}