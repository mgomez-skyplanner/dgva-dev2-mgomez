/**
* @author    	 - Marcel Gomez
* @date  	     - 06/07/2017
* @description 	 - This class contains test methods for:
* 					- class OrderConversionUtility (92% - 079/13/2017)
* 					- trigger OrderConversion_Trigger (96% - 07/05/2017)
* 					- trigger OrderProductConversion_Trigger (96% - 07/05/2017)
*/

@isTest
public with sharing class OrderConversionUtility_Test {

	@testSetup
	static void setup() {

		TestClass_Utility.createCustomSettingsData();

		Route__c route 	= TestClass_Utility.createRoute('Test Route');
		insert route;

		Account account = TestClass_Utility.createAccount('Test Account', route);
		insert account;

		Opportunity opp = TestClass_Utility.createOpportunity(account);
		insert opp;

		Order order 			= TestClass_Utility.createOrder(account, opp, 'Draft');
		insert order;

		Product2 product1 		= TestClass_Utility.createProduct('Test Product 1');
		insert product1;

		Id standardPriceBookId 	= Test.getStandardPricebookId();
		PricebookEntry pbe 		= [SELECT Id FROM PricebookEntry WHERE Product2Id =: product1.Id AND PriceBook2Id =: standardPriceBookId];

		OrderItem orderProduct1 = TestClass_Utility.createOrderProduct(order, pbe);
		insert orderProduct1;

		TestClass_Utility.CreateDefaultDataForBillings(account, order, product1);
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Performs assertions for test method test_ConvertOrder_To_SalesOrder
	*/
	static void performOrder2SalesOrderAssertions(Order order, AcctSeedERP__Sales_Order__c salesOrder) {
		List<Order_Conversion_Fields__c> fieldsList = Order_Conversion_Fields__c.getall().values();

		for(Order_Conversion_Fields__c field : fieldsList) {
			String orderField = field.Name;
			String salesOrderField = field.Sales_Order_Field__c;

			if(orderField == 'Status') {
				String convertedStatus = OrderConversionUtility.ConvertOrderStatus_To_SalesOrderStatus(order.Status);
				
				system.assertEquals(convertedStatus, salesOrder.get(salesOrderField));
			}
			else {
				system.assertEquals(order.get(orderField), salesOrder.get(salesOrderField));
			}
		}
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - Test method for methods of class OrderConversionUtility
	*/
	@isTest
	static void test_ConvertOrder_To_SalesOrder() {
		Test.startTest();

		Order order = [SELECT 
							Id,
							Status,
							Sales_Order__c,
							(
								SELECT 
									Id
								FROM
									OrderItems
							)
						FROM
							Order];

		Test.stopTest();

		AcctSeedERP__Sales_Order__c salesOrder = OrderConversionUtility.Sync_Order_SalesOrder(order);

		order = OrderConversionUtility.ReloadOrder(order);

		system.assertEquals(order.Id, salesOrder.Order__c);
		system.assertEquals(salesOrder.Id, order.Sales_Order__c);

		performOrder2SalesOrderAssertions(order, salesOrder);

		order.Status = 'Prepared';
		update order;
		
		order = OrderConversionUtility.ReloadOrder(order);

		OrderConversionUtility.LockOrder(order);
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - Test method for methods of class OrderConversionUtility
	*/
	@isTest
	static void test_VoidOrderProduct() {
		Test.startTest();

		Order order = [SELECT 
							Id,
							Status,
							Sales_Order__c,
							(
								SELECT 
									Id
								FROM
									OrderItems
							)
						FROM
							Order];

		order = OrderConversionUtility.ReloadOrder(order);

		Test.stopTest();

		OrderItem orderProduct1 = order.OrderItems[0];

		orderProduct1.Quantity = orderProduct1.Quantity + 5;
		update orderProduct1;

		order.Status = 'Prepared';
		update order;
		
		order = OrderConversionUtility.ReloadOrder(order);

		OrderConversionUtility.OrderProductVoid_Wrapper opWrapper = new OrderConversionUtility.OrderProductVoid_Wrapper();
		opWrapper.OrderProduct 	 = orderProduct1;
		opWrapper.QuantityToVoid = 1;
		opWrapper.Explanation 	 = 'explanation';

		OrderConversionUtility.VoidOrderProduct(order, new List<OrderConversionUtility.OrderProductVoid_Wrapper> { opWrapper });

		order = OrderConversionUtility.ReloadOrder(order);

		order.Status = 'Delivered';
		update order;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/05/2017
	* @description 	 - Test method for part of trigger OrderConversion_Trigger that sets SalesOrder.Status = 'Cancelled' when Order.Status = 'Cancelled'
	*/
	@isTest
	static void test_CancelOrder_CancelSalesOrder() {
		Test.startTest();

		Order order = [SELECT Id, Status FROM Order];

		Test.stopTest();

		AcctSeedERP__Sales_Order__c salesOrder = OrderConversionUtility.Sync_Order_SalesOrder(order);

		order = OrderConversionUtility.ReloadOrder(order);

		order.Status = 'Cancelled';
		update order;
	}
}