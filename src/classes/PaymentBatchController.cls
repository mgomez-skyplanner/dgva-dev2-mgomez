/**
* <p>
* Version log:
* <ul>
* <li>1.00 07/10/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 07/10/2017
* @description 	 - Represents an extension controller to provide actions related to the Payment Batch object
*/
 
global with sharing class PaymentBatchController {

	public static String LastQuery {get;set;}

	public PaymentBatchController() {
		
	}

	public PaymentBatchController(ApexPages.StandardController stdController) {
		
	}

	public void CalculateCurrentHost() {
		
	}

	/**
	 * Custom setting for configurations 
	 * @author William Garcia
	 * @date   09/01/2017
	 */
	public String settings {
		get{
			return JSON.serialize(PaymentBatchSettings__c.getOrgDefaults());
		}
		set;}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/10/2017
	* @description 	 - Action to load all initial search values for page PaymentBatchBuilderPage
	* @return      	 - A custom class instance with all necessary values
	*/
	@AuraEnabled
	@RemoteAction
	webservice static PaymentBatchSearchInitialValues LoadInitialValues() {
		PaymentBatchSearchInitialValues result = new PaymentBatchSearchInitialValues();

		result.PaymentTerms 	= LoadPaymentTerms();
		result.EndDate 			= Date.today();
		result.CustomerLevels 	= LoadCustomerLevels();
		result.CustomerGroups 	= LoadCustomerGroups();

		return result;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/10/2017
	* @description 	 - Action to search Accounts given a search criteria
	* @param       	 - paymentTerm: the Payment Term of the Account
	* 				 - paymentTerm: the End Date of the Orders of the Account
	* 				 - level: the Level of the Account
	* 				 - cGroup: the Customer Group of the Account
	*/
	@AuraEnabled
	@RemoteAction
	webservice static List<AccountRecordWrapper> searchAccounts(String paymentTerm, String endDate, String level, String cGroup) {
		PaymentBatchSearchValues searchTerms = new PaymentBatchSearchValues();
		searchTerms.PaymentTerm 	= paymentTerm;
		searchTerms.CustomerLevel 	= level;
		searchTerms.CustomerGroup 	= cGroup;

		String subDate = endDate.substring(0, endDate.indexOf('T'));
		searchTerms.EndDate 		= subDate;

		Boolean refreshBalance = true;

		List<Account> accounts = LoadAccounts(searchTerms, refreshBalance);

		if(refreshBalance) {
            
            /* Commented on 11/19/2017 by JLF to prevent calculating the Account Balance.
            
                //this call forces the Accounts to refresh their Balances before showing on the UI to prevent discordance between Account.Balance and Sum(Billings.Balance)
                Set<Id> accountIds = new Set<Id>();		
                for(Account acc : accounts) {
                    accountIds.add(acc.Id);
                }
    
                AccountBalances_Utility.UpdateAccountBalances(accountIds);
    
                accounts = LoadAccounts(searchTerms, false);
			*/
		}

		List<AccountRecordWrapper> result = new List<AccountRecordWrapper>();
		for(Account account : accounts) {
			if(account.AcctSeed__Billings__r.size() > 0) {
				AccountRecordWrapper aWrapper = new AccountRecordWrapper();
				
				aWrapper.Account = account;
				aWrapper.Checked = false;
				aWrapper.Expanded = false;

				List<BillingRecordWrapper> billings = new List<BillingRecordWrapper>();

				for(AcctSeed__Billing__c billing : account.AcctSeed__Billings__r) {
					BillingRecordWrapper bWrapper = new BillingRecordWrapper();

					bWrapper.Billing 	= billing;
					bWrapper.Checked 	= false;

					Datetime dt = Datetime.newInstance(billing.AcctSeed__Due_Date2__c.year(), billing.AcctSeed__Due_Date2__c.month(), billing.AcctSeed__Due_Date2__c.day());
					bWrapper.EndDate 	= dt.format('MMM d, yyyy');

					billings.add(bWrapper);
				}

				aWrapper.Billings = billings;
				//aWrapper.Query = LastQuery;

				result.add(aWrapper);
			}
		}

		return result;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/10/2017
	* @description 	 - Creates Payment_Batch__c records with corresponding Payment_Batch_Entry__c records based on user selection and invokes the batch PaymentBatchProcessor
	* @param       	 - orderIdList: list of Order Ids to process related Billings
	*/
	@AuraEnabled
	@RemoteAction
	webservice static String processPayments(List<String> billingIdList) {
		if(billingIdList.size() == 0)
			return 'You must select at least one Order to process payment';

		List<AcctSeed__Billing__c> billings = [SELECT
													Id,
													Order__c,
													AcctSeed__Balance__c,
													Payment_Batch_Entry__c
												FROM
													AcctSeed__Billing__c
												WHERE
													AcctSeed__Balance__c > 0 AND 
													//(Order__c = NULL OR Order__r.Status = 'Activated') AND 
													(Payment_Batch_Entry__c = NULL OR Payment_Batch_Entry__r.Status__c = 'Failed') AND 								
													Id IN: billingIdList ];

		if(billings.size() == 0)
			return 'No Orders found to process payment';

		try {
			//create the Payment Batch (parent) object
			Payment_Batch__c pBatch = new Payment_Batch__c();
			pBatch.Status__c = 'Pending';

			insert pBatch;

			List<Payment_Batch_Entry__c> entries = new List<Payment_Batch_Entry__c>();
			Map<Id, Payment_Batch_Entry__c> billingId_Entry_Map = new Map<Id, Payment_Batch_Entry__c>();

			for(AcctSeed__Billing__c billing : billings) {
				//create new entry
				Payment_Batch_Entry__c entry = new Payment_Batch_Entry__c();

				entry.Billing__c 		= billing.Id;
				entry.Amount__c  		= billing.AcctSeed__Balance__c;
				entry.Status__c  		= 'Pending';
				entry.Payment_Batch__c 	= pBatch.Id;
				
				entries.add(entry);

				billingId_Entry_Map.put(billing.Id, entry);
			}

			insert entries;

			List<AcctSeed__Billing__c> billings2Update = new List<AcctSeed__Billing__c>();
			for(AcctSeed__Billing__c billing : billings) {
				if(billingId_Entry_Map.containsKey(billing.Id)) {
					billing.Payment_Batch_Entry__c = billingId_Entry_Map.get(billing.Id).Id;

					billings2Update.add(billing);
				}
			}

			update billings2Update;

			//start processing payments batch
			PaymentBatchProcessor pBatchProcessor = new PaymentBatchProcessor(entries);
        	Database.executeBatch(pBatchProcessor, 1);
		}
		catch(Exception e) {
			return e.getMessage();
		}

		return '1';
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/10/2017
	* @description 	 - Creates Payment_Batch__c records with corresponding Payment_Batch_Entry__c records based on user selection and invokes the batch PaymentBatchProcessor
	* @param       	 - orderIdList: list of Order Ids to process related Billings
	*/
	@RemoteAction
	webservice static String processPaymentButton(String paymentBatchId) {
		List<Payment_Batch_Entry__c> entries = [SELECT
													Id,
													Payment_Batch__c,
													Billing__c,
													Billing__r.AcctSeed__Customer__c,
													Billing__r.AcctSeed__Accounting_Period__c,
													Billing__r.AcctSeed__Balance__c
												FROM
													Payment_Batch_Entry__c
												WHERE
													Payment_Batch__c =: paymentBatchId];

		if(entries.size() == 0)
			return 'No Orders found to process payment';

		try {
			//start processing payments batch
			PaymentBatchProcessor pBatchProcessor = new PaymentBatchProcessor(entries);
        	Database.executeBatch(pBatchProcessor, 10);
		}
		catch(Exception e) {
			return e.getMessage();
		}

		return '1';
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/10/2017
	* @description 	 - Action to load all values of picklist field Account.Payment_Terms__c
	* @return      	 - List<SelectOption> with all available values for field Account.Payment_Terms__c
	*/
	public static List<SelectOption> LoadPaymentTerms() {
		return LoadPicklistValues(Account.Payment_Terms__c.getDescribe());
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/10/2017
	* @description 	 - Action to load all values of picklist field Account.Customer_Group__c
	* @return      	 - List<SelectOption> with all available values for field Account.Customer_Group__c
	*/
	public static List<SelectOption> LoadCustomerGroups() {
		return LoadPicklistValues(Account.Customer_Group__c.getDescribe());
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/10/2017
	* @description 	 - Action to load all values of picklist field Account.Level__c
	* @return      	 - List<SelectOption> with all available values for field Account.Level__c
	*/
	public static List<SelectOption> LoadCustomerLevels() {
		return LoadPicklistValues(Account.Level__c.getDescribe());
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/10/2017
	* @description 	 - Utility method to load available values of a picklist field
	* @param 	 	 - The Schema.DescribeFieldResult that results from invoking method SObject.FieldName.getDescribe()
	* @return      	 - List<SelectOption> with all available values for field Account.Level__c
	*/
	public static List<SelectOption> LoadPicklistValues(Schema.DescribeFieldResult fieldResult) {
		List<SelectOption> result = new List<SelectOption>{new SelectOption('', '')};

		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

	   	for( Schema.PicklistEntry f : ple) {
	    	result.add(new SelectOption(f.getLabel(), f.getValue()));
	   	}

	   return result;		
	}

	private static List<Account> LoadAccounts(PaymentBatchSearchValues searchTerms, Boolean refreshBalance) {
		String query = 'SELECT ' + 
							'Id, ' +
							'Name, ' +
							'Balance__c, ' +
							'Level__c, ' +
							'Customer_Group__c, ' +
							'AcctSeed__Stripe_Customer_Id__c, ' +
							'( ' +
								'SELECT ' +
									'Id, ' +
									'Order__c, ' +
									//'Order__r.OrderNumber, ' +
									'Name, ' + 
									//'Order__r.EndDate, ' +
									'AcctSeed__Due_Date2__c, ' +
									//'Order__r.TotalAmount ' +
									'AcctSeed__Balance__c ' + 
								'FROM ' +
									'AcctSeed__Billings__r ' +
								'WHERE ' +
									'AcctSeed__Balance__c > 0 AND ' +
									'(Payment_Batch_Entry__c = NULL OR Payment_Batch_Entry__r.Status__c = \'Failed\') ';
									//'Order__r.Status = \'Activated\' AND ';

		if(!String.isBlank(searchTerms.EndDate))
			query += ' AND AcctSeed__Due_Date2__c <= ' + searchTerms.EndDate;
		
		query += ' ORDER BY Order__r.EndDate ASC ) ' +
				'FROM ' +
					'Account ';

		String whereClause = '';

		if(!String.isBlank(searchTerms.PaymentTerm))
			whereClause += ' Payment_Terms__c =\'' +  searchTerms.PaymentTerm + '\' AND ';

		if(!String.isBlank(searchTerms.CustomerLevel))
			whereClause += ' Level__c =\'' +  searchTerms.CustomerLevel + '\' AND ';

		if(!String.isBlank(searchTerms.CustomerGroup))
			whereClause += ' Customer_Group__c =\'' +  searchTerms.CustomerGroup + '\' ';

		if(whereClause.endsWith('AND '))
			whereClause = whereClause.removeEnd('AND ');

		if(!String.isBlank(whereClause))
			query += 'WHERE ' + whereClause;

		LastQuery = query;

		system.debug('---------------------------------------- Accounts Query: ' + query);

		List<Account> result = Database.query(query);

		return result;
	}

	global class PaymentBatchSearchInitialValues {
		public List<SelectOption> PaymentTerms { get; set; }
		public Date EndDate { get; set; }
		public List<SelectOption> CustomerLevels { get; set; }
		public List<SelectOption> CustomerGroups { get; set; }
	}

	global class PaymentBatchSearchValues {
		public String PaymentTerm { get; set; }
		public String EndDate { get; set; }
		public String CustomerLevel { get; set; }
		public String CustomerGroup { get; set; }
	}

	global class AccountRecordWrapper {
		public Account Account { get; set; }
		public Boolean Checked { get; set; }
		public Boolean Expanded { get; set; }

		public List<BillingRecordWrapper> Billings { get; set; }
		public String Query { get; set; }
	}

	global class BillingRecordWrapper {
		public AcctSeed__Billing__c Billing { get; set; }
		
		public String EndDate { get; set; }
		public Boolean Checked { get; set; }
	}
}