/**
 * Handles all interaction with order. Creation, activation, cancellation,
 * business processes, and all other items related directly to orders
 * will be provided by this class.
 * @author Fernando Gomez, SkyPlanner LLC
 * @date May 8th, 2017
 * @version 1.0.0 Initial version.
 */
global without sharing class OrderManager {
	private static final String STATUS_ACTIVATED = 'Activated';
    private static final String STATUS_DELIVERED = 'Delivered';
	private static final String TYPE_REGULAR = 'Regular';

	/**
	 * Main constructor.
	 * @author Fernando Gomez, SkyPlanner LLC
	 * @date May 8th, 2017
	 */
	global OrderManager() {
		
	}

	/**
	 * Returns all orders by delivery date and activated before or at the datetime specified
	 * @author Marcel Gomez
	 * @date 09/16/2017
	 */
	global List<Order> fetchByActivationDate(Date deliveryDate, Datetime activationDate) {
		return [
			SELECT
				Id,
				OrderNumber,
				Status,
            	EffectiveDate,
				EndDate,
				PoNumber,
				Order_Total__c,
				AccountId,
				Account.Name,
            	Account.Account_Number__c,
				Account.ShippingStreet,
				Account.ShippingCity,
				Account.ShippingState,
				Account.ShippingPostalCode,
				Account.ShippingCountry,
				Account.Phone,
				Account.Payment_Terms__c,
				Account.Level__c,
				Route__c,
				Route__r.Name,
				Printed_By__c, 
				Printed_By__r.Name,
				Printed_On__c,
				(
					SELECT 
						Id,
						Quantity,
						UnitPrice,
						TotalPrice,
						Void_Explanation__c,
						Product2.Pieces__c,
						Product2.Name,
						Product2.SKU__c,
						Product2.Units_Pk__c
					FROM OrderItems
				)
			FROM 
				Order
			WHERE
				(Status = :STATUS_ACTIVATED OR Status =:STATUS_DELIVERED) AND
				Type = :TYPE_REGULAR AND
				Route__c != NULL AND
				EndDate = :deliveryDate AND
				ActivatedDate <= :activationDate
			ORDER BY 
				Route__r.Name ASC
		];
	}

	/**
	 * Returns all orders by route, delivery date and activated before or at the datetime specified
	 * @author Marcel Gomez
	 * @date 09/16/2017
	 */
	global List<Order> fetchByRouteActivationDate(Id routeId, Date deliveryDate, Datetime activationDate) {
		return [
			SELECT
				Id,
				OrderNumber,
				Status,
            	EffectiveDate,
				EndDate,
				ActivatedDate,
				PoNumber,
				Order_Total__c,
				AccountId,
				Account.Name,
            	Account.Account_Number__c,
				Account.ShippingStreet,
				Account.ShippingCity,
				Account.ShippingState,
				Account.ShippingPostalCode,
				Account.ShippingCountry,
				Account.Phone,
				Account.Payment_Terms__c,
				Account.Level__c,
				Route__c,
				Route__r.Name,
				Printed_By__c, 
				Printed_By__r.Name,
				Printed_On__c,
				(
					SELECT 
						Id,
						Quantity,
						UnitPrice,
						TotalPrice,
						Void_Explanation__c,
						Product2.Pieces__c,
						Product2.Name,
						Product2.SKU__c,
						Product2.Units_Pk__c
					FROM OrderItems
				)
			FROM 
				Order
			WHERE
				(Status = :STATUS_ACTIVATED OR Status =:STATUS_DELIVERED) AND
				Type = :TYPE_REGULAR AND
				Route__c = :routeId AND
				EndDate = :deliveryDate AND
				ActivatedDate <= :activationDate
			ORDER BY 
				Route__r.Name ASC
		];
	}

	/**
	 * Returns all orders that are scheduled to be delivered
	 * on the specified date, that also belongs to the specified
	 * route. Note: Only active orders are returned..
	 * @author Fernando Gomez, SkyPlanner LLC
	 * @date May 8th, 2017
	 */
	global List<Order> fetchByRoute(Id routeId, Date deliveryDate) {
		return [
			SELECT
				Id,
				OrderNumber,
				Status,
            	EffectiveDate,
				EndDate,
				PoNumber,
				Order_Total__c,
				AccountId,
				Account.Name,
            	Account.Account_Number__c,
				Account.ShippingStreet,
				Account.ShippingCity,
				Account.ShippingState,
				Account.ShippingPostalCode,
				Account.ShippingCountry,
				Account.Phone,
				Account.Payment_Terms__c,
				Account.Level__c,
				Route__c,
				Route__r.Name, (
					SELECT 
						Id,
						Quantity,
						UnitPrice,
						TotalPrice,
						Void_Explanation__c,
						Product2.Pieces__c,
						Product2.Name,
						Product2.SKU__c,
						Product2.Units_Pk__c
					FROM OrderItems
				)
			FROM Order
			WHERE Route__c = :routeId
			AND EndDate = :deliveryDate
			AND (Status = :STATUS_ACTIVATED OR Status =:STATUS_DELIVERED)
			AND Type = :TYPE_REGULAR
			ORDER BY OrderNumber ASC
		];
	}

	/**
	 * Returns the specified order
	 * @author Fernando Gomez, SkyPlanner LLC
	 * @date May 8th, 2017
	 * @param orderId
	 */
	global Order fetch(Id orderId) {
		return [
			SELECT
				Id,
				OrderNumber,
				Status,
            	EffectiveDate,
				EndDate,
				PoNumber,
				Order_Total__c,
				AccountId,
				Account.Name,
                Account.Account_Number__c,
				Account.ShippingStreet,
				Account.ShippingCity,
				Account.ShippingState,
				Account.ShippingPostalCode,
				Account.ShippingCountry,
				Account.Phone,
				Account.Payment_Terms__c,
				Account.Level__c,
				Route__c,
				Route__r.Name, (
					SELECT 
						Id,
						Quantity,
						UnitPrice,
						TotalPrice,
						Void_Explanation__c,
						Product2.Pieces__c,
						Product2.Name,
						Product2.SKU__c,
						Product2.Units_Pk__c
					FROM OrderItems
				)
			FROM Order
			WHERE Id = :orderId
		];
	}

	/**
	 * Returns the specified order. Specified by Id.
	 * The oreder information is wrapped to avoid permission problems.
	 * @author Fernando Gomez, SkyPlanner LLC
	 * @date May 8th, 2017
	 * @param info
	 */
	global OrderInformation getById(Id orderId) {
		return new OrderInformation(fetch(orderId));
	}

	/**
	 * Activates the specified order.
	 * Takes the wrapper class and updates the status.
	 * @date May 8th, 2017
	 */
	global void activate(OrderInformation info) {
		activate(info.orderId);
		info.status = STATUS_ACTIVATED;
	}

	/**
	 * Activates the specified order
	 * @date May 8th, 2017
	 * @param oredrId
	 */
	global void activate(Id orderId) {
		Order o;
		// we fetch the order
		o = fetch(orderId);
		// we change the status
		o.Status = STATUS_ACTIVATED;
		// and commit the changes
		update o;
	}

	/**
	 * Wraps information about a specific order.
	 * The infromation amount is minimal. This class contains no processed
	 * and cannot be constructed outside the order manager.
	 * @author Fernando Gomez, SkyPlanner LLC
	 * @date May 8th, 2017
	 */
	global class OrderInformation {
		global Id orderId { get; private set; }
		global String orderNumber { get; private set; }
		global String status { get; private set; }
		global String clientName { get; private set; }

		/**
		 * Private constructor as instances of this wrapper
		 * cannot be constructed ouside of the manager.
		 * @author Fernando Gomez, SkyPlanner LLC
	 	 * @date May 8th, 2017
	 	 */
	 	private OrderInformation(Order o) {
	 		orderId = o.Id;
	 		orderNumber = o.OrderNumber;
	 		status = o.Status;
	 		clientName = o.Account.Name;
	 	}
	}

	/**
	 * Throwable exception. We might need custom message for 
	 * expected exceptions. We use this class.
	 * @author Fernando Gomez, SkyPlanner LLC
	 * @date May 8th, 2017
	 */
	global class OrderManagerException extends Exception { }
}