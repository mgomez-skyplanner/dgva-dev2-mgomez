/**
* <p>
* Version log:
* <ul>
* <li>1.00 10/04/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 10/04/2017
* @description 	 - Represents the controller for VF page OutstandingStatementPage
*/

global with sharing class OutstandingStatementController {

	global Decimal ItemsPerPage1 { get; set; }
	global Decimal ItemsPerPage { get; set; }

	private String targetStatementId;
	global String StatementId { 
		get { return this.targetStatementId; }
		set { 
			this.targetStatementId = value;
			this.InitByStatement(this.targetStatementId);
		}
	}

	global List<OutStandingStatement_Wrapper> WrappersList { get; private set; }

	public OutstandingStatementController(ApexPages.StandardController stdController) {
		system.debug('---------------------- OutstandingStatementController custom controller constructor....');
		Init();
	}

	public OutstandingStatementController() {
		system.debug('---------------------- OutstandingStatementController default constructor....');
		Init();
	}

	public List<OutStandingStatement_Wrapper> VisibleWrappersList { 
		get {
			List<OutStandingStatement_Wrapper> result = new List<OutStandingStatement_Wrapper>();
			for(OutStandingStatement_Wrapper item: WrappersList) {
				if(item.BalanceDue > 0)
					result.add(item);
			}
			return result;
		}
	}


	private void Init() {
		Map<String, String> params;

		try{
			//initialize page settings
			Statements_Settings__c settings = Statements_Settings__c.getOrgDefaults();
			this.ItemsPerPage1 = settings.Items_Per_Page1_Outstanding__c;
			this.ItemsPerPage = settings.Items_Per_Page_Outstanding__c;

			//initialize Wrappers List
			this.WrappersList = new List<OutStandingStatement_Wrapper>();

			//get the query string parameters
			params = ApexPages.currentPage().getParameters();

			system.debug('---------------------- Page params:');
			system.debug(params);

			//parse query string parameters
			StatementManager_Helper.StatementManagement_Filters queryStringFilters = StatementManager_Helper.ParsePageParams(params);
			
			system.debug('---------------------- queryStringFilters:');
			system.debug(queryStringFilters);

			//decide how to generate the PDF based on the page parameters
			if(!String.isBlank(queryStringFilters.StatementId) || !String.isBlank(queryStringFilters.AccountId) || !String.isBlank(queryStringFilters.RouteId)) {
				if(!String.isBlank(queryStringFilters.AccountId)) {
					system.debug('---------------------- InitByAccount');

					InitByAccount(queryStringFilters.AccountId, queryStringFilters.EndDate);
				}
				else if(!String.isBlank(queryStringFilters.StatementId)) {
					system.debug('---------------------- InitByStatement');

					InitByStatement(queryStringFilters.StatementId);
				}
				else {
					system.debug('---------------------- InitByRoute');

					InitByRoute(queryStringFilters);
				}

				CompletePageItems();
			}
		}
		catch(Exception e) {
			system.debug('-------------------------------------- OutstandingStatementController ERROR !!! ' + e.getMessage());
			system.debug('-------------------------------------- OutstandingStatementController params: ');
			system.debug(params);
		}
	}

	private void InitByAccount(String accountId, Date endDate) {
		OutStandingStatement_Wrapper wrapper = new OutStandingStatement_Wrapper(accountId, endDate);
		this.WrappersList.add(wrapper);
	}

	private void InitByRoute(StatementManager_Helper.StatementManagement_Filters queryStringFilters) {
		List<Account> accountsInRoute = StatementManager_Helper.LoadAccounts(queryStringFilters);

		List<Id> accountIds = new List<Id>();
		for(Account account : accountsInRoute) {
			accountIds.add(account.Id);
		}

		StatementBuilder_Outstanding builder = new StatementBuilder_Outstanding(accountIds, queryStringFilters.EndDate, true);
		builder.generateOutStandingStatement(queryStringFilters.GenerateStatements);

		for(Account account : accountsInRoute) {
			Boolean includeAccount = !queryStringFilters.OnlyNoEmailReady || account.AcctSeed__Billing_Contact__c == NULL;

			if(includeAccount) {
				OutStandingStatement_Wrapper wrapper = new OutStandingStatement_Wrapper();
				
				wrapper.Account = account;
				wrapper.StatementDate = queryStringFilters.EndDate;
				wrapper.StatementBalanceByDate = queryStringFilters.EndDate;
				wrapper.BalanceDue = builder.Balance.get(account.Id);
				wrapper.Rows = builder.LineItems.get(account.Id);				

				this.WrappersList.add(wrapper);
			}
		}
	}

	private void InitByStatement(String statementId) {
		system.debug('----------------- OutstandingStatementController InitByStatement statementId = ' + statementId);

		if(!String.isBlank(statementId) && WrappersList.size() == 0) {
			Statement__c statement = [SELECT Id, Account__c, Period_Start_Date__c, Period_End_Date__c FROM Statement__c WHERE Id =: statementId];

			OutStandingStatement_Wrapper wrapper = new OutStandingStatement_Wrapper(statement.Account__c, statement.Period_End_Date__c);
			this.WrappersList.add(wrapper);

			CompletePageItems();
		}
	}

	private void CompletePageItems() {
		//if(this.ItemsPerPage == NULL)
			return;//review because at this point properties ItemsPerPage and ItemsPerPage1 are not set yet

		for(OutStandingStatement_Wrapper wrapper : this.WrappersList) {
			Integer rowsAmount = wrapper.Rows == NULL ? 1 : wrapper.Rows.size();

			if(wrapper.Rows == NULL)
				wrapper.Rows = new List<Statement_Row>();

			system.debug('----------------------- rowsAmount = ' + rowsAmount);
			system.debug('----------------------- ItemsPerPage = ' + ItemsPerPage);

			Integer module = Math.mod(rowsAmount, (Integer)this.ItemsPerPage);

			if(module > 0) {
				for(Integer i = 0; i < module; i++) {
					Statement_Row row = new Statement_Row();
					row.Reference = '&nbsp;';

					wrapper.Rows.add(row);
				}
			}
		}
	}

	global class OutStandingStatement_Wrapper {
		public Date StatementDate { get; set; }
		public Date StatementBalanceByDate { get; set; }
		public Decimal BalanceDue { get; set; }
		public Account Account { get; set; }
		public List<Statement_Row> Rows { get; set; }

		public OutStandingStatement_Wrapper() {}

		public OutStandingStatement_Wrapper(Id accountId, Date endDate) {
			this.Account = [SELECT 
								Id,
								Name,
								Phone,
								Route__r.Name,
								ShippingStreet,
								ShippingCity,
								ShippingState,
								ShippingPostalCode,
								ShippingCountry
							FROM
								Account
							WHERE
								Id =: accountId
							];

			StatementBuilder_Outstanding builder = new StatementBuilder_Outstanding(new List<Id> { this.Account.Id }, endDate, true);
			builder.generateOutStandingStatement(false);

			this.StatementDate 	= Date.today();
			this.StatementBalanceByDate = endDate;
			this.BalanceDue 	= builder.Balance.get(this.Account.Id);
			this.Rows 			= builder.LineItems.get(this.Account.Id);
		}
	}
}