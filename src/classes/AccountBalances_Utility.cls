/**
* <p>
* Version log:
* <ul>
* <li>1.00 06/28/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 06/28/2017
* @description 	 - This class provides methods to calculate the Balance and the Past Due Balance of one or more Accounts
*/
 
global class AccountBalances_Utility {

	public AccountBalances_Utility(ApexPages.StandardController stdController) {
		
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/28/2017
	* @description 	 - Loads the Accounts along with their Billings
	* @param       	 - accountIds: Set of Account Ids to get their Orders
	* @return      	 - Returns the list of Accounts with Billings
	*/
	private static List<Account> reloadAccountsById(Set<Id> accountIds) {
		List<String> orderStatuses = new List<String> { 'Activated', 'Delivered' };

		return [SELECT
					Id,
					Balance__c,
					Past_Due__c,
					(
						SELECT
							AcctSeed__Balance__c,
							AcctSeed__Due_Date2__c
						FROM
							AcctSeed__Billings__r
						WHERE
							AcctSeed__Status__c = 'Posted' AND
							AcctSeed__Balance__c != 0 AND
							AcctSeed__Date__c <: Date.today().addDays(1)
					),
					(
						SELECT
							AcctSeed__Balance__c
							
						FROM
							AcctSeed__Cash_Receipts__r
						WHERE
							AcctSeed__Status__c = 'Posted' AND
							AcctSeed__Balance__c != 0 AND
							AcctSeed__Receipt_Date__c <: Date.today().addDays(1)

					)
				FROM
					Account
				WHERE
					Id IN: accountIds];
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/28/2017
	* @description 	 - Calculates the Account Balance and Past Due based balance of Billings of Activated Orders
	* @param       	 - accounts: List of Account to update
	*/
	public static void UpdateAccountBalances(Set<Id> accountIds) {
		List<Account> reloadedAccounts = reloadAccountsById(accountIds);

		List<Account> accounts2Update = new List<Account>();

		for(Account account : reloadedAccounts) {
			Decimal balance = 0;
			Decimal pastDue = 0;

			if(account.AcctSeed__Billings__r != NULL && account.AcctSeed__Billings__r.size() > 0) {
				for(AcctSeed__Billing__c billing : account.AcctSeed__Billings__r) {
					balance += billing.AcctSeed__Balance__c;

					if(billing.AcctSeed__Due_Date2__c < Date.today())
						pastDue += billing.AcctSeed__Balance__c;
				}
			}

			if(account.AcctSeed__Cash_Receipts__r != NULL && account.AcctSeed__Cash_Receipts__r.size() > 0) {
				for(AcctSeed__Cash_Receipt__c cashReceipt : account.AcctSeed__Cash_Receipts__r) {
					balance -= cashReceipt.AcctSeed__Balance__c;
				}
			}

			Boolean balancesChanged = false;

			if(balance != account.Balance__c) {
				account.Balance__c = balance;
				balancesChanged = true;
			}

			if(pastDue != account.Past_Due__c) {
				account.Past_Due__c = pastDue;
				balancesChanged = true;
			}

			if(balancesChanged)
				accounts2Update.add(account);
		}

		if(accounts2Update.size() > 0) {
			try{
				update accounts2Update;

				system.debug('------------------------------ Account Balance UPDATED: ' + accounts2Update.size());
			}
			catch (Exception e) {
				system.debug('------------------------------ Account Balance UPDATE ERROR: ' + e.getMessage());
			}
		}
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/28/2017
	* @description 	 - Calculates the Account Balance and Past Due based balance of Billings of Activated Orders
	* @param       	 - accounts: List of Account to update
	*/
	@AuraEnabled
    webservice static String UpdateAccountBalances(String accountId) {
    	try {
    		AccountBalances_Utility.UpdateAccountBalances(new Set<Id> { (Id)accountId });
    	}
    	catch (Exception e) {
    		system.debug('Error: ' + e.getMessage());
    	}

    	return '1';
    }
}