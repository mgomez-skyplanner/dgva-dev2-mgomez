/**
 * @author       William Garcia
 * @date         10/27/2017
 * @description  StatementController test class.
 */
@isTest (seeAllData=false)
public class StatementControllerTest {
	
	@testSetup
    static void data() {
    	Route__c route1 = new Route__c(
			Name = 'route1'
   		);
   		insert route1;

    	Contact contact1 = new Contact(LastName = 'test', FirstName = 'user', Email = 'usertest@example.com');
    	insert contact1;

    	Account acc1 = new Account(
			Name = 'Account1',
			BillingCountry = 'C1',
			BillingCity = 'C2',
			Total_Balance__c = 50,
			Route__c = route1.Id,
			AcctSeed__Billing_Contact__c = contact1.Id
		);
		insert acc1;
    }

    /**
	 * @author William Garcia
	 * @date 10/27/2017
	 */
	@isTest
	static void generateOutstandingStatementsBulk_Test() {
		Account acc1 = [SELECT Id FROM Account][0];
		Route__c route = [SELECT Id, Name FROM Route__c][0];
		String strToday = String.valueOf(Datetime.now().getTime());

		StatementController ctrl = new StatementController();
		StatementController.generateStatements_Bulk(acc1.Id, null, null, null, 
			route.Name, String.valueOf(StatementManager_Helper.StatementType.Outstanding), 
			null, strToday, true);

		List<Statement__c> sts = [SELECT Id FROM Statement__c];
		//System.assertEquals(sts.size(), 1);

		//Get the initial values.
		object result = StatementController.LoadInitialValues(acc1.Id);

		//Search the statements
		List<StatementController.StatementManagement_Rows> resultStatements = 
			StatementController.searchStatements(acc1.Id, null, null, null, 
				route.Name, StatementManager_Helper.StatementType.Outstanding + '', 
				null, null);
		//System.assertEquals(1, resultStatements.size());

		//Test the email sent
		String emailResult = StatementController.sendStatementsByEmail(acc1.Id, 
			null , //paymentTerm
			null , //customerLevel
			null , //customerGroup
			null , //routeId
			null , //statementType
			null , //startDate
			null); //endDate
	}

	@isTest
	static void generateActivityStatementsBulk_Test() {
		Account acc1 = [SELECT Id FROM Account][0];
		Route__c route = [SELECT Id, Name FROM Route__c][0];
		String strToday = String.valueOf(Datetime.now().getTime());

		StatementController ctrl = new StatementController();
		StatementController.generateStatements_Bulk(acc1.Id, null, null, null, 
			route.Name, String.valueOf(StatementManager_Helper.StatementType.Activity), 
			strToday, strToday, true);
	}

	@isTest
	static void generateAllOutstandingStatementsBulk_Test() {
		Route__c route = [SELECT Id, Name FROM Route__c][0];
		String strToday = String.valueOf(Datetime.now().getTime());

		StatementController ctrl = new StatementController();
		StatementController.generateStatements_Bulk(null, null, null, null, 
			route.Name, String.valueOf(StatementManager_Helper.StatementType.Outstanding), 
			null, strToday, true);
	}

	@isTest
	static void generateAllActivityStatementsBulk_Test() {
		Route__c route = [SELECT Id, Name FROM Route__c][0];
		String strToday = String.valueOf(Datetime.now().getTime());

		StatementController ctrl = new StatementController();
		StatementController.generateStatements_Bulk(null, null, null, null, 
			route.Name, String.valueOf(StatementManager_Helper.StatementType.Activity), 
			strToday, strToday, true);
	}

}