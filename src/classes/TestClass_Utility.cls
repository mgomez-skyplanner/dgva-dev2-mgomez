/**
* <p> 
* Version log:
* <ul>
* <li>1.00 06/29/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 06/29/2017
* @description 	 - Provides utility methods to create dummy data for test classes (97% - 07/07/2017)
*/
 
public with sharing class TestClass_Utility {

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This methods creates data for custom settings used in the processes related to Order, Sales Order and Billing. Feel free to invoke this method any time you want to interact with any of those object
	*/
	public static void createCustomSettingsData() {
		TestClass_Utility.Create_CustomSetting_Order_Conversion_Fields();
		TestClass_Utility.Create_CustomSetting_Order_Product_Conversion_Fields();

		TestClass_Utility.Create_CustomSetting_Order_to_Billing_Fields();
		TestClass_Utility.Create_CustomSetting_Order_Product_to_Billing_Line_Fields();

		TestClass_Utility.Create_CustomSetting_Trigger_Control_Setting();
		TestClass_Utility.Create_CustomSetting_Order_Status_Conversion_Values();

		TestClass_Utility.CreateDefaultSettingsAccountingSeed();
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This methods creates records for custom setting Order_Conversion_Fields__c
	*/
	public static void Create_CustomSetting_Order_Conversion_Fields() {
		List<Order_Conversion_Fields__c> orderFields = new List<Order_Conversion_Fields__c>();

		Order_Conversion_Fields__c field1 = new Order_Conversion_Fields__c();
		field1.Name 				= 'AccountId';
		field1.Sales_Order_Field__c = 'AcctSeedERP__Customer__c';
		orderFields.add(field1);

		Order_Conversion_Fields__c field2 = new Order_Conversion_Fields__c();
		field2.Name 				= 'BillingCity';
		field2.Sales_Order_Field__c = 'AcctSeedERP__Billing_City__c';
		orderFields.add(field2);
		
		Order_Conversion_Fields__c field3 = new Order_Conversion_Fields__c();
		field3.Name 				= 'BillingCountry';
		field3.Sales_Order_Field__c = 'AcctSeedERP__Billing_Country__c';
		orderFields.add(field3);

		Order_Conversion_Fields__c field4 = new Order_Conversion_Fields__c();
		field4.Name 				= 'BillingPostalCode';
		field4.Sales_Order_Field__c = 'AcctSeedERP__Billing_PostalCode__c';
		orderFields.add(field4);

		Order_Conversion_Fields__c field5 = new Order_Conversion_Fields__c();
		field5.Name 				= 'BillingState';
		field5.Sales_Order_Field__c = 'AcctSeedERP__Billing_State__c';
		orderFields.add(field5);
		
		Order_Conversion_Fields__c field6 = new Order_Conversion_Fields__c();
		field6.Name 				= 'ShippingCity';
		field6.Sales_Order_Field__c = 'AcctSeedERP__Shipping_City__c';
		orderFields.add(field6);
		
		Order_Conversion_Fields__c field7 = new Order_Conversion_Fields__c();
		field7.Name 				= 'ShippingCountry';
		field7.Sales_Order_Field__c = 'AcctSeedERP__Shipping_Country__c';
		orderFields.add(field7);
		
		Order_Conversion_Fields__c field8 = new Order_Conversion_Fields__c();
		field8.Name 				= 'ShippingPostalCode';
		field8.Sales_Order_Field__c = 'AcctSeedERP__Shipping_PostalCode__c';
		orderFields.add(field8);
		
		Order_Conversion_Fields__c field9 = new Order_Conversion_Fields__c();
		field9.Name 				= 'ShippingState';
		field9.Sales_Order_Field__c = 'AcctSeedERP__Shipping_State__c';
		orderFields.add(field9);
		
		Order_Conversion_Fields__c field10 = new Order_Conversion_Fields__c();
		field10.Name 				= 'ShippingStreet';
		field10.Sales_Order_Field__c = 'AcctSeedERP__Shipping_Street__c';
		orderFields.add(field10);
		
		Order_Conversion_Fields__c field11 = new Order_Conversion_Fields__c();
		field11.Name 				= 'Status';
		field11.Sales_Order_Field__c = 'AcctSeedERP__Status__c';
		orderFields.add(field11);
		
		insert orderFields;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This methods creates records for custom setting Order_Product_Conversion_Fields__c
	*/
	public static void Create_CustomSetting_Order_Product_Conversion_Fields() {
		List<Order_Product_Conversion_Fields__c> orderProductFields = new List<Order_Product_Conversion_Fields__c>();		
		
		Order_Product_Conversion_Fields__c field12 = new Order_Product_Conversion_Fields__c();
		field12.Name 					  = 'Quantity';
		field12.Sales_Order_Line_Field__c = 'AcctSeedERP__Quantity_Ordered__c';
		orderProductFields.add(field12);

		Order_Product_Conversion_Fields__c field13 = new Order_Product_Conversion_Fields__c();
		field13.Name 					  = 'UnitPrice';
		field13.Sales_Order_Line_Field__c = 'AcctSeedERP__Unit_Price__c';
		orderProductFields.add(field13);
		
		insert orderProductFields;
	}
	
	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This methods creates records for custom setting Order_to_Billing_Fields__c
	*/
	public static void Create_CustomSetting_Order_to_Billing_Fields() {
		List<Order_to_Billing_Fields__c> records = new List<Order_to_Billing_Fields__c>();

		Order_to_Billing_Fields__c record1 = new Order_to_Billing_Fields__c();
		record1.Name 			 = 'AccountId';
		record1.Billing_Field__c = 'AcctSeed__Customer__c';
		records.add(record1);

		Order_to_Billing_Fields__c record2 = new Order_to_Billing_Fields__c();
		record2.Name 			 = 'BillingCity';
		record2.Billing_Field__c = 'AcctSeed__Billing_City__c';
		records.add(record2);

		Order_to_Billing_Fields__c record3 = new Order_to_Billing_Fields__c();
		record3.Name 			 = 'BillingCountry';
		record3.Billing_Field__c = 'AcctSeed__Billing_Country__c';
		records.add(record3);

		Order_to_Billing_Fields__c record4 = new Order_to_Billing_Fields__c();
		record4.Name 			 = 'BillingPostalCode';
		record4.Billing_Field__c = 'AcctSeed__Billing_PostalCode__c';
		records.add(record4);

		Order_to_Billing_Fields__c record5 = new Order_to_Billing_Fields__c();
		record5.Name 			 = 'BillingState';
		record5.Billing_Field__c = 'AcctSeed__Billing_State__c';
		records.add(record5);

		Order_to_Billing_Fields__c record6 = new Order_to_Billing_Fields__c();
		record6.Name 			 = 'BillingStreet';
		record6.Billing_Field__c = 'AcctSeed__Billing_Street__c';
		records.add(record6);

		Order_to_Billing_Fields__c record7 = new Order_to_Billing_Fields__c();
		record7.Name 			 = 'Sales_Order__c';
		record7.Billing_Field__c = 'AcctSeedERP__Sales_Order__c';
		records.add(record7);

		Order_to_Billing_Fields__c record8 = new Order_to_Billing_Fields__c();
		record8.Name 			 = 'ShippingCity';
		record8.Billing_Field__c = 'AcctSeed__Shipping_City__c';
		records.add(record8);

		Order_to_Billing_Fields__c record9 = new Order_to_Billing_Fields__c();
		record9.Name 			 = 'ShippingCountry';
		record9.Billing_Field__c = 'AcctSeed__Shipping_Country__c';
		records.add(record9);

		Order_to_Billing_Fields__c record10 = new Order_to_Billing_Fields__c();
		record10.Name 			 = 'ShippingPostalCode';
		record10.Billing_Field__c = 'AcctSeed__Shipping_PostalCode__c';
		records.add(record10);

		Order_to_Billing_Fields__c record11 = new Order_to_Billing_Fields__c();
		record11.Name 			 = 'ShippingState';
		record11.Billing_Field__c = 'AcctSeed__Shipping_State__c';
		records.add(record11);

		Order_to_Billing_Fields__c record12 = new Order_to_Billing_Fields__c();
		record12.Name 			 = 'ShippingStreet';
		record12.Billing_Field__c = 'AcctSeed__Shipping_Street__c';
		records.add(record12);

		/*
		Order_to_Billing_Fields__c record13 = new Order_to_Billing_Fields__c();
		record13.Name 			 = 'EffectiveDate';
		record13.Billing_Field__c = 'AcctSeed__Date__c';
		records.add(record13);
		*/

		Order_to_Billing_Fields__c record14 = new Order_to_Billing_Fields__c();
		record14.Name 			 = 'OrderNumber';
		record14.Billing_Field__c = 'AcctSeed__Proprietary_Billing_Number__c';
		records.add(record14);

		Order_to_Billing_Fields__c record15 = new Order_to_Billing_Fields__c();
		record15.Name 			 = 'PoNumber';
		record15.Billing_Field__c = 'AcctSeed__PO_Number__c';
		records.add(record15);

		Order_to_Billing_Fields__c record16 = new Order_to_Billing_Fields__c();
		record16.Name 			 = 'EndDate';
		record16.Billing_Field__c = 'AcctSeed__Date__c';
		records.add(record16);

		insert records;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This methods creates records for custom setting Order_Product_to_Billing_Line_Fields__c
	*/
	public static void Create_CustomSetting_Order_Product_to_Billing_Line_Fields() {
		List<Order_Product_to_Billing_Line_Fields__c> records = new List<Order_Product_to_Billing_Line_Fields__c>();

		Order_Product_to_Billing_Line_Fields__c record1 = new Order_Product_to_Billing_Line_Fields__c();
		record1.Name 			 	 = 'Quantity';
		record1.BillingLine_Field__c = 'AcctSeed__Hours_Units__c';
		records.add(record1);

		Order_Product_to_Billing_Line_Fields__c record2 = new Order_Product_to_Billing_Line_Fields__c();
		record2.Name 			 	 = 'UnitPrice';
		record2.BillingLine_Field__c = 'AcctSeed__Rate__c';
		records.add(record2);

		insert records;
	}
	
	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This methods creates records for custom setting Order_Status_Conversion_Values__c
	*/
	public static void Create_CustomSetting_Order_Status_Conversion_Values() {
		List<Order_Status_Conversion_Values__c> records = new List<Order_Status_Conversion_Values__c>();		
		
		Order_Status_Conversion_Values__c record1 = new Order_Status_Conversion_Values__c();
		record1.Name 				 = 'Activated';
		record1.Sales_Order_Status__c = 'Open';
		records.add(record1);		
		
		Order_Status_Conversion_Values__c record2 = new Order_Status_Conversion_Values__c();
		record2.Name 				 = 'Cancelled';
		record2.Sales_Order_Status__c = 'Cancelled';
		records.add(record2);		
		
		Order_Status_Conversion_Values__c record3 = new Order_Status_Conversion_Values__c();
		record3.Name 				 = 'Draft';
		record3.Sales_Order_Status__c = 'Open';
		records.add(record3);		
		
		Order_Status_Conversion_Values__c record4 = new Order_Status_Conversion_Values__c();
		record4.Name 				 = 'Prepared';
		record4.Sales_Order_Status__c = 'Open';
		records.add(record4);

		insert records;
	}
	
	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This methods creates records for custom setting Trigger_Control_Setting__c
	*/
	public static void Create_CustomSetting_Trigger_Control_Setting() {
		List<Trigger_Control_Setting__c> records = new List<Trigger_Control_Setting__c>();

		Trigger_Control_Setting__c record1 	= new Trigger_Control_Setting__c();
		record1.Name 						= 'OrderConversion_Trigger';
		record1.Execute__c 					= true;
		records.add(record1);

		Trigger_Control_Setting__c record2 	= new Trigger_Control_Setting__c();
		record2.Name 						= 'OrderProductConversion_Trigger';
		record2.Execute__c 					= true;
		records.add(record2);

		insert records;
	}
	
	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/05/2017
	* @description 	 - This methods creates a record for Accounting Seed setting object AcctSeed__Accounting_Period__c
	*/
	public static AcctSeed__Accounting_Period__c Create_AccountingSeedSetting_AccountingPeriod(Order order) {
		//calculate Accounting Period
		AcctSeed__Accounting_Period__c accountingPeriod = new AcctSeed__Accounting_Period__c();

		Datetime dt = Datetime.newInstance(order.EffectiveDate.year(), order.EffectiveDate.month(), order.EffectiveDate.day());
		String accountingPeriodName = dt.format('YYYY-MM');

		accountingPeriod.Name 					 = accountingPeriodName;
		accountingPeriod.AcctSeed__Start_Date__c = order.EffectiveDate;
		accountingPeriod.AcctSeed__End_Date__c 	 = order.EndDate;
		accountingPeriod.AcctSeed__Status__c 	 = 'Open';

		return accountingPeriod;
	}
	
	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/05/2017
	* @description 	 - This methods creates a record for Accounting Seed setting object AcctSeed__Billing_Format__c
	* @param 	 	 - name: the name of the PDF Format
	* @param 	 	 - pType: the type of the PDF Format
	* @param 	 	 - insertRecord: if true, the created record is inserted
	*/
	public static AcctSeed__Billing_Format__c Create_AccountingSeedSetting_PDFFormat(String name, String pType, Boolean insertRecord) {
		AcctSeed__Billing_Format__c pdfFormat = new AcctSeed__Billing_Format__c();

		pdfFormat.Name = name;//'PDF Format';
		pdfFormat.AcctSeed__Type__c = pType;//'Billing';
		pdfFormat.AcctSeed__Visualforce_PDF_Page__c = 'VoidOrderProducts';
		pdfFormat.AcctSeed__Default_Email_Template__c = 'OrderConfirmation';

		if(insertRecord)
			insert pdfFormat;

		return pdfFormat;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Creates a GL Account of a given type
	* @param 	 	 - name: the name of GL Account
	* @param 	 	 - glType: the type of GL Account
	* @param 	 	 - bank: indicates if the GL Account is a bank
	* @param 	 	 - insertRecord: if true, the created record is inserted
	*/
	public static AcctSeed__GL_Account__c CreateGLAccount(String name, String glType, Boolean bank, Boolean insertRecord) {

		AcctSeed__GL_Account__c glAccount = new AcctSeed__GL_Account__c();

		glAccount.Name 									= name;
		glAccount.AcctSeed__Active__c 					= true;
		glAccount.AcctSeed__Bank__c 					= bank;
		glAccount.AcctSeed__Billing_Description__c 		= 'description';
		glAccount.AcctSeed__Expense_Report_Name__c 		= 'report name';
		glAccount.AcctSeed__Mileage_Account__c 			= true;
		glAccount.AcctSeed__Mileage_Rate__c 			= 1;
		glAccount.AcctSeed__Sub_Type_1__c 				= 'Assets';
		glAccount.AcctSeed__Sub_Type_2__c 				= 'Cash';
		glAccount.AcctSeed__Type__c 					= glType;
		glAccount.AcctSeed__Used_in_Expense_Reports__c 	= false;		

		if(insertRecord)
			insert glAccount;
		
		return glAccount;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/07/2017
	* @description 	 - Creates a Ledger
	* @param 	 	 - name: the name of the Ledger
	* @param 	 	 - lType: the type of the Ledger
	* @param 	 	 - insertRecord: if true, the created record is inserted
	*/
	public static AcctSeed__Ledger__c CreateLedger(String name, String lType, Boolean insertRecord) {

		AcctSeed__Ledger__c ledger = new AcctSeed__Ledger__c();

		ledger.Name 					= name;
		ledger.AcctSeed__Description__c	= 'description ';
		ledger.AcctSeed__Type__c 		= lType;

		if(insertRecord)
			insert ledger;
		
		return ledger;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/07/2017
	* @description 	 - Creates default mandatory data to create a Billing
	*/
	public static void CreateDefaultDataForBillings(Account account, Order order, Product2 product) {
		//create Accounting Period
		AcctSeed__Accounting_Period__c accountingPeriod = TestClass_Utility.Create_AccountingSeedSetting_AccountingPeriod(order);
		insert accountingPeriod;
		
		AcctSeed__Billing_Format__c pdfFormat = TestClass_Utility.Create_AccountingSeedSetting_PDFFormat('PDF Format', 'Billing', true);

		AcctSeed__GL_Account__c glAccount = TestClass_Utility.CreateGLAccount('GL Account - Product', 'Revenue', true, true);

		product.AcctSeed__Revenue_GL_Account__c = glAccount.Id;
		update product;

		account.AcctSeed__Billing_Format__c = pdfFormat.Id;
		update account;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/07/2017
	* @description 	 - Creates default settings for Accounting Seed
	*/
	public static void CreateDefaultSettingsAccountingSeed() {
		//GL Accounts
		List<AcctSeed__GL_Account__c> defaultGLAccounts = new List<AcctSeed__GL_Account__c>();

		AcctSeed__GL_Account__c glAccount1 = TestClass_Utility.CreateGLAccount('1000-Cash', 'Balance Sheet', true, false);
		defaultGLAccounts.add(glAccount1);

		AcctSeed__GL_Account__c glAccount2 = TestClass_Utility.CreateGLAccount('3060-Current Year Earnings', 'Balance Sheet', false, false);
		defaultGLAccounts.add(glAccount2);

		AcctSeed__GL_Account__c glAccount3 = TestClass_Utility.CreateGLAccount('3050-Retained Earnings', 'Balance Sheet', false, false);
		defaultGLAccounts.add(glAccount3);

		AcctSeed__GL_Account__c glAccount4 = TestClass_Utility.CreateGLAccount('4000-Product Revenue', 'Revenue', false, false);
		defaultGLAccounts.add(glAccount4);

		AcctSeed__GL_Account__c glAccount5 = TestClass_Utility.CreateGLAccount('1200-Accounts Receivable', 'Balance Sheet', false, false);
		defaultGLAccounts.add(glAccount5);

		AcctSeed__GL_Account__c glAccount6 = TestClass_Utility.CreateGLAccount('1050-Unapplied Cash', 'Balance Sheet', false, false);
		defaultGLAccounts.add(glAccount6);

		AcctSeed__GL_Account__c glAccount7 = TestClass_Utility.CreateGLAccount('2000-Accounts Payable', 'Balance Sheet', false, false);
		defaultGLAccounts.add(glAccount7);

		AcctSeed__GL_Account__c glAccount8 = TestClass_Utility.CreateGLAccount('2010-Vouchers Payable', 'Balance Sheet', false, false);
		defaultGLAccounts.add(glAccount8);

		AcctSeed__GL_Account__c glAccount9 = TestClass_Utility.CreateGLAccount('5010-Inventory Cost Variance', 'Expense', false, false);
		defaultGLAccounts.add(glAccount9);

		AcctSeed__GL_Account__c glAccount10 = TestClass_Utility.CreateGLAccount('1600-Work In Process', 'Balance Sheet', false, false);
		defaultGLAccounts.add(glAccount10);

		insert defaultGLAccounts;

		//PDF Format
		AcctSeed__Billing_Format__c pdfFormat = TestClass_Utility.Create_AccountingSeedSetting_PDFFormat('PDF Format 1', 'Billing', true);


		AcctSeed__Accounting_Settings__c settings = new AcctSeed__Accounting_Settings__c();

		settings.AcctSeed__Default_Ledger__c 					= TestClass_Utility.CreateLedger('Actual', 'Transactional', true).Id;

		settings.AcctSeed__Default_Bank_Account__c 				= glAccount1.Id;
		settings.AcctSeed__Current_Year_Earnings_GL_Account__c 	= glAccount2.Id;
		settings.AcctSeed__Retained_Earnings_GL_Account__c 		= glAccount3.Id;
		settings.AcctSeed__Revenue_GL_Account__c 				= glAccount4.Id;
		settings.AcctSeed__AR_Control_GL_Account__c 			= glAccount5.Id;
		settings.AcctSeed__Unapplied_Cash_GL_Account__c 		= glAccount6.Id;
		settings.AcctSeed__AP_Control_GL_Account__c 			= glAccount7.Id;
		settings.AcctSeed__Vouchers_Payable_GL_Account__c 		= glAccount8.Id;
		settings.AcctSeed__Inventory_Variance_GL_Account__c 	= glAccount9.Id;
		settings.AcctSeed__Work_in_Process_GL_Account__c 		= glAccount10.Id;

		settings.AcctSeed__Default_Billing_Format__c			= pdfFormat.Id;
		settings.AcctSeed__Billing_Activity_Statement_Format__c = pdfFormat.Id;
		settings.AcctSeed__Billing_Outstanding_Statement_Format__c 	= pdfFormat.Id;

		insert settings;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This methods creates an instance of type Route__c
	* @return        - An instance of Route__c
	*/
	public static Route__c createRoute(String name) {
		Route__c route 	= new Route__c();
		route.Name 		= name;
		
		return route;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This methods creates an Account given a name and a Route__c
	* @param       	 - name: the name of the Account
	*        	 	 - route: the route of the Account
	* @return        - An instance of Account
	*/
	public static Account createAccount(String name, Route__c route) {
		Account account = new Account();
		account.Name = name;
		account.Route__c = route.Id;
		account.Statement_Frequency__c = 'Monthly';
		account.Statement_Day__c = '1';

		return account;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This methods creates an Order for a given Account
	* @param       	 - account: the Account the Order belongs to
	* 				 - opp: the Opportunity of the Order
	* 				 - status: the Status of the Order
	* @return        - An instance of Order
	*/
	public static Order createOrder(Account account, Opportunity opp, String status) {
		Id recordTypeId = Schema.SObjectType.Order.getRecordTypeInfosByName().get('Default').getRecordTypeId();

		Order order 		= new Order();
		order.RecordTypeId 	= recordTypeId;
		order.AccountId 	= account.Id;
		order.OpportunityId	= opp.Id;

		order.EffectiveDate = Date.today();
		order.EndDate 		= Date.today().addDays(365);
		order.Status 		= String.isBlank(status) ? 'Draft' : status;

		order.BillingCity 		= 'city 1';
		order.BillingCountry 	= 'USA';
		order.BillingPostalCode = '300101';
		order.BillingState 		= 'FL';
		order.BillingStreet 	= 'street 1';

		order.ShippingCity 		= 'city 1';
		order.ShippingCountry 	= 'USA';
		order.ShippingPostalCode= '300101';
		order.ShippingState 	= 'FL';
		order.ShippingStreet 	= 'street 1';

		return order;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This methods creates an Opportunity
	* @param       	 - account: The Account of the Opportunity
	* @return        - An instance of Opportunity
	*/
	public static Opportunity createOpportunity(Account account) {
		Opportunity opp	= new Opportunity();
		opp.Name 		= 'Test Opportunity';
		opp.AccountId 	= account.Id;
		opp.CloseDate 	= Date.today().addDays(365);
		opp.StageName	= 'Proposal';

		return opp;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This methods creates a Product
	* @param       	 - product: The name of the Product
	* @return        - An instance of Product2
	*/
	public static Product2 createProduct(String name) {
		Product2 product = new Product2(Name = name, isActive = true, Price__c = 10, SKU__c = '123');

        return product;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This methods creates a PricebookEntry given a Product
	* @param       	 - product: The Product to be added to the Order
	* @return        - An instance of PricebookEntry
	*/
	public static PricebookEntry createPriceBookEntry(Product2 product) {
		Id standardPriceBookId = Test.getStandardPricebookId();

		PricebookEntry standardPBE = new PricebookEntry(Pricebook2Id = standardPriceBookId, Product2Id = product.Id, UnitPrice = 10, IsActive = true, UseStandardPrice = false);

        return  standardPBE;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This methods creates an Order Product
	* @param       	 - order: The Order the Order Product belongs to
	* 				 - pbe: The PriceBookEntry containing the reference to the Product
	* @return        - An instance of Order Product
	*/
	public static OrderItem createOrderProduct(Order order, PricebookEntry pbe) {
		OrderItem orderProduct = new OrderItem();

		orderProduct.OrderId 			= order.Id;
		orderProduct.PricebookEntryId 	= pbe.Id;
		orderProduct.Quantity 			= 5;
		orderProduct.UnitPrice 			= 2;
		orderProduct.Delivery_Days__c	= 'Monday; Tuesday';

		return orderProduct;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This methods creates an AcctSeed__Accounting_Settings__c
	* @param       	 - The Order the Order Product belongs to
	* @param       	 - The PriceBookEntry containing the reference to the Product
	* @return        - An instance of Order Product
	*/
	public static AcctSeed__Billing_Format__c createPDFFormat(String name, String pType, String vfPage, String emailTemplate) {
		AcctSeed__Billing_Format__c pdfFormat = new AcctSeed__Billing_Format__c();

		pdfFormat.Name 									= name;
		pdfFormat.AcctSeed__Type__c 					= pType;
		pdfFormat.AcctSeed__Visualforce_PDF_Page__c		= vfPage;
		pdfFormat.AcctSeed__Default_Email_Template__c 	= emailTemplate;

		return pdfFormat;
	}

	/* Stripe related methods */

	public static void createDefault_StripeSettings() {		
		Stripe_Settings__c setting = new Stripe_Settings__c();

		setting.Delayed_Webhook_Processor_ID__c = '';
		setting.Is_Live_Environment__c = false;
		
		setting.Stripe_Publishable_Live_Key__c = '1';
		setting.Stripe_Secret_Live_Key__c = '1';

		setting.Stripe_Publishable_Test_Key__c = 'pk_test_JOgw2m8hW3qkMsAi7adInHo3';
		setting.Stripe_Secret_Test_Key__c = 'sk_test_PTr3jTvEqY9EjcadxVK6eM1p';

		setting.Charge_Description_Pattern__c = 'Protanos Bakery <Name>';

		insert setting;
	}

	/* END Stripe related methods */


	/* BILLINGS */

	private static AcctSeed__Billing__c createBilling(Boolean invoice, Id accountId, Date pDate, Integer quantity, Decimal unitPrice) {
		AcctSeed__Billing__c result = new AcctSeed__Billing__c();

		result.AcctSeed__Status__c = 'Approved';
		result.AcctSeed__Customer__c = accountId;
		result.AcctSeed__Date__c = pDate;
		result.AcctSeed__PO_Number__c = '12345';

		insert result;

		AcctSeed__Billing_Line__c billingLine = new AcctSeed__Billing_Line__c();
		billingLine.AcctSeed__Billing__c = result.Id;
		billingLine.AcctSeed__Hours_Units__c = Math.abs(quantity);//Quantity
		billingLine.AcctSeed__Rate__c = Math.abs(unitPrice) * (invoice ? 1 : -1);//Unit Price
		billingLine.AcctSeed__Date__c = pDate;

		AcctSeed__GL_Account__c glAccount = [SELECT Id FROM AcctSeed__GL_Account__c LIMIT 1];
		billingLine.AcctSeed__Revenue_GL_Account__c = glAccount.Id;

		insert billingLine;

		return result;
	}

	public static AcctSeed__Billing__c createInvoice(Id accountId, Date pDate, Integer quantity, Decimal unitPrice) {
		return createBilling(true, accountId, pDate, quantity, unitPrice);
	}

	public static AcctSeed__Billing__c createCreditMemo(Id accountId, Date pDate, Integer quantity, Decimal unitPrice) {
		return createBilling(false, accountId, pDate, quantity, unitPrice);
	}

	public static AcctSeed__Billing_Credit_Memo__c applyCreditMemoToBilling(AcctSeed__Billing__c invoice, AcctSeed__Billing__c creditMemo, Decimal appliedAmount) {
		//unpost invoice and credit memo		
		AcctSeed.BillingPostService.unpostBillings(new List<AcctSeed__Billing__c> { invoice, creditMemo });

		AcctSeed__Billing_Credit_Memo__c billingCreditMemo = new AcctSeed__Billing_Credit_Memo__c();
		billingCreditMemo.AcctSeed__Billing_Invoice__c = invoice.Id;
		billingCreditMemo.AcctSeed__Billing_Credit_Memo__c = creditMemo.Id;
		billingCreditMemo.AcctSeed__Amount__c = appliedAmount;

		AcctSeed__Accounting_Period__c accountingPeriod = [SELECT Id FROM AcctSeed__Accounting_Period__c LIMIT 1];
		billingCreditMemo.AcctSeed__Accounting_Period__c = accountingPeriod.Id;

		//insert billingCreditMemo;

		//post invoice and credit memo
		//AcctSeed.BillingPostService.postBillings(new List<AcctSeed__Billing__c> { invoice, creditMemo });

		return billingCreditMemo;
	}

	/* END BILLINGS */


	/* CASH RECEIPTS */

	public static AcctSeed__Cash_Receipt__c createCashReceipt(Id accountId, Date pDate, Decimal amount) {
		AcctSeed__Cash_Receipt__c result = new AcctSeed__Cash_Receipt__c();

		result.AcctSeed__Account__c = accountId;
		result.AcctSeed__Purpose__c = 'Customer Receipt';
		result.AcctSeed__Amount__c = amount;
		result.AcctSeed__Receipt_Date__c = pDate;
		result.AcctSeed__Payment_Reference__c = 'cus_B0wCFC2jXlw4oB';

		AcctSeed__GL_Account__c glAccount = [SELECT Id FROM AcctSeed__GL_Account__c LIMIT 1];
		result.AcctSeed__Bank_Account__c = glAccount.Id;
		result.AcctSeed__Credit_GL_Account__c = glAccount.Id;

		return result;
	}

	public static AcctSeed__Billing_Cash_Receipt__c applyCashReceiptToBilling(AcctSeed__Cash_Receipt__c cashReceipt, AcctSeed__Billing__c billing, Decimal appliedAmount) {
		AcctSeed__Billing_Cash_Receipt__c result = new AcctSeed__Billing_Cash_Receipt__c();

		result.AcctSeed__Billing__c = billing.Id;
		result.AcctSeed__Cash_Receipt__c = cashReceipt.Id;
		result.AcctSeed__Applied_Amount__c = appliedAmount;

		return result;
	}

	/* END CASH RECEIPTS */


	/* STATEMENTS */

	public static List<Statement__c> generateStatementsForAccount(Id accountId, StatementManager_Helper.StatementType sType) {
		Integer startMonth = -5;
		Integer endMonth = startMonth + 1;

		Date todayDate = Date.today();

		List<Statement__c> result = new List<Statement__c>();

		for(Integer i = 1; i <= 5; i++) {
			Date startDate = Date.newInstance(todayDate.year(), i, 1);
			Date endDate = startDate.addMonths(1);

			Statement__c statement = new Statement__c();

			statement.Account__c = accountId;
			statement.Balance__c = 100 * i;
			statement.Period_Start_Date__c = startDate;
			statement.Period_End_Date__c = endDate;
			statement.Type__c = String.valueOf(sType);

			result.add(statement);
		}

		return result;
	}

	/* END STATEMENTS */
}