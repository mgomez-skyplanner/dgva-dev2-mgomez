/**
* <p> 
* Version log:
* <ul>
* <li>1.00 09/28/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 09/28/2017
* @description 	 - This class is a wrapper to contain Statement rows. It is implemented in a first level class (not a nested class) in order to be accesible from VF components
*/

public with sharing class Statement_Row {
	public Id AccountId { get; set; }

	public Datetime ActivityDate { get; set; }
	public Datetime SortingDate { get; set; }
	public Date DueDate { get; set; }

	public StatementManager_Helper.StatementActivity Activity { get; set; }
	public String ActivityType { get; set; }
	public String Reference { get; set; }
	public String PONumber { get; set; }

	public Decimal Total { get; set; }
	public Decimal Balance { get; set; }

	public Decimal BilledAmount { get; set; }
	public Decimal PaidAmount { get; set; }	
}