global class SP_ChangeOrderStatus_Schedule implements Schedulable {
	
	global Date endDate;

	global void execute(SchedulableContext sc) {
		
		SP_ChangeOrderStatus_Batch b = new SP_ChangeOrderStatus_Batch(endDate == null ? Date.today() : endDate, 'Activated', 'Delivered');
		database.executebatch(b, 1);
	}
}