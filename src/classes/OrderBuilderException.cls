/**
 * Exception class for order builder
 * @author William Garcia
 * @date   03/17/2017
 */
public class OrderBuilderException extends Exception {

	/**
	 * Method for asserts
	 * @author William Garcia
 	 * @date   03/17/2017
	 * @param result  Condition
	 * @param message Throw message
	 */
    public static void customAssert(Boolean result, String message){
        if(result == null || !result)
            throw new OrderBuilderException(message);
    }

    /**
     * Order builder custom exception method
     * This show custom and standard message combination
     * @author William Garcia
     * @date   03/17/2017
     * @param  ex  Standard exception
     * @param  msg Custom message
     * @return     Order builder custom exception
     */
    public OrderBuilderException(Exception ex, String msg) {
        if(ex.getTypeName() == 'OrderBuilderException')
            setMessage(ex.getMessage());
        else
            setMessage(msg + ' Error: ' + ex.getMessage());
    }

}