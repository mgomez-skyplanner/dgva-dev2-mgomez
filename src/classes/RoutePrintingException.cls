/**
 * Exception class for route pricing
 * @author William Garcia
 * @date   05/23/2017
 */
public class RoutePrintingException extends Exception {

    /**
     * Method for asserts
     * @author William Garcia
     * @date   05/23/2017
     * @param result  Condition
     * @param message Throw message
     */
    public static void customAssert(Boolean result, String message){
        if(result == null || !result)
            throw new RoutePrintingException (message);
    }

    /**
     * Route pricing custom exception method
     * This show custom and standard message combination
     * @author William Garcia
     * @date   05/23/2017
     * @param  ex  Standard exception
     * @param  msg Custom message
     * @return     Route pricing custom exception
     */
    public RoutePrintingException (Exception ex, String msg) {
        if(ex.getTypeName() == 'RoutePrintingException')
            setMessage(ex.getMessage());
        else
            setMessage(msg + ' Error: ' + ex.getMessage());
    }

}