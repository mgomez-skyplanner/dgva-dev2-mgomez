/**
* <p>
* Version log:
* <ul>
* <li>1.00 06/27/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 06/27/2017
* @description 	 - Represents an extension controller to provide actions related to the Order object
*/
 
global class OrderController {

	public static String Error_OrderNotFound 		 = 'Order not found';
	public static String Error_OrderIdEmpty 		 = 'Order Id can not be empty';
	public static String Error_NoProductFoundInOrder = 'No Products found for this Order';
	public static String Error_NoOrderProductAsParam = 'No Order Product to be voided. Please check at least one Order Product to be voided from the Order.';

	public String OrderId { get; private set; }

	public OrderController() {
		Init();
	}

	public OrderController(ApexPages.StandardController stdController) {
		Init();
	}

	private void Init() {
		this.OrderId = ApexPages.currentPage().getParameters().get('id');
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 08/24/2017
	* @description 	 - Indicate if at least one Order Billing have been partially or fully paid
	* @param       	 - orderId: The Id of the Order to void Products from
	*/
	private static Boolean check_OrderBilling_HasBeenPaid(String orderId) {
		List<AcctSeed__Billing__c> billings = [SELECT 
													Id,
													AcctSeed__Balance__c,
													AcctSeed__Total__c
												FROM
													AcctSeed__Billing__c
												WHERE
													Order__c =: orderId];
		
		for(AcctSeed__Billing__c billing : billings) {
			if(billing.AcctSeed__Balance__c < billing.AcctSeed__Total__c) {
				return true;
			}
		}

		return false;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/27/2017
	* @description 	 - Action to void an Order Product from Lightning
	* @param       	 - orderItemsIds: List of OrderItem Ids to be "ignored" from the Order
	* 				 - explanation: text with an explanation on why those Products are been removed from the Order
	*/
	@AuraEnabled
	@RemoteAction
	webservice static OrderProductRemover_Wrapper voidOrderProducts(String orderId, List<String> orderItemsIds, List<Decimal> quantitiesToVoid, List<String> explanations) {
		OrderProductRemover_Wrapper result = new OrderProductRemover_Wrapper();
		String error = '';

		if(String.isBlank(orderId))
			error = Error_OrderIdEmpty;
		else if(orderItemsIds.size() == 0)
			error = Error_NoOrderProductAsParam;
		else {
			List<Order> orders = [SELECT 
									Id,
									Status,
									IgnoreMinimumAmount__c,
									(
										SELECT 
											Id,
											OrderId,
											PricebookEntryId,
											UnitPrice,
											Quantity,
											Void_Explanation__c
										FROM
											OrderItems
										WHERE
											Id IN: orderItemsIds
									)
								FROM
									Order
								WHERE
									Id =: orderId];

			try {
				//following maps ensure the OrderItem.Id match the correct quantity to void and explanation
				Map<String, Decimal> orderItemId_QuantityToVoid_Map = new Map<String, Decimal>();
				Map<String, String> orderItemId_Explanation_Map = new Map<String, String>();

				for(Integer i = 0; i < orders[0].OrderItems.size(); i++) {
					OrderItem current = orders[0].OrderItems[i];
					Integer j = 0;

					for(; j < orderItemsIds.size(); j++) {
						if(current.Id == orderItemsIds[j]) {
							orderItemId_QuantityToVoid_Map.put(current.Id, quantitiesToVoid[j]);
							orderItemId_Explanation_Map.put(current.Id, explanations[j]);
						}
					}
				}

				List<OrderConversionUtility.OrderProductVoid_Wrapper> opWrappers = new List<OrderConversionUtility.OrderProductVoid_Wrapper>();

				for(Integer i = 0; i < orders[0].OrderItems.size(); i++) {
					if(orderItemId_QuantityToVoid_Map.containsKey(orders[0].OrderItems[i].Id)) {
						OrderConversionUtility.OrderProductVoid_Wrapper opWrapper = new OrderConversionUtility.OrderProductVoid_Wrapper();
						
						opWrapper.OrderProduct 	 = orders[0].OrderItems[i];
						opWrapper.QuantityToVoid = orderItemId_QuantityToVoid_Map.get(opWrapper.OrderProduct.Id);
						opWrapper.Explanation 	 = orderItemId_Explanation_Map.get(opWrapper.OrderProduct.Id);

						opWrappers.add(opWrapper);
					}
				}

				OrderConversionUtility.VoidOrderProduct(orders[0], opWrappers);
			}
			catch (Exception e) {
				error = e.getMessage();
			}
		}

		result = getOrderProducts(OrderId);
		result.Error = error;

		return result;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/28/2017
	* @description 	 - Load wrapped Order Items to bind data to the Order Product removing dialog
	* @param       	 - orderId: Id of the Order to load its Order Items
	*/
	@AuraEnabled
	@RemoteAction
	webservice static OrderProductRemover_Wrapper getOrderProducts(String orderId) {
		OrderProductRemover_Wrapper result = new OrderProductRemover_Wrapper();

		String error = '';

		if(String.isBlank(orderId))
			error = Error_OrderIdEmpty;
		else {
			try {
				//Marcel - 08/24/2017
				//if no Billings have been partially or fully paid
				if(!OrderController.check_OrderBilling_HasBeenPaid(orderId)) {

					List<OrderItem> orderItems = [SELECT 
													Id,
													PricebookEntry.Product2.Name,
													PricebookEntry.Product2Id,
													UnitPrice,
													Quantity,

													Order.OrderNumber,
													Order.Account.Name
												FROM
													OrderItem
												WHERE
													OrderId =: orderId];


					if(orderItems.size() == 0)
						error = Error_NoProductFoundInOrder;
					else {
						//key: OrderItem.Product2Id
						//value: Remaining OrderItem.Quantity after summing up all Quantities
						//in the end of the for loop, this map will contain only those Products with a remaining in the Order
						Map<Id, Decimal> product_Quantity = new Map<Id, Decimal>();

						//key: OrderItem.Product2Id
						//value: an OrderItem with Quantity > 0
						//in the end of the for loop, this map will contain only those Products with a remaining in the Order and one related OrderItem with Quantity > 0
						Map<Id, OrderItem> product_OrderItem = new Map<Id, OrderItem>();

						List<OrderProduct_Wrapper> products = new List<OrderProduct_Wrapper>();

						//calculate the remaining of each product after summing up negative quantities
						for(OrderItem oi : orderItems) {
							Decimal quantity = oi.Quantity;
							String productId = oi.PricebookEntry.Product2Id;

							if(product_Quantity.containsKey(productId)) {
								quantity = product_Quantity.get(productId) + oi.Quantity;
							}
							
							product_Quantity.put(productId, quantity);

							if(quantity > 0 && !product_OrderItem.containsKey(productId)) {
								product_OrderItem.put(productId, oi);
							}
						}

						for(Id productId : product_Quantity.keySet()) {
							Decimal remaining = product_Quantity.get(productId);

							if(remaining > 0 && product_OrderItem.containsKey(productId)) {
								OrderItem oi = product_OrderItem.get(productId);

								OrderProduct_Wrapper wrapper = new OrderProduct_Wrapper(oi, remaining);
								products.add(wrapper);
							}
						}

						result.Products = products;

						result.OrderId 		= orderItems[0].OrderId;
						result.OrderNumber 	= orderItems[0].Order.OrderNumber;
						result.OrderAccount = orderItems[0].Order.Account.Name;
					}
				}
				else
					error = 'This Order has a Billing fully or partially paid. Order Products can not be voided.';
			}
			catch (Exception e) {
				error = e.getMessage();
			}
		}

		result.Error = error;

		return result;
	}
	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/28/2017
	* @description 	 - Load wrapped Order Items to bind data to the Order Product removing dialog
	* @param       	 - orderId: Id of the Order to load its Order Items
	*/
	@AuraEnabled
	@RemoteAction
	webservice static List<String> getExplanationItems() {
		Schema.DescribeFieldResult fieldResult = OrderItem.Void_Explanation__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		List<String> result = new List<String>();
		for(Schema.PicklistEntry entry : ple) {
			result.add(entry.getLabel());
		}
		return result;
	}
	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 08/17/2017
	* @description 	 - Sets the Order.Status = 'Delivered'
	* @param       	 - orderId: Id of the Order to update
	*/
	@AuraEnabled
	@RemoteAction
	webservice static String setOrderAsDelivered(String orderId) {
		String error = '';
		if(String.isBlank(orderId))
			error = Error_OrderIdEmpty;
		else {
			try {
				List<Order> orders = [SELECT 
											Id,
											Status
										FROM
											Order
										WHERE
											Id =: orderId];
				if(orders.size() == 0)
					error = Error_OrderNotFound;
				else {
					Order order = orders[0];
					order.Status = 'Delivered';
					update order;
				}
			}
			catch (Exception e) {
				error = e.getMessage();
			}
		}
		if(!String.isBlank(error))
			return error;
		return '1';
	}

	global class OrderProduct_Wrapper {
		public Boolean Checked { get; set; }
		public OrderItem OrderProduct { get; set; }
		public Decimal OldQuantity { get; set; }
		public Decimal Quantity { get; set; }
		public String Explanation { get; set; }

		public OrderProduct_Wrapper() {
			this.OrderProduct 	= NULL;
			this.Checked 		= false;
			this.OldQuantity 	= 0;
			this.Quantity 		= 0;
			this.Explanation = '';
		}

		public OrderProduct_Wrapper(OrderItem oProduct, Decimal quantity) {
			this.OrderProduct 	= oProduct;
			this.Checked 		= checked;
			this.OldQuantity 	= quantity;
			this.Quantity 	  	= 0;
			this.Explanation = '';
		}
	}

	global class OrderProductRemover_Wrapper {
		public String OrderId { get; set; }
		public String OrderNumber { get; set; }
		public String OrderAccount { get; set; }

		public List<OrderProduct_Wrapper> Products { get; set; }

		public String Error { get; set; }

		public OrderProductRemover_Wrapper() {
			this.OrderId = '';
			this.OrderNumber = '';
			this.OrderAccount = '';

			this.Products = new List<OrderProduct_Wrapper>();
			this.Error = '';
		}
	}
}