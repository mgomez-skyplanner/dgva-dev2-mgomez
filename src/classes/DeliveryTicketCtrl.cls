/**
 * Provides controller support for the DeliveryTicket.page,
 * which should render as a PDF.
 * @author Fernando Gomez
 * @date May 23rd, 2017
 * @version 1.0: initial by author
 */
global class DeliveryTicketCtrl {
	global List<Order> orders { get; private set; }

	/**
	 * Main constructor
	 * @author Fernando Gomez
	 * @date May 23rd, 2017
	 * @version 1.0: initial by author
	 */
	global DeliveryTicketCtrl() {
		setup();
	}

	/**
	 *
	 * @author Fernando Gomez
	 * @date May 23rd, 2017
	 * @version 1.0: initial by author
	 */
	private void setup() {
		Map<String, String> params;
		OrderManager m = new OrderManager();

		// we extract the parameters
		params = ApexPages.currentPage().getParameters();

		// we need either an orderId, or both the date and routeId
		// to fetch the orders.
		if (params.containsKey('orderId'))
			orders = new List<Order> { m.fetch(params.get('orderId')) };
		else if (params.containsKey('date') && params.containsKey('routeId') && params.containsKey('actDate')) {			
			orders = m.fetchByRouteActivationDate(params.get('routeId'), parseDate(params.get('date')), parseDateTime(params.get('actDate')));
		}
		else if (params.containsKey('date') && params.containsKey('routeId'))
			orders = m.fetchByRoute(params.get('routeId'), parseDate(params.get('date')));
	}

	/**
	 *
	 * @author Fernando Gomez
	 * @date May 23rd, 2017
	 * @version 1.0: initial by author
	 */
	private Date parseDate(String dt) {
		try {
			List<String> dstr = dt.split('-');
			return Date.newInstance(
				Integer.valueOf(dstr.get(0)),
				Integer.valueOf(dstr.get(1)),
				Integer.valueOf(dstr.get(2)));
		} catch (Exception ex) {
			throw new DeliveryTicketCtrlException(
				'The date parameter is invalid. ' +
				'It cannot be empty, and must comply with' +
				' with the format: yyyy/mm/dd.');
		}
	}

	/**
	 *
	 * @author Marcel Gomez
	 * @date 09/16/2017
	 * @version 1.0: initial by author
	 */
	private DateTime parseDateTime(String dt) {
		try {
			return Datetime.newInstance(Long.valueOf(dt));
		} catch (Exception ex) {
			throw new DeliveryTicketCtrlException(
				'The datetime parameter is invalid. ' +
				'It cannot be empty, and must comply with' +
				' with the format of tick counts');
		}
	}

	/**
	 * Throwable exception
	 * @author Fernando Gomez
	 * @date May 23rd, 2017
	 * @version 1.0: initial by author
	 */
	private class DeliveryTicketCtrlException extends Exception { }
}