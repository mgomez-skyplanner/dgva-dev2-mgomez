/**
* <p> 
* Version log:
* <ul>
* <li>1.00 07/06/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 07/06/2017
* @description 	 - This class provides methods to perform the interaction with Stripe (https://stripe.com)
*/

public with sharing class WebManager_Stripe {

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/06/2017
	* @description 	 - Indicates whether the tasks performed by this class methods are in Test Mode or not
	*/
	public static Boolean IsTest { get; set; }

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/06/2017
	* @description 	 - Charges a Billing on Stripe according to its Balance
	* @param 		 - billingId: Id of the Billing to be charged
	* @return 		 - The instance of StripeCharge created as a result of the operation
	*/
	public static StripeCharge CreateCharge(Id billingId) {
		StripeAPI.IsTest = IsTest;

		List<AcctSeed__Billing__c> billingsList = [SELECT
														Id,
														Name,
														AcctSeed__Balance__c,
														AcctSeed__Customer__c,
														AcctSeed__Customer__r.AcctSeed__Stripe_Customer_Id__c,

														AcctSeed__Proprietary_Billing_Number__c,
														AcctSeed__Customer__r.Name
													FROM
														AcctSeed__Billing__c
													WHERE
														Id =: billingId];

		if(billingsList.size() == 0)
			throw new ExceptionHelper.BillingNotFound_Exception(billingId);

		AcctSeed__Billing__c billing = billingsList[0];
		String customerId 			 = billing.AcctSeed__Customer__r.AcctSeed__Stripe_Customer_Id__c;

		//this variable is just for convenience, it tells the code when to use the custom setting or not depending on a human decision
		//09/05/2017 - Jorgito's decision
		Boolean useCustomSetting = false;

		String description = billing.AcctSeed__Customer__r.Name + ' - ' + billing.AcctSeed__Proprietary_Billing_Number__c;

		if(useCustomSetting) {
			//get the value from the custom setting Stripe_Settings__c.Charge_Description_Pattern__c
			String customSetting = StripeAPI.ChargeDescriptionPattern;
			if(!String.isBlank(customSetting)) {
				description = customSetting;

				//if value contains a placeholder, then replace it with the Billing field				
				String field = GetFieldFromPlaceholder(description);

				if(billing.get(field) != NULL) {
					description = description.replace('<' + field + '>', String.valueOf(billing.get(field)));
				}
			}
		}

		Map<String, String> chargeParams = new Map<String, String>();
		chargeParams.put('currency', 'usd'); //Company Information -> Currency Locale (not available from APEX)
		chargeParams.put('description', description); //custom setting with placeholder

		system.debug('------------------------ billing Balance: ');
		system.debug(billing.AcctSeed__Balance__c);

		return StripeCharge.create(customerId, billing.AcctSeed__Balance__c, chargeParams, NULL);
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/07/2017
	* @description 	 - Extracts a Billing field name from a placeholder in the form "<field_name>".
	* 				   IMPORTANT: make sure to include the Billing field in the SELECT statement on method PaymentBatchProcessor.start to avoid unhandled Exceptions
	* @param 		 - placeholder: the string that represents the placeholder
	* @return 		 - The name of the Billing field extracted from the placeholder. i.e. Placeholder = "<Name>", Result = "Name"
	*/
	public static String GetFieldFromPlaceholder(String placeholder) {
		String result = '';

		if(!String.isBlank(placeholder)) {
			result = placeholder;

			//if value contains a placeholder, then replace it with the Billing field
			if(result.indexOf('<') > 0 && result.indexOf('>') > 0) {
				result = result.substringBetween('<', '>');
			}
		}

		return result;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/06/2017
	* @description 	 - Creates a Cash Receipt (AcctSeed__Cash_Receipt__c) record based on a succesful Stripe Charge made to a Billing
	* @param 		 - billingId: Id of the Billing charged
	* @param 		 - accountId: Id of the Account of the Billing charged
	* @param 		 - charge: StripeCharge record resulted from the Charge operation on Stripe
	* @return 		 - An instance of AcctSeed__Cash_Receipt__c
	*/
	public static AcctSeed__Cash_Receipt__c CreateCashReceipt(Id accountId, String customerId, Decimal amount) {
		AcctSeed__Cash_Receipt__c cashReceipt = new AcctSeed__Cash_Receipt__c();

		cashReceipt.AcctSeed__Account__c 			= accountId;
		cashReceipt.AcctSeed__Payment_Reference__c  = customerId;//charge.customer_id;
		cashReceipt.AcctSeed__Amount__c 			= amount;
		cashReceipt.AcctSeed__Receipt_Date__c 		= Date.today();
		cashReceipt.AcctSeed__Purpose__c 			= 'Debit/Credit Card'; //changed on 09/15/2017
		cashReceipt.AcctSeed__Status__c 			= 'Posted';

		system.debug('------------------------ CashReceipt: ');
		system.debug(cashReceipt);

		return cashReceipt;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 07/06/2017
	* @description 	 - Charges a Billing on Stripe according to its Balance
	* @param 		 - billing: the Billing to be charged
	* @return 		 - The instance of StripeCharge created as a result of the operation
	*/
	public static AcctSeed__Billing_Cash_Receipt__c CreateBillingCashReceipt(Id billingId, Id billingAccountingPeriodId, AcctSeed__Cash_Receipt__c cashReceipt) {
		AcctSeed__Billing_Cash_Receipt__c billingCashReceipt = new AcctSeed__Billing_Cash_Receipt__c();

		billingCashReceipt.AcctSeed__Billing__c 	 		= billingId;
		billingCashReceipt.AcctSeed__Cash_Receipt__c 		= cashReceipt.Id;
		billingCashReceipt.AcctSeed__Applied_Amount__c		= cashReceipt.AcctSeed__Amount__c;
		billingCashReceipt.AcctSeed__Accounting_Period__c	= billingAccountingPeriodId;

		return billingCashReceipt;
	}
}