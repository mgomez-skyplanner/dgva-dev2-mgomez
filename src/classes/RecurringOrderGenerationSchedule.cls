/**
 * Schedule clas to run recurring orders generation batch
 * @author	William Garcia
 * @date	05/10/2017
 */
global class RecurringOrderGenerationSchedule implements Schedulable {
	
	global void execute(SchedulableContext sc) {
		// Get settings
		Recurring_Order_Settings__c stt = Recurring_Order_Settings__c.getOrgDefaults();
		
		// Run batch
		System.scheduleBatch(new RecurringOrderGenerationBatch(),'RecurringOrderGenerationBatch', 0, 
			stt.Batch_Records_size__c == null ? 200 : stt.Batch_Records_size__c.intValue());
	}

}