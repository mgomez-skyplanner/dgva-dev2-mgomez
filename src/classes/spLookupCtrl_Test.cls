/**
 * @author       William Garcia
 * @date         10/27/2017
 * @description  StatementController test class.
 */
@isTest (seeAllData=false)
public class spLookupCtrl_Test {
	
	@testSetup
    static void data() {
    	Route__c route1 = new Route__c(
			Name = 'route1'
   		);
   		insert route1;

    	Account acc1 = new Account(
			Name = 'Account1',
			BillingCountry = 'C1',
			BillingCity = 'C2',
			Total_Balance__c = 50,
			Route__c = route1.Id
		);
    }

    @isTest
	static void testLookup() {
		spLookupCtrl ctrl = new spLookupCtrl();
		List<SObject> result = spLookupCtrl.lookup('Account', 'Name' , 'Name', 'Account1', 
								new Map<String, String>{'Name' => 'Account1'});
	}
}