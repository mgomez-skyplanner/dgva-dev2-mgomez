/**
* <p>
* Version log:
* <ul>
* <li>1.00 06/28/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 06/28/2017
* @description 	 - This class represents a scheduler for batch AccountBalances_Batch
*/
 
global class AccountBalances_Scheduler implements Schedulable {

	/*
	private static String scheduleTimming = '0 00 00 * * ?';  //Every Day at Midnight 

    global static String scheduleMe(String scheduleTime) {
        AccountBalances_Scheduler scheduler = new AccountBalances_Scheduler(); 
        return System.schedule('Account Balance Scheduled Job', String.isBlank(scheduleTime) ? scheduleTimming : scheduleTime, scheduler);
    }
    */

    global void execute(SchedulableContext sc) {
        AccountBalances_Batch batch = new AccountBalances_Batch();
        
        Database.executeBatch(batch, 100);
    }
}