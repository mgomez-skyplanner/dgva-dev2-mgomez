/**
 * Bath to gets all the Recurring Orders (Type = 'Recurring') 
 * and generate for each of those recurring orders one Regular Order.
 * @author  William Garcia
 * @date    05/09/2017
 */
global class RecurringOrderGenerationBatch 
    implements Database.Batchable<sObject>, Database.Stateful {
    
    private Integer failures = 0;
    private Date    runDate;
    
    global RecurringOrderGenerationBatch(){
        runDate = Date.Today();
    }
    
    global RecurringOrderGenerationBatch(Integer year, Integer month, Integer day){
        runDate = Date.newInstance(year, month, day);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return RecurringOrderManager.retrieveOrdersQL();
    }
    
    global void execute(Database.BatchableContext BC, List<Order> orders) {
        RecurringOrderManager manager = new RecurringOrderManager(orders);
        manager.createOrders(runDate.year(), runDate.month(), runDate.day());
    }
    
    global void finish(Database.BatchableContext BC) {}

    /* Execute batch
        parameters:
        1- new instance of batch class
        2- name that is going to get the job
        3- time tu start the job
        4- how many records to analize at the same time(put one to keep safe of governor limits)
        System.scheduleBatch(new RecurringOrderGenerationBatch(),'RecurringOrderGenerationBatch',0, 100);
    */
}