/**
 * Class to keep global variables
 * @author	William Garcia
 * @date	05/09/2017
 */
public class Constants {
	
	/********** ORDER TYPES ****************/
	public static final String ORDER_TYPE_RECURRING = 'Recurring';
	public static final String ORDER_TYPE_REGULAR = 'Regular';

	/********** ORDER STATUSES ****************/
	public static final String ORDER_STATUS_DRAFT = 'Draft';
	public static final String ORDER_STATUS_PREPARED = 'Prepared';
	public static final String ORDER_STATUS_ACTIVATED = 'Activated';
	public static final String ORDER_STATUS_CANCELLED = 'Cancelled';

}