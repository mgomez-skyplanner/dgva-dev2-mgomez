/**
* <p> 
* Version log:
* <ul>
* <li>1.00 06/30/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 06/30/2017
* @description 	 - Test class for classes:
* 					AccountBalances_Utility (94% - 06/30/2017)
* 					AccountBalances_Batch (100% - 06/30/2017)
* 					AccountBalances_Scheduler (100% - 06/30/2017)
*/
 
@isTest
public with sharing class AccountBalances_Utility_Test {

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - Test method for constructor of class AccountBalances_Utility
	*/
	@isTest
	static void Test_Constructor() {
		Account account = new Account();
		ApexPages.StandardController stdController = new ApexPages.StandardController(account);

		AccountBalances_Utility abUtility = new AccountBalances_Utility(stdController);
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - Test method for class AccountBalances_Utility, AccountBalances_Batch, AccountBalances_Scheduler
	*/
	@isTest
	static void test_UpdateAccountBalances() {
		Test.startTest();

		TestClass_Utility.createCustomSettingsData();

		Route__c route 	= TestClass_Utility.createRoute('Test Route');
		insert route;

		Account account = TestClass_Utility.createAccount('Test Account', route);
		insert account;

		Opportunity opp = TestClass_Utility.createOpportunity(account);
		insert opp;

		Order order 			= TestClass_Utility.createOrder(account, opp, 'Draft');
		insert order;

		Product2 product1 		= TestClass_Utility.createProduct('Test Product 1');
		insert product1;

		Id standardPriceBookId = Test.getStandardPricebookId();
		PricebookEntry pbe 		= [SELECT Id FROM PricebookEntry WHERE Product2Id =: product1.Id AND PriceBook2Id =: standardPriceBookId];

		OrderController.OrderProductRemover_Wrapper oprWrapper = OrderController.getOrderProducts(order.Id);
		system.assertEquals(OrderController.Error_NoProductFoundInOrder, oprWrapper.Error);

		OrderItem orderProduct1 = TestClass_Utility.createOrderProduct(order, pbe);
		insert orderProduct1;

		//Mandatory setting for Accounting Seed
		TestClass_Utility.CreateDefaultDataForBillings(account, order, product1);

		order.Status = 'Prepared';
		update order;

		TriggerControlClass.stopOrderTriggers = true;

		order.Status = 'Activated';
		update order;

		TriggerControlClass.stopOrderTriggers = true;

		//reload order
		BillingManager.LoadCustomSettings();
		order = BillingManager.ReloadOrder(order.Id);

		AcctSeed__Billing__c billing = BillingManager.GenerateBillingFromOrder(order, true);

		//change due date to the past to update field Account.Past_Due__c
		billing.AcctSeed__Due_Date2__c = Date.today().addDays(-10);
		update billing;

		//test the batch and scheduler
		AccountBalances_Scheduler scheduler = new AccountBalances_Scheduler();
		scheduler.execute(NULL);

		Test.stopTest();

		AccountBalances_Utility.UpdateAccountBalances(new Set<Id> { account.Id });
		system.assertEquals(billing.AcctSeed__Balance__c, account.Balance__c);
		system.assertEquals(billing.AcctSeed__Balance__c, account.Past_Due__c);

		AccountBalances_Utility.UpdateAccountBalances(account.Id);
	}
}