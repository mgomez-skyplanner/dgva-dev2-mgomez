/**
* <p> 
* Version log:
* <ul>
* <li>1.00 10/11/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 10/11/2017
* @description 	 - This class provides methods to generate an Activity Statement
* @version 
* 	1.01		 10/26/2017 William Garcia: Statement develoment, continuation...
*/

public with sharing class StatementBuilder_Activity {
	
	public List<Id> AccountIds { get; set; }

	public Date StatementStartDate { get; set; }
	public Date StatementEndDate { get; set; }

	public Map<Id, Decimal> Balance { get; set; }
	public Map<Id, Decimal> PurchaseActivity { get; set; }
	public Map<Id, List<Statement_Row>> LineItems { get; set; }

	public Map<Id, Map<Id, AcctSeed__Billing__c>> Invoices { get; set; }

	public Map<Id, Map<Id, AcctSeed__Billing__c>> CreditMemos { get; set; }
	public Map<Id, Map<Id, AcctSeed__Billing_Credit_Memo__c>> CreditMemosApplied { get; set; }
	
	public Map<Id, Map<Id, AcctSeed__Cash_Receipt__c>> CashReceipts { get; set; }
	public Map<Id, Map<Id, AcctSeed__Billing_Cash_Receipt__c>> CashReceiptsApplied { get; set; }

	//Indicates the amount of money paid for a Billing after the Statement Date
	private Map<Id, Decimal> billing_amountPaidLater_Map { get; set; }

	public Boolean GeneratedFromBatch { get; set; }
	public Boolean IsSetup { get; set; }

	public Boolean SendEmail { get; set; }

	public StatementBuilder_Activity(List<Id> accountIds, Date startDate, Date endDate, Boolean send) {
		
		this.AccountIds			= accountIds;
		this.StatementStartDate	= startDate;
		this.StatementEndDate	= endDate;

		this.Balance 			= new Map<Id, Decimal>();
		this.PurchaseActivity	= new Map<Id, Decimal>();
		this.LineItems 			= new Map<Id, List<Statement_Row>>();

		this.Invoices 			= new Map<Id, Map<Id, AcctSeed__Billing__c>>();

		this.CreditMemos 		= new Map<Id, Map<Id, AcctSeed__Billing__c>>();
		this.CreditMemosApplied = new Map<Id, Map<Id, AcctSeed__Billing_Credit_Memo__c>>();
		
		this.CashReceipts		= new Map<Id, Map<Id, AcctSeed__Cash_Receipt__c>>();
		this.CashReceiptsApplied= new Map<Id, Map<Id, AcctSeed__Billing_Cash_Receipt__c>>();

		this.GeneratedFromBatch = false;
		this.IsSetup = false;
		this.SendEmail = send;

		System.debug('StatementBuilder_Activity-->> ' + this);
	}

	private void Load_Invoices_CreditMemos() {
		List<AcctSeed__Billing__c> allUnappliedBillings = [SELECT 
																Id,
																Name,
																CreatedDate,
																AcctSeed__Customer__c,
																AcctSeed__Date__c,
																AcctSeed__Proprietary_Billing_Number__c,
																AcctSeed__PO_Number__c,
																AcctSeed__Due_Date2__c,
																AcctSeed__Total__c,
																AcctSeed__Balance__c,
																AcctSeed__Type__c															
															FROM
																AcctSeed__Billing__c
															WHERE 
																AcctSeed__Customer__c IN: this.AccountIds AND 
																AcctSeed__Date__c >=: this.StatementStartDate AND 
															    AcctSeed__Status__c = 'Posted'

																//AcctSeed__Date__c <: this.StatementEndDate.addDays(1) 															
															ORDER BY
																AcctSeed__Date__c];

		for(AcctSeed__Billing__c billing : allUnappliedBillings) {
			//create the key/value pair for an Account if does not exists
			if(billing.AcctSeed__Type__c == 'Invoice'){
				if(!this.Invoices.containsKey(billing.AcctSeed__Customer__c)){
					this.Invoices.put(billing.AcctSeed__Customer__c, new Map<Id, AcctSeed__Billing__c>());				
				}

				this.Invoices.get(billing.AcctSeed__Customer__c).put(billing.Id, billing);	
			}
			else {
				if(!this.CreditMemos.containsKey(billing.AcctSeed__Customer__c)){
					this.CreditMemos.put(billing.AcctSeed__Customer__c, new Map<Id, AcctSeed__Billing__c>());				
				}

				this.CreditMemos.get(billing.AcctSeed__Customer__c).put(billing.Id, billing);
			}
		}
	}

	private void LoadCashReceipts() {
		List<AcctSeed__Cash_Receipt__c> cashReceipts = [SELECT 
															Id,
															Name,
															CreatedDate,
															AcctSeed__Account__c,
															AcctSeed__Balance__c,
															AcctSeed__Receipt_Date__c,
															AcctSeed__Amount__c,
															AcctSeed__Purpose__c,
															AcctSeed__Payment_Reference__c
														FROM
															AcctSeed__Cash_Receipt__c
														WHERE 
															AcctSeed__Account__c IN: this.AccountIds AND
															AcctSeed__Receipt_Date__c >=: this.StatementStartDate AND
															AcctSeed__Status__c = 'Posted'
															//AcctSeed__Receipt_Date__c <: this.StatementEndDate.addDays(1)
														ORDER BY 
															AcctSeed__Receipt_Date__c ASC];

		for(AcctSeed__Cash_Receipt__c cashReceipt : cashReceipts) {
			//include only those Cash Receipts where Balance > 0 or were applied after date
			//if(cashReceipt.AcctSeed__Balance__c > 0 || cashReceipt.AcctSeed__Billing_Cash_Receipts__r.size() > 0) {
				if(!this.CashReceipts.containsKey(cashReceipt.AcctSeed__Account__c))
					this.CashReceipts.put(cashReceipt.AcctSeed__Account__c, new Map<Id, AcctSeed__Cash_Receipt__c>());

				this.CashReceipts.get(cashReceipt.AcctSeed__Account__c).put(cashReceipt.Id, cashReceipt);
			//}
		}
	}

	private void LoadAppliedCreditMemos() {

		List<AcctSeed__Billing_Credit_Memo__c> credtiMemos = [SELECT 
														AcctSeed__Billing_Invoice__c,
														AcctSeed__Billing_Credit_Memo__c,
														AcctSeed__Amount__c,
														CreatedDate,

														//credit memo record fields
														AcctSeed__Billing_Credit_Memo__r.Name,
														AcctSeed__Billing_Credit_Memo__r.AcctSeed__Customer__c,
														AcctSeed__Billing_Credit_Memo__r.AcctSeed__Proprietary_Billing_Number__c,
														AcctSeed__Billing_Credit_Memo__r.AcctSeed__PO_Number__c,
														AcctSeed__Billing_Credit_Memo__r.AcctSeed__Type__c
													FROM
														AcctSeed__Billing_Credit_Memo__c
													WHERE
														AcctSeed__Billing_Credit_Memo__r.AcctSeed__Customer__c IN: this.AccountIds AND
														CreatedDate >=: this.StatementStartDate AND 														
														CreatedDate <: this.StatementEndDate.addDays(1)];

		for(AcctSeed__Billing_Credit_Memo__c billingCreditMemo : credtiMemos) {
			Id accountId = billingCreditMemo.AcctSeed__Billing_Credit_Memo__r.AcctSeed__Customer__c;

			if(!this.CreditMemosApplied.containsKey(accountId))
				this.CreditMemosApplied.put(accountId, new Map<Id, AcctSeed__Billing_Credit_Memo__c>());

			this.CreditMemosApplied.get(accountId).put(billingCreditMemo.Id, billingCreditMemo);
		}
	}

	private void LoadAppliedCashReceipts() {
		
		List<AcctSeed__Billing_Cash_Receipt__c> appliedCashReceipts = [SELECT 
														AcctSeed__Billing__c,
														AcctSeed__Cash_Receipt__c,
														AcctSeed__Applied_Amount__c,
														CreatedDate,

														//cash receipt record fields
														AcctSeed__Cash_Receipt__r.Name,
														AcctSeed__Cash_Receipt__r.CreatedDate,														
														AcctSeed__Cash_Receipt__r.AcctSeed__Account__c,
														AcctSeed__Cash_Receipt__r.AcctSeed__Receipt_Date__c,
														AcctSeed__Cash_Receipt__r.AcctSeed__Purpose__c,														
														AcctSeed__Cash_Receipt__r.AcctSeed__Payment_Reference__c ,														
														AcctSeed__Cash_Receipt__r.AcctSeed__Amount__c  ,														

														//billing record fields
														AcctSeed__Billing__r.AcctSeed__PO_Number__c
													FROM
														AcctSeed__Billing_Cash_Receipt__c
													WHERE
														AcctSeed__Cash_Receipt__r.AcctSeed__Account__c IN: this.AccountIds AND
														CreatedDate >=: this.StatementStartDate AND 														
														CreatedDate <: this.StatementEndDate.addDays(1)];

		for(AcctSeed__Billing_Cash_Receipt__c billingCashReceipt : appliedCashReceipts) {
			if(!this.CashReceiptsApplied.containsKey(billingCashReceipt.AcctSeed__Cash_Receipt__r.AcctSeed__Account__c))
				this.CashReceiptsApplied.put(billingCashReceipt.AcctSeed__Cash_Receipt__r.AcctSeed__Account__c, new Map<Id, AcctSeed__Billing_Cash_Receipt__c>());

			this.CashReceiptsApplied.get(billingCashReceipt.AcctSeed__Cash_Receipt__r.AcctSeed__Account__c).put(billingCashReceipt.Id, billingCashReceipt);
		}
	}

	

	public List<Statement__c> generateActivityStatement(Boolean insertDirectly) {
		this.Load_Invoices_CreditMemos();
		//this.LoadAppliedCreditMemos();
		this.LoadCashReceipts();
		//this.LoadAppliedCashReceipts();
		
		
		//if(billing.AcctSeed__Type__c != 'Invoice' && balance != 0)
				//system.assert(false, 
				//	' Invoices: ' + Invoices + 
				//	' CreditMemosApplied: ' + CreditMemosApplied + 
				//	' CashReceiptsApplied: ' + CashReceiptsApplied + 
					//' **** Total: ' + billing.AcctSeed__Total__c + 
					//' **** Balance: ' + balance + 
					//' **** ' + billing.AcctSeed__Billing_Cash_Receipts__r + 
					//' **** ' + billing.AcctSeed__Billing_Credit_Memos1__r + 
					//' **** ' + billing.AcctSeed__Billing_Credit_Memos__r
				//);


		Map<Id, Statement__c> previousStatements = StatementManager_Helper.GetPreviousStatement(this.AccountIds, this.StatementStartDate);		

		StatementBuilder_Outstanding outBuilder = new StatementBuilder_Outstanding(this.AccountIds, this.StatementStartDate.addDays(-1), false);
		List<Statement__c> outStatements = outBuilder.generateOutStandingStatement(false);


		for(Id accountId : this.AccountIds) {

			Decimal finalPurchaseActivity = 0;

			Decimal currentBalance = 0;
			Decimal creditMemosAfter = 0;
			Decimal cashReceiptsAfter = 0;
			Decimal invoicesAfter = 0;

			Decimal credits  = 0;
			Decimal payments = 0;
			List<Statement_Row> account_rows = new List<Statement_Row>();

			Decimal previousBalance = previousStatements.get(accountId) != NULL ? previousStatements.get(accountId).Balance__c : 0;

			//if no StatementStartDate was defined, then take the End Date of the previous Statement
			//otherwise get an arbitrary Date of one month ago
			if(this.StatementStartDate == NULL) {
				if(previousStatements.get(accountId) != NULL)
					this.StatementStartDate = previousStatements.get(accountId).Period_End_Date__c;
				else
					this.StatementStartDate = Date.today().addYears(-10);
			}

			System.debug('StatementBuilder_Activity-->> ' + this);

			if(this.Invoices.containsKey(accountId)) {
				for(Id billingId : this.Invoices.get(accountId).keySet()) {
					AcctSeed__Billing__c billing = this.Invoices.get(accountId).get(billingId);


					if(!this.IsSetup && billing.AcctSeed__Date__c < this.StatementEndDate.addDays(1)) {
						finalPurchaseActivity += billing.AcctSeed__Total__c;
						Statement_Row row = StatementManager_Helper.CreateRowFromBilling(billing, billing.AcctSeed__Total__c, StatementManager_Helper.StatementActivity.Debit);
						account_rows.add(row);						
					}
				}
			}

			//if(this.CreditMemosApplied.containsKey(accountId)) {
			//	for(Id billingCreditMemoId : this.CreditMemosApplied.get(accountId).keySet()) {
			//		AcctSeed__Billing_Credit_Memo__c billingCreditMemo = this.CreditMemosApplied.get(accountId).get(billingCreditMemoId);

			//		Statement_Row row = StatementManager_Helper.CreateRowFromBillingCreditMemo(billingCreditMemo,
			//																		 		   StatementManager_Helper.StatementActivity.Payment);
			//		account_rows.add(row);
			//	}
			//}

			//if(this.CashReceiptsApplied.containsKey(accountId)) {
			//	for(Id billingCashReceiptId : this.CashReceiptsApplied.get(accountId).keySet()) {
			//		AcctSeed__Billing_Cash_Receipt__c billingCashReceipt = this.CashReceiptsApplied.get(accountId).get(billingCashReceiptId);

			//		Statement_Row row = StatementManager_Helper.CreateRowFromBillingCashReceipt(billingCashReceipt, 																								 
			//																					 StatementManager_Helper.StatementActivity.Payment);
			//		account_rows.add(row);
			//	}
			//}

			if(this.CreditMemos.containsKey(accountId)) {
				for(Id billingId : this.CreditMemos.get(accountId).keySet()) {
					AcctSeed__Billing__c creditMemo = this.CreditMemos.get(accountId).get(billingId);

					currentBalance -= math.abs(creditMemo.AcctSeed__Balance__c);

					if(!this.IsSetup && creditMemo.AcctSeed__Date__c < this.StatementEndDate.addDays(1)) {
						Statement_Row row = StatementManager_Helper.CreateRowFromBilling(
							creditMemo, 
							-Math.abs(creditMemo.AcctSeed__Total__c), 
							StatementManager_Helper.StatementActivity.Credit);
						account_rows.add(row);
					}
				}
			}

			if(this.CashReceipts.containsKey(accountId)) {
				for(Id cashReceiptId : this.CashReceipts.get(accountId).keySet()) {
					AcctSeed__Cash_Receipt__c cashReceipt = this.CashReceipts.get(accountId).get(cashReceiptId);

					currentBalance -= math.abs(cashReceipt.AcctSeed__Balance__c);

					if(!this.IsSetup && cashReceipt.AcctSeed__Receipt_Date__c < this.StatementEndDate.addDays(1)) {
						Statement_Row row = StatementManager_Helper.CreateRowFromCashReceipt(cashReceipt, -cashReceipt.AcctSeed__Amount__c, StatementManager_Helper.StatementActivity.Payment);
						account_rows.add(row);
					}
				}
			}

			if(!this.IsSetup)
				this.LineItems.put(accountId, account_rows);

			this.PurchaseActivity.put(accountId, finalPurchaseActivity);			
		}

		if(!this.IsSetup) {
			//sort Statement Line Items by Date
			for(Id accountId : this.LineItems.keySet()) {
				List<Statement_Row> rows = this.LineItems.get(accountId);
				rows = StatementManager_Helper.SortStatementsQuickSort(rows);
				//Add initial balance
				if(rows.size()>0){

					Decimal startDateOustandingBalance = outBuilder.Balance.get(accountId);
					startDateOustandingBalance = startDateOustandingBalance == null ? 0 : startDateOustandingBalance;

					Statement_Row firstRow = rows[0];

					Statement_Row initialRow = new Statement_Row();
					initialRow.AccountId 	 = accountId;
					initialRow.ActivityDate = firstRow.ActivityDate - 1;
					initialRow.SortingDate  = firstRow.ActivityDate - 1;
					initialRow.Activity 	 = null;
					initialRow.PONumber = 'Opening Balance';
					initialRow.BilledAmount = null;
					initialRow.Balance 	 = startDateOustandingBalance;
					rows.add(0, initialRow);
				}

				this.LineItems.put(accountId, rows);

				//update Balance of every Statement Line Item based on chronological operations
				//StatementManager_Helper.UpdateCumulativeBalance(this.LineItems.get(accountId), StatementManager_Helper.StatementType.Activity);
				
				//Added by Daniel: Specific Update of Cumulative Balance for Activity
				Decimal previousBalance = 0;
				Integer counter = 0;
				Decimal finalBalance = 0;

				for(Statement_Row row : rows) {				
					if(counter++ > 0){				
						row.Balance = previousBalance + row.BilledAmount;
					}
					previousBalance = row.Balance;					
				}

				this.Balance.put(accountId, previousBalance);
			}
		}

		Map<Id, Account> billingContacts_Map = StatementManager_Helper.getBillingContacts(this.AccountIds);

		String sType = String.valueOf(StatementManager_Helper.StatementType.Activity);

		List<Statement__c> statements2Insert = new List<Statement__c>();
		for(Id accountId : billingContacts_Map.keySet()) {
			//generate Statement__c record
			Statement__c st 		= new Statement__c();
			st.Account__c       	= accountId;
			st.Balance__c       	= this.Balance.get(accountId);
			st.Type__c         		= sType;
			st.Period_Start_Date__c = this.StatementStartDate;
			st.Period_End_Date__c   = this.StatementEndDate;
			st.Account_Billing_Contact__c = billingContacts_Map.get(accountId).AcctSeed__Billing_Contact__c;
			st.Send__c = this.SendEmail;
			statements2Insert.add(st);
		}

		System.debug('statements2Insert-->> ' + statements2Insert);

		//insert if needed
		if(insertDirectly && statements2Insert.size() > 0) {			
			// Delete old ones that match with new ones
			delete StatementManager_Helper.getExistingStatements(statements2Insert, sType);
			// Insert new ones
			insert statements2Insert;

			List<Id> statementIds = new List<Id>();
			//generate the attached PDF file
			for(Statement__c statement : statements2Insert) {
				statementIds.add(statement.Id);
			}

			if(!this.GeneratedFromBatch)
				StatementManager_Helper.attachPDFToStatements_Future(statementIds);
		}

		return statements2Insert;
	}

}