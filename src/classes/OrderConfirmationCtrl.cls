/**
 * Controller for: OrderConfirmation.page
 * @author Fernando Gomez, SkyPlanner LLC
 * @date May 8th, 2017
 * @version 
 *		1.0.0 Initial version.
 */
public class OrderConfirmationCtrl {
	public OrderManager.OrderInformation orderInfo { get; private set; }

	// properties
	public transient Boolean isSuccess { get; private set; }
	public transient String message { get; private set; }

	/**
	 * Constructor.
	 * @author Fernando Gomez, SkyPlanner LLC
	 * @date May 8th, 2017
	 * @param ctrl
	 */
	public OrderConfirmationCtrl() {
		
	}

	/**
	 * Gets the order. Exception is thrown if not found.
	 * @author Fernando Gomez, SkyPlanner LLC
	 * @date May 8th, 2017
	 * @param ctrl
	 */
	private Boolean getOrder(String orderId) {
		Id validId;
		OrderManager m;

		try {			
			m = new OrderManager();
			// we parse the id,
			// and obtain the order
			validId = (Id)orderId;
			orderInfo = m.getById(validId);
			// at this point, everyyhing was smooth
			message = null;
			return true;
		} catch (StringException ex) {
			// string exception will be thrown 
			// if the Id could not be casted properly,
			// meaning that the user passed an invalid id
			message = Label.OrderConfirmationErrorInvalidOrder;
			return false;
		} catch (QueryException ex) {
			// query exception is thrown when the order 
			// is not found.. so we give a complrehensive message
			message = Label.OrderConfirmationErrorOrderNotFound;
			return false;
		} catch (Exception ex) {
			// We are not expecting any other exception,
			// we show the message for support
			message = ex.getMessage();
			return false;
		} 
	}

	/**
	 * Activates the order.
	 * If the order has already been activated, 
	 * we should show a fridnly message and not let
	 * the validation throw an error.
	 * @author Fernando Gomez, SkyPlanner LLC
	 * @date May 8th, 2017
	 * @param ctrl
	 */
	public void activateOrder() {
		OrderManager m;
		List<ApexPages.Message> messages;
		// we build the manager
		m = new OrderManager();
		// first, we need the order
		isSuccess = getOrder(
			ApexPages.currentPage().getParameters().get('order'));

		if (isSuccess)
			try {
				// activation requires only the id
				m.activate(orderInfo);
				// we mark the operation as successfull
				isSuccess = true;
				message = null;
			} catch (DmlException ex) {
				isSuccess = false;
				// when dml exception is thrown, the message 
				// is not very friendly.. we process it and
				// provide a more comprehensive message
				ApexPages.addMessages(ex);
				messages = ApexPages.getMessages();
				message = messages != null && !messages.isEmpty() ?
					messages[0].getSummary() : ex.getMessage();
			} catch (Exception ex) {
				// We are not expecting any other exception,
				// we show the message for support
				isSuccess = false;
				message = ex.getMessage();
			} 
	}	
}