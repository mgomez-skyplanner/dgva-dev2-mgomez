/**
 * Test class for OrderConfirmationCtrl.
 * @author Fernando Gomez, SkyPlanner LLC
 * @date May 9th, 2017
 * @version 1.0.0 Initial version.
 */
@isTest
private class OrderConfirmationCtrlTest {
	/**
	 * Setup test data
	 * @author 
	 * @date   03/21/2017
	 * 
	 */
	@testSetup
	public static void setup() {
		Route__c route1;
		Account acc1;
		Product2 prod1;
		PricebookEntry pbe1;
		Order order1;
		OrderItem orderItem1;
		
		route1 = new Route__c(
			Name = 'route1'
		);
		insert route1;

		acc1 = new Account(
			Name = 'Account1',
			BillingCountry = 'C1',
			BillingCity = 'C2',
			Total_Balance__c = 50,
			Route__c = route1.Id
		);
		insert acc1;

		prod1 = new Product2(
			Name = 'Product1',
			IsActive = true,
			Price__c = 10,
			Units_Pk__c = 'DOZ',
			SKU__c = '123'
		);
		insert prod1;

		pbe1 = [
			SELECT Id 
			FROM PricebookEntry 
			WHERE Pricebook2Id = :[
				SELECT Price_Book__c 
				FROM Account 
				WHERE Id = :acc1.Id][0].Price_Book__c 
			AND Product2Id = :prod1.Id
		];

		order1 = new Order (
			AccountId = acc1.Id,
			Status = 'Draft', 
			EffectiveDate = System.today(),
			EndDate = System.today()
		);
		insert order1;

		orderItem1 = new OrderItem(
			EndDate = System.today(),
			OrderId = order1.Id,
			PricebookentryId = pbe1.Id,
			Quantity = 2,
			Total_Pieces__c = 2,
			SKU__c = '123',
			UnitPrice = 10
		);
		insert orderItem1;
	}

	/**
	 * Test:
	 * public void activateOrder()
	 * @author Fernando Gomez, SkyPlanner LLC
	 * @date May 9th, 2017
	 */
	@isTest
	static void activateOrder() {
		OrderConfirmationCtrl ctrl;
		PageReference current;
		Order ord;

		// setup pages
		current = Page.OrderConfirmation;
		Test.setCurrentPage(current);

		// build
		ctrl = new OrderConfirmationCtrl();

		// should fail for null ids
		ctrl.activateOrder();
		System.assert(!ctrl.isSuccess);

		// set invalid id as parameter
		current.getParameters().put('order', 'invalid id');
		ctrl.activateOrder();
		System.assert(!ctrl.isSuccess);

		// and we create a valid order
		ord = [
			SELECT Id
			FROM Order
			WHERE Account.Name = 'Account1'
		];

		current.getParameters().put('order', ord.Id);
		ctrl.activateOrder();
		System.assert(!ctrl.isSuccess);

		ord.Status = 'Prepared';
		update ord;
		ctrl.activateOrder();
		System.assert(ctrl.isSuccess);
	}
}