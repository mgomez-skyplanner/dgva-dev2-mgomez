/**
* @author    	 - Marcel Gomez
* @date  	     - 06/09/2017
* @description 	 - This class provides custom Exception objects
*/
 
public with sharing class ExceptionHelper {
	
	public class InvalidOrderStatusException extends Exception {}

    /**
	* <p> 
	* Version log:
	* <ul>
	* <li>1.00 06/29/2017 Marcel Gomez: Initial version</li>
	* </ul>
	* <p>
	* @author	     - Marcel Gomez
	* @date  	     - 06/29/2017
	* @description 	 - This class represents the Exception that is thrown when the Accounting Seed Setting "Default Billing Format" is not set
	*/
    public class BillingPDFFormatMissing_Exception extends Exception { 

		/*
		* @author    	 - Marcel Gomez
		* @date  	     - 06/09/2017
		* @description 	 - Constructor
		* @param       	 - overload: this parameter does nothing but it is necessary because Apex does not allow overloading the default constructor of class Exception
		*/
    	public BillingPDFFormatMissing_Exception(Boolean overload) {
    		setMessage('You must enter a value or define a default Billing Format on the Accounting Settings tab: [AcctSeed__Billing_Format__c].');
    	}
    }

    /**
	* <p> 
	* Version log:
	* <ul>
	* <li>1.00 07/03/2017 Marcel Gomez: Initial version</li>
	* </ul>
	* <p>
	* @author	     - Marcel Gomez
	* @date  	     - 07/03/2017
	* @description 	 - This class represents the Exception that is thrown when no Accounting Period is found for a Billing
	*/
    public class BillingAccountingPeriodMissing_Exception extends Exception { 

		/*
		* @author    	 - Marcel Gomez
		* @date  	     - 07/03/2017
		* @description 	 - Constructor
		* @param       	 - overload: this parameter does nothing but it is necessary because Apex does not allow overloading the default constructor of class Exception
		*/
    	public BillingAccountingPeriodMissing_Exception(Boolean overload) {
    		setMessage('No Accounting Period found.');
    	}
    }

    /**
	* <p> 
	* Version log:
	* <ul>
	* <li>1.00 07/07/2017 Marcel Gomez: Initial version</li>
	* </ul>
	* <p>
	* @author	     - Marcel Gomez
	* @date  	     - 07/07/2017
	* @description 	 - This class represents the Exception that is thrown when no Billing is found
	*/
    public class BillingNotFound_Exception extends Exception {

		/*
		* @author    	 - Marcel Gomez
		* @date  	     - 07/03/2017
		* @description 	 - Constructor
		* @param       	 - id: represents the Billing.Id
		*/
    	public BillingNotFound_Exception(Id id) {
    		setMessage('No Billing found with Id = ' + id);
    	}
    }
}