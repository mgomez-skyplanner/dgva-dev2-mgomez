/**
* <p>
* Version log:
* <ul>
* <li>1.00 09/28/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 09/28/2017
* @description 	 - Represents an extension controller to provide actions related to the Statement object
* @version
* 	1.01		 10/26/2017 William Garcia: Statement develoment, continuation...
*/
 
global with sharing class StatementController {

	private static String LastQuery { get; set; }

	public StatementController() {
		
	}

	public StatementController(ApexPages.StandardController stdController) {
		
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/05/2017
	* @description 	 - Action to generate Statements based on a filters set
	*/
	@AuraEnabled
	@RemoteAction
	webservice static List<StatementManagement_Rows> generateStatements_Bulk(
		String accountId, String paymentTerm, String customerLevel, String customerGroup, 
		String route, String statementType, String startDate, String endDate, Boolean sendEmail) {
		try {

			System.debug('startDate-->> ' + startDate);
			System.debug('endDate-->> ' + endDate);

			StatementManager_Helper.StatementManagement_Filters searchTerms = new StatementManager_Helper.StatementManagement_Filters();

			searchTerms.AccountId 		= accountId;
			searchTerms.PaymentTerm 	= paymentTerm;
			searchTerms.CustomerLevel 	= customerLevel;
			searchTerms.CustomerGroup 	= customerGroup;
			searchTerms.Route 			= route;

			searchTerms.StatementType 	= statementType;
			searchTerms.StartDate 		= !String.isBlank(startDate) ? StatementManager_Helper.parseDateByTicks(startDate) : NULL;
			searchTerms.EndDate 		= !String.isBlank(endDate) 	 ? StatementManager_Helper.parseDateByTicks(endDate) : NULL;

			System.debug('searchTerms-->> ' + searchTerms);

			//if accountId has value => generate Statement synchronously
			if(!String.isBlank(accountId)) {
				System.debug('generate Statement synchronously-----');
				if(statementType == 'Outstanding') {
					StatementBuilder_Outstanding builder = new StatementBuilder_Outstanding(
						new List<Id> { accountId }, searchTerms.EndDate, sendEmail);
					builder.generateOutStandingStatement(true);
				} else {
					StatementBuilder_Activity builder = new StatementBuilder_Activity(
						new List<Id> { accountId }, searchTerms.StartDate, searchTerms.EndDate, sendEmail);
					builder.generateActivityStatement(true);
				}
			}			
			//if accountId has NO value => generate Statement asynchronously using batch
			else {
				System.debug('generate Statement asynchronously-----');
				List<Account> accounts = StatementManager_Helper.LoadAccounts(searchTerms);

				List<Id> accountIds = new List<Id>();
				for(Account account : accounts)
					accountIds.add(account.Id);

				StatementsGeneration_Batch batch = new StatementsGeneration_Batch(
					accountIds, statementType == 'Activity' ? 
						StatementManager_Helper.StatementType.Activity : 
						StatementManager_Helper.StatementType.Outstanding, 
					sendEmail, searchTerms.startDate, searchTerms.endDate, true);

        		Database.executeBatch(batch, 1);
			}

			return searchStatements(searchTerms);
		} catch(Exception e) {
			throw e;
		}

		return new List<StatementManagement_Rows>();
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/27/2017
	* @description 	 - Action to load all initial search values for page StatementManagementPage
	* @return      	 - A custom class instance with all necessary values
	*/
	@AuraEnabled
	@RemoteAction
	webservice static StatementManagement_SearchInitialValues LoadInitialValues(String accountId) {
		StatementManagement_SearchInitialValues result = new StatementManagement_SearchInitialValues();
		Date today = Date.today();

		result.PaymentTerms 	= PaymentBatchController.LoadPaymentTerms();
		result.CustomerLevels 	= PaymentBatchController.LoadCustomerLevels();
		result.CustomerGroups 	= PaymentBatchController.LoadCustomerGroups();
		result.Routes 			= LoadRoutes();

		List<SelectOption> types = PaymentBatchController.LoadPicklistValues(Statement__c.Type__c.getDescribe());
		types.remove(0);
		result.Types 			= types;

		result.StartDate 		= today.addMonths(-1).addDays(1);
		result.EndDate 			= today.addDays(1);

		if(!String.isBlank(accountId))
			result.Account = [SELECT Id, Name FROM Account WHERE Id =: accountId];

		return result;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/05/2017
	* @description 	 - Action to generate Statements of a given StatementType in a given period of time for a specific Account
	*/
	@AuraEnabled
	@RemoteAction
	webservice static String sendStatementsByEmail(String accountId, String paymentTerm, 
		String customerLevel, String customerGroup, String routeId, String statementType, 
		String startDate, String endDate) {
			try {
				StatementManager_Helper.StatementManagement_Filters searchTerms = 
					new StatementManager_Helper.StatementManagement_Filters();

				searchTerms.AccountId = accountId;
				searchTerms.PaymentTerm = paymentTerm;
				searchTerms.CustomerLevel = customerLevel;
				searchTerms.CustomerGroup = customerGroup;
				searchTerms.RouteId = routeId;
				searchTerms.StatementType = statementType;
				searchTerms.StartDate = !String.isBlank(startDate) ? 
					StatementManager_Helper.parseDateByTicks(startDate) : NULL;
				searchTerms.EndDate = !String.isBlank(endDate) ? 
					StatementManager_Helper.parseDateByTicks(endDate) : NULL;

				System.System.debug('sendStatementsByEmail-searchTerms-->> ' + searchTerms);

				List<Account> accounts = StatementManager_Helper.LoadAccounts(searchTerms);
				List<Statement__c> statementsToSend = new List<Statement__c>();

				for(Account account : accounts)
					//validate the Account has a Billing Contact
					if(account.AcctSeed__Billing_Contact__c != NULL) {
						for(Statement__c statement : account.Statements__r) {
							statement.Resend__c = true;

							statementsToSend.add(statement);
						}
					}

				update statementsToSend;

				System.System.debug('sendStatementsByEmail-statementsToSend-->> ' + statementsToSend);

				return '1';
			}
			catch(Exception e) {
				throw e;
			}

			return '0';
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/29/2017
	* @description 	 - Action to search all Statement__c records based on a filters set
	* @return      	 - A List of objects wrapping Statement__c records data
	*/
	@AuraEnabled
	@RemoteAction
	webservice static List<StatementManagement_Rows> searchStatements(String accountId, String paymentTerm, String customerLevel, String customerGroup, 
                                              						  String route, String statementType, String startDate, String endDate) {

		StatementManager_Helper.StatementManagement_Filters searchTerms = new StatementManager_Helper.StatementManagement_Filters();

		searchTerms.AccountId 		= accountId;
		searchTerms.PaymentTerm 	= paymentTerm;
		searchTerms.CustomerLevel 	= customerLevel;
		searchTerms.CustomerGroup 	= customerGroup;
		searchTerms.Route 			= route;

		searchTerms.StatementType 	= statementType;
		searchTerms.StartDate 		= !String.isBlank(startDate) ? StatementManager_Helper.parseDateByTicks(startDate) : NULL;
		searchTerms.EndDate 		= !String.isBlank(endDate) 	 ? StatementManager_Helper.parseDateByTicks(endDate) : NULL;

		System.debug('searchStatements-searchTerms-->> ' + searchTerms);

		return searchStatements(searchTerms);
	}

	private static List<StatementManagement_Rows> searchStatements(StatementManager_Helper.StatementManagement_Filters searchTerms) {
		List<Account> accounts = StatementManager_Helper.LoadAccounts(searchTerms);
		
		system.debug('------------- Accounts found: ' + accounts.size());
		system.debug(accounts);

		Map<String, StatementManagement_Rows> route_Map = new Map<String, StatementManagement_Rows>();

		for(Account account : accounts) {
			String routeId 		 = account.Route__c;
			String routeName 	 = account.Route__r.Name;
			Integer noStatements = account.Statements__r.size();
			Integer noEmailReady = account.AcctSeed__Billing_Contact__c != NULL ? noStatements : 0;

			system.debug('------------- current account: ');
			system.debug(account);
			system.debug('------------- routeName = ' + routeName);
			system.debug('------------- noStatements = ' + noStatements);
			system.debug('------------- noEmailReady = ' + noEmailReady);

			system.debug('------------- current StatementManagement_Rows BEFORE: ');
			system.debug(route_Map.get(account.Route__c));

			if(noStatements > 0) {
				if(!route_Map.containsKey(account.Route__c)){
					StatementManagement_Rows row = new StatementManagement_Rows();
					
					row.RouteId			 = routeId;
					row.Route 			 = routeName;
					row.StatementsAmount = noStatements;
					row.EmailReady 		 = noEmailReady;

					route_Map.put(account.Route__c, row);
				}
				else {
					StatementManagement_Rows updatedRow = route_Map.get(account.Route__c);

					updatedRow.StatementsAmount += noStatements;
					updatedRow.EmailReady 		+= noEmailReady;

					route_Map.put(account.Route__c, updatedRow);
				}
			}

			system.debug('------------- current StatementManagement_Rows AFTER: ');
			system.debug(route_Map.get(account.Route__c));
		}

		//remove entries with no statements
		List<StatementManagement_Rows> result = new List<StatementManagement_Rows>();
		for(String route : route_Map.keySet()) {
			result.add(route_Map.get(route));
		}

		system.debug('------------- results: ');
		system.debug(result);

		return result;
	}

	/*
	* @author    	 - Marcel Gomez
	* @date  	     - 09/28/2017
	* @description 	 - Loads all Route__c records
	* @return      	 - A List<SelectOption> with all Route__c records
	*/
	public static List<SelectOption> LoadRoutes() {
		List<SelectOption> result = new List<SelectOption>{new SelectOption('', '')};

		List<Route__c> routes = [SELECT Id, Name FROM Route__c ORDER BY Name ASC];

		for(Route__c route : routes) {
	    	result.add(new SelectOption(route.Name, route.Id));
		}

		return result;
	}

	private class StatementControllerException extends Exception { }

	global class StatementManagement_SearchInitialValues {
		//Account Filters
		public List<SelectOption> PaymentTerms { get; set; }
		public List<SelectOption> CustomerLevels { get; set; }
		public List<SelectOption> CustomerGroups { get; set; }
		public List<SelectOption> Routes { get; set; }

		//Statement Filters
		public List<SelectOption> Types { get; set; }
		public Date StartDate { get; set; }
		public Date EndDate { get; set; }

		public Account Account { get; set; }
	}

	global class StatementManagement_Rows {
		global String RouteId { get; set; }
		global String Route { get; set; }
		global Integer StatementsAmount { get; set; }
		global Integer EmailReady { get; set; }

		//public String Query { get; set; }
	}
}