/**
 * Recurring orders process batch test
 * @author  William Garcia
 * @date    05/10/2017
 */
@isTest (seeAllData = false)
global class RecurringOrderGenerationBatchTest {

    /**
     * Test recurring order process batch
     * @author William Garcia
     * @date   05/10/2017
     */
    static testMethod void test() {
        Test.startTest();
        
        RecurringOrderGenerationBatch batch = new RecurringOrderGenerationBatch();
        Id batchprocessid = Database.executeBatch(batch);

        Test.stopTest(); 

        system.assert(String.isNotBlank(batchprocessid));
    }
    
}