/**
* <p>
* Version log:
* <ul>
* <li>1.00 10/18/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 10/18/2017
* @description 	 - Generates first Statements for Accounts
*/
 
public with sharing class StatementsGenerationSetup_Batch implements Database.Batchable<sObject>, Database.Stateful {

	List<Statement__c> StatementsGenerated { get; set; }

	public Date SetupDate { get; set; }
	public Boolean GeneratePDFs { get; set; }
	public StatementManager_Helper.StatementType StatementType { get; set; }

	public StatementsGenerationSetup_Batch() {
		this.StatementsGenerated = new List<Statement__c>();

		this.SetupDate = Date.today();
		this.GeneratePDFs = false;
		this.StatementType = StatementManager_Helper.StatementType.Outstanding;
	}

	public StatementsGenerationSetup_Batch(Date pSetupDate) {
		this.StatementsGenerated = new List<Statement__c>();
		
		this.SetupDate = pSetupDate;
		this.GeneratePDFs = false;
		this.StatementType = StatementManager_Helper.StatementType.Outstanding;
	}

	public StatementsGenerationSetup_Batch(Date pSetupDate, Boolean pGeneratePDFs) {
		this.StatementsGenerated = new List<Statement__c>();
		
		this.SetupDate = pSetupDate;
		this.GeneratePDFs = pGeneratePDFs;
		this.StatementType = StatementManager_Helper.StatementType.Outstanding;
	}

	public StatementsGenerationSetup_Batch(Date pSetupDate, Boolean pGeneratePDFs, StatementManager_Helper.StatementType pStatementType) {
		this.StatementsGenerated = new List<Statement__c>();
		
		this.SetupDate = pSetupDate;
		this.GeneratePDFs = pGeneratePDFs;
		this.StatementType = pStatementType;
	}



	
	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator([SELECT Id FROM Account]);
	}

   	public void execute(Database.BatchableContext bc, List<Account> scope) {
   		List<Id> accountIds = new List<Id>();

   		for(Account account : scope) {
			accountIds.add(account.Id);
   		}

   		List<Statement__c> statements_List = new List<Statement__c>();

   		if(this.StatementType == StatementManager_Helper.StatementType.Activity) {
	   		//generate first Activity Statements
			StatementBuilder_Activity builderActivity = new StatementBuilder_Activity(accountIds, NULL, this.SetupDate, true);
			builderActivity.IsSetup = true;
			builderActivity.GeneratedFromBatch = true;
			
			statements_List = builderActivity.generateActivityStatement(true);
		}
		else if(this.StatementType == StatementManager_Helper.StatementType.Outstanding) {
	   		//generate first Outstanding Statements
			StatementBuilder_Outstanding builderOutstanding = new StatementBuilder_Outstanding(accountIds, this.SetupDate, true);
			builderOutstanding.IsSetup = true;
			builderOutstanding.GeneratedFromBatch = true;

			statements_List = builderOutstanding.generateOutStandingStatement(true);
		}

		this.StatementsGenerated.addAll(statements_List);
	}
	
	public void finish(Database.BatchableContext bc) {
		if(this.GeneratePDFs) {
			List<Id> statementIds = new List<Id>();
			for(Statement__c st : this.StatementsGenerated) {
				statementIds.add(st.Id);
			}

			StatementManager_Helper.attachPDFToStatements(statementIds);
		}
	}
}