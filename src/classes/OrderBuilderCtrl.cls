/**
 * Class to handle order builder methods
 * @author William Garcia
 * @date   03/21/2017
 * @version	1.0	William Garica, 03/21/2017
 *			1.1 Fernando Gomez, 05/10/2017
 */
public class OrderBuilderCtrl {
	/**
	 * Main constructor
	 *	@author	Fernando Gomez
	 *	@date May 11th, 2017
	 */
	public OrderBuilderCtrl() {

	}

	/**
	 * Standard controller constructor.
	 *	@author	Fernando Gomez
	 *	@date May 11th, 2017
	 */
	public OrderBuilderCtrl(ApexPages.StandardController ctrl) {
		
	}
	
	/**
	 *	@author			William Garcia
	 *	@date			03/16/2017
	 *	@description	Remote action to retrieve information from a lookup field
	 */
	@RemoteAction
	public static List<sObject> lookupObject(
			String objName, String field, String criteria, 
			List<OrderBuilderHelper.WhereClauseStatement> whereConditions) {
		return OrderBuilderHelper.lookupObject(objName, 
				field, criteria, whereConditions);
	}

	/**
	 * To select account record
	 * @author	William Garcia
	 * @date   03/16/2017
	 * @param accId Selected account's id
	 * @return OrderBuilderHelper.AccountWrapper Account Wrapper
	 */
	@RemoteAction
	public static OrderBuilderHelper.AccountWrapper selectAccount(Id accId) {
		return OrderBuilderHelper.selectAccount(accId);
	}

	/**
	 * Search products from some SKU and Name
	 * @author William Garcia
	 * @date   03/17/2017
	 * @param criteria Input criteria
	 * @param pbId Account price book id
	 * @return List<OrderBuilderHelper.ProductWrapper> Product wrapper list
	 */
	@RemoteAction
	public static List<OrderBuilderHelper.ProductWrapper> searchProducts(
			Id accId, Id pbId, String criteria) {
		return OrderBuilderHelper.searchProducts(accId, pbId, criteria);
	}

	/**
	 * Get products from some SKU and Name
	 * @author William Garcia
	 * @date   03/17/2017
	 * @param accId Account'sid
	 * @return List<OrderBuilderHelper.ProductWrapper> Product wrapper list
	 */
	@RemoteAction
	public static List<OrderBuilderHelper.ProductWrapper> getProducts(
			Id pbId, Id accId) {
		return OrderBuilderHelper.getProducts(pbId, accId);
	}

	/**
	 * Get products from some SKU and Name
	 * @author William Garcia
	 * @date   03/17/2017
	 * @param accId Account'sid
	 * @return List<OrderBuilderHelper.ProductWrapper> Product wrapper list
	 */
	@RemoteAction
	public static Order saveOrder(OrderBuilderHelper.OrderWrapper order) {
		return OrderBuilderHelper.saveOrder(order);
	}

	/**
	 * Updates the preferences of the account regarding
	 * the specified product...
	 * @author Fernando Gomez
	 * @date   May 11, 2017
	 * @param accountId
	 * @param productId
	 * @param isFavorite
	 * @param isShownByDefault
	 */
	@RemoteAction
	public static void updatePreferences(Id accountId, Id productId, 
			Boolean isFavorite, Boolean isShownByDefault) {
		ProductPreferenceManager m = 
			new ProductPreferenceManager(accountId);
		m.setFavorite(productId, isFavorite);
		m.setShowByDefault(productId, isShownByDefault);
	}

	/**
	 * Returns all categories that have add-ons that applies to both 
	 * the specified product familiy and subcategory. Each cateogory
	 * returned mapps the amount of addons that are currently availabel.
	 * @author Fernando Gomez
	 * @date July 11, 2017
	 * @return List<AggregateResult>
	 */
	@RemoteAction
	public static List<AggregateResult> getCategories(
			String family, String subcategory) {
		AddonManager m = new AddonManager();
		return m.getCategories(family, subcategory);
	}

	/**
	 * Return all available addons mapped
	 * by category.
	 * @author Fernando Gomez
	 * @date July 10, 2017
	 * @return Map<String, List<AddOn__c>>
	 * @param pricebookId
	 * @param family
	 * @param subcategory
	 * @param category
	 */
	@RemoteAction
	public static List<AddonManager.AddOn> getAddons(Id pricebookId,
			String family, String subcategory, String category) {
		AddonManager m = new AddonManager();
		return m.getAddons(pricebookId, family, subcategory, category);
	}
}