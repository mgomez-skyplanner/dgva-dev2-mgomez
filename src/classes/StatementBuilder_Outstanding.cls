/**
* <p> 
* Version log:
* <ul>
* <li>1.00 10/11/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 10/11/2017
* @description 	 - This class provides methods to generate an Outstanding Statement
* @version 
* 	1.01		 10/26/2017 William Garcia: Statement develoment, continuation...
*/

public with sharing class StatementBuilder_Outstanding {
	
	public List<Id> AccountIds { get; set; }

	public Date StatementDate { get; set; }

	public Map<Id, Decimal> Balance { get; set; }
	public Map<Id, List<Statement_Row>> LineItems { get; set; }

	//key: Account Id
	//Value: Map with Invoices (Key: Invoice Id, Value: Invoice)
	private Map<Id, Map<Id, AcctSeed__Billing__c>> 	  UnappliedInvoices { get; set; }

	//key: Account Id
	//Value: Map with Credit Memos (Key: Credit Memo Id, Value: Credit Memo)
	private Map<Id, Map<Id, AcctSeed__Billing__c>> 	  UnappliedCreditMemos { get; set; }

	//key: Account Id
	//Value: Map with Cash Receipts (Key: Cash Receipt Id, Value: Cash Receipt)
	private Map<Id, Map<Id, AcctSeed__Cash_Receipt__c>> CashReceipts { get; set; }

	//Indicates the amount of money paid for a Billing after the Statement Date
	private Map<Id, Decimal> billing_amountPaidLater_Map { get; set; }

	//Indicates the amount of money applied of a Credit Memo after the Statement Date
	private Map<Id, Decimal> creditMemo_amountPaidLater_Map { get; set; }

	//Indicates the amount of money applied of a Credit Memo after the Statement Date
	private Map<Id, Decimal> cashReceipt_amountPaidLater_Map { get; set; }

	public Boolean GeneratedFromBatch { get; set; }
	public Boolean IsSetup { get; set; }
	
	public Boolean SendEmail { get; set; }

    /*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/11/2017
	* @description 	 - Constructor
	* @param 	 	 - AccountIds: Account Ids to generate the Statement for
	* @param 	 	 - pDate: the Statement date
	*/
	public StatementBuilder_Outstanding(List<Id> accountIds, Date pDate, Boolean send) {
		this.AccountIds 	= accountIds;
		this.StatementDate 	= pDate;
		this.Balance 		= new Map<Id, Decimal>();
		this.LineItems 		= new Map<Id, List<Statement_Row>>();

		this.UnappliedInvoices 		= new Map<Id, Map<Id, AcctSeed__Billing__c>>();
		this.UnappliedCreditMemos 	= new Map<Id, Map<Id, AcctSeed__Billing__c>>();
		this.CashReceipts 			= new Map<Id, Map<Id, AcctSeed__Cash_Receipt__c>>();

		this.billing_amountPaidLater_Map 	 = new Map<Id, Decimal>();
		this.creditMemo_amountPaidLater_Map  = new Map<Id, Decimal>();
		this.cashReceipt_amountPaidLater_Map = new Map<Id, Decimal>();
		
		this.GeneratedFromBatch = false;
		this.IsSetup = false;
		this.SendEmail = send;
	}

    /*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/11/2017
	* @description 	 - Loads all Billings (Invoices and Credit Memos) of Accounts, with Balance != 0 and with Billing Date <= Statement date
	*/
	private void LoadUnnaplied_Invoices_CreditMemos() {
		List<AcctSeed__Billing__c> allUnappliedBillings = [SELECT 
																Id,
																Name,
																CreatedDate,
																AcctSeed__Customer__c,
																AcctSeed__Date__c,
																AcctSeed__Proprietary_Billing_Number__c,
																AcctSeed__PO_Number__c,
																AcctSeed__Due_Date2__c,
																AcctSeed__Total__c,
																AcctSeed__Balance__c,
																AcctSeed__Type__c,
																(
																	SELECT AcctSeed__Amount__c,
																	       AcctSeed__Billing_Credit_Memo__c,
																	       AcctSeed__Billing_Invoice__c
																	FROM AcctSeed__Billing_Credit_Memos1__r
																	WHERE CreatedDate <: this.StatementDate.addDays(1)
																),
																(
																	SELECT AcctSeed__Amount__c,
																	       AcctSeed__Billing_Credit_Memo__c,
																	       AcctSeed__Billing_Invoice__c
																	FROM AcctSeed__Billing_Credit_Memos__r
																	WHERE CreatedDate <: this.StatementDate.addDays(1)
																),
																(
																	SELECT 
																		Id,																	
																		AcctSeed__Applied_Amount__c,
																		AcctSeed__Adjustment_Amount__c,
																		CreatedDate,
																		AcctSeed__Billing__c,
																		AcctSeed__Billing__r.AcctSeed__Due_Date2__c,
																		AcctSeed__Billing__r.AcctSeed__PO_Number__c
																	FROM
																		AcctSeed__Billing_Cash_Receipts__r
																	WHERE
																		CreatedDate <: this.StatementDate.addDays(1)
																)
															FROM
																AcctSeed__Billing__c
															WHERE 
																AcctSeed__Customer__c IN: this.AccountIds AND 
																AcctSeed__Date__c <: this.StatementDate.addDays(1) AND
																AcctSeed__Status__c = 'Posted' 
																//AcctSeed__Balance__c != 0
																
															ORDER BY
																AcctSeed__Date__c];

		system.debug('-------------------------- Billings of Account ' + this.AccountIds);
		system.debug(allUnappliedBillings);



		for(AcctSeed__Billing__c billing : allUnappliedBillings) {
			
			Decimal balance = billing.AcctSeed__Total__c;			
			
			//Remove Cash Receipts
			for(AcctSeed__Billing_Cash_Receipt__c bcr: billing.AcctSeed__Billing_Cash_Receipts__r) {																		
				balance -= bcr.AcctSeed__Applied_Amount__c
						 + (bcr.AcctSeed__Adjustment_Amount__c == null ? 0 : bcr.AcctSeed__Adjustment_Amount__c);
			}

			//Remove Applied Credit Memos - Makes Sense only for Invoices
			for(AcctSeed__Billing_Credit_Memo__c appliedCreditMemo: billing.AcctSeed__Billing_Credit_Memos1__r) {
				balance -= Math.abs(appliedCreditMemo.AcctSeed__Amount__c);
			}

			//Remove Applied Invoices - Makes Sense only for Credit Memos
			for(AcctSeed__Billing_Credit_Memo__c appliedCreditMemo: billing.AcctSeed__Billing_Credit_Memos__r) {
				balance += Math.abs(appliedCreditMemo.AcctSeed__Amount__c);
			}

			//if(billing.AcctSeed__Type__c != 'Invoice' && balance != 0)
			//	system.assert(false, 
			//		' TYPE: ' + billing.AcctSeed__Type__c + 
			//		' **** Total: ' + billing.AcctSeed__Total__c + 
			//		' **** Balance: ' + balance + 
			//		' **** ' + billing.AcctSeed__Billing_Cash_Receipts__r + 
			//		' **** ' + billing.AcctSeed__Billing_Credit_Memos1__r + 
			//		' **** ' + billing.AcctSeed__Billing_Credit_Memos__r);


			//Use the field AcctSeed__Credit_Memo_Applied_Amount__c as a data holder not to be saved
			billing.AcctSeed__Credit_Memo_Applied_Amount__c = balance;

			if(billing.AcctSeed__Type__c == 'Invoice' && balance > 0) { 

				//create the key/value pair for an Account if does not exists
				if(!this.UnappliedInvoices.containsKey(billing.AcctSeed__Customer__c))
					this.UnappliedInvoices.put(billing.AcctSeed__Customer__c, new Map<Id, AcctSeed__Billing__c>());

				this.UnappliedInvoices.get(billing.AcctSeed__Customer__c).put(billing.Id, billing);
			}
			else if(billing.AcctSeed__Type__c != 'Invoice' && balance < 0) {
				//create the key/value pair for an Account if does not exists
				if(!this.UnappliedCreditMemos.containsKey(billing.AcctSeed__Customer__c))
					this.UnappliedCreditMemos.put(billing.AcctSeed__Customer__c, new Map<Id, AcctSeed__Billing__c>());

				this.UnappliedCreditMemos.get(billing.AcctSeed__Customer__c).put(billing.Id, billing);
			}

		}


		//REMOVED BY DANIEL
		//for(AcctSeed__Billing__c billing : allUnappliedBillings) {

		//	//system.assert(false, 'Billing Id: ' + billing.Id + ' **** TYPE: ' + billing.AcctSeed__Type__c + ' **** ' + billing.AcctSeed__Billing_Credit_Memos__r + ' **** ' + billing.AcctSeed__Billing_Credit_Memos1__r);

		//	if(billing.AcctSeed__Type__c == 'Invoice') {  //assuming Balance > 0

		//		//create the key/value pair for an Account if does not exists
		//		if(!this.UnappliedInvoices.containsKey(billing.AcctSeed__Customer__c))
		//			this.UnappliedInvoices.put(billing.AcctSeed__Customer__c, new Map<Id, AcctSeed__Billing__c>());

		//		this.UnappliedInvoices.get(billing.AcctSeed__Customer__c).put(billing.Id, billing);
		//	}
		//	else {
		//		//create the key/value pair for an Account if does not exists
		//		if(!this.UnappliedCreditMemos.containsKey(billing.AcctSeed__Customer__c))
		//			this.UnappliedCreditMemos.put(billing.AcctSeed__Customer__c, new Map<Id, AcctSeed__Billing__c>());

		//		this.UnappliedCreditMemos.get(billing.AcctSeed__Customer__c).put(billing.Id, billing);
		//	}
		//}

		system.debug('-------------------------- UnappliedInvoices of Account ' + this.AccountIds);
		system.debug(this.UnappliedInvoices);
	}

    /*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/11/2017
	* @description 	 - Loads all Cash Receipts, partially or not fully applied, of Accounts with Balance > 0 and Receipt Date <= Statement date
	*/
	private void LoadCashReceipts() {
		List<AcctSeed__Cash_Receipt__c> cashReceipts = [SELECT 
															Id,
															Name,
															CreatedDate,
															AcctSeed__Account__c,
															AcctSeed__Balance__c,
															AcctSeed__Receipt_Date__c,
															AcctSeed__Amount__c,
															AcctSeed__Purpose__c,
															AcctSeed__Payment_Reference__c,
															(
																SELECT 
																	Id,																	
																	AcctSeed__Applied_Amount__c,
																	CreatedDate,

																	AcctSeed__Billing__c,
																	AcctSeed__Billing__r.AcctSeed__Due_Date2__c,
																	AcctSeed__Billing__r.AcctSeed__PO_Number__c
																FROM
																	AcctSeed__Billing_Cash_Receipts__r
																WHERE
																	CreatedDate <: this.StatementDate.addDays(1)
															)
														FROM
															AcctSeed__Cash_Receipt__c
														WHERE 
															AcctSeed__Account__c IN: this.AccountIds AND
															AcctSeed__Receipt_Date__c <: this.StatementDate.addDays(1) AND
															AcctSeed__Status__c = 'Posted' 

														ORDER BY 
															AcctSeed__Receipt_Date__c ASC];

		for(AcctSeed__Cash_Receipt__c cashReceipt : cashReceipts) {
			
			for(AcctSeed__Billing_Cash_Receipt__c bcr: cashReceipt.AcctSeed__Billing_Cash_Receipts__r) {
				cashReceipt.AcctSeed__Amount__c -= bcr.AcctSeed__Applied_Amount__c;
			}

			if(cashReceipt.AcctSeed__Amount__c != 0){
				if(!this.CashReceipts.containsKey(cashReceipt.AcctSeed__Account__c))
					this.CashReceipts.put(cashReceipt.AcctSeed__Account__c, new Map<Id, AcctSeed__Cash_Receipt__c>());

				this.CashReceipts.get(cashReceipt.AcctSeed__Account__c).put(cashReceipt.Id, cashReceipt);
			}

			//system.assert(false, cashReceipt + 'sssssssss' + cashReceipt.AcctSeed__Billing_Cash_Receipts__r);
			
			//REMOVED BY DANIEL

			////include only those Cash Receipts where Balance > 0 or were applied after date
			//if(cashReceipt.AcctSeed__Balance__c > 0 || cashReceipt.AcctSeed__Billing_Cash_Receipts__r.size() > 0) {

			//	//sum up the amount applied after Statement date
			//	if(cashReceipt.AcctSeed__Billing_Cash_Receipts__r.size() > 0) {
			//		Decimal paidAmountAfterDate = 0;

			//		for(AcctSeed__Billing_Cash_Receipt__c bcr : cashReceipt.AcctSeed__Billing_Cash_Receipts__r) {
			//			paidAmountAfterDate += bcr.AcctSeed__Applied_Amount__c;
			//		}

			//		this.cashReceipt_amountPaidLater_Map.put(cashReceipt.Id, paidAmountAfterDate);
			//	}

			//	if(!this.CashReceipts.containsKey(cashReceipt.AcctSeed__Account__c))
			//		this.CashReceipts.put(cashReceipt.AcctSeed__Account__c, new Map<Id, AcctSeed__Cash_Receipt__c>());

			//	this.CashReceipts.get(cashReceipt.AcctSeed__Account__c).put(cashReceipt.Id, cashReceipt);
			//}
		}

		//system.assert(false, this.CashReceipts.get(cashReceipts[0].AcctSeed__Account__c));
	}

    /*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/11/2017
	* @description 	 - Loads all Billing of type "Invoice", created before Statement date, with applied Credit Memos and/or Applied Cash Receipts after the Statement date.
	* 				   This is useful to get the balance of the Billing on the Statement date.
	*/
	private Map<Id, AcctSeed__Billing__c> LoadBillings_LaterApplied() {

		//since Created Date is a Datetime, and Statement Date is a Date (with Time = 00:00) => 
		//when you expect CreatedDate = StatementDate, actually CreatedDate > StatementDate based on the Time part
		//i.e. CreatedDate = 2017-10-15 15:00:00, StatementDate = 2017-10-15 (same date, but time parts are different)
		//therefore, we need to add 1 day to the StatementDate in order to get what we want
		/*
		Date statementDatePlus1 = this.StatementDate.addDays(1);

		system.debug('--------------------- StatementDate = ' + this.StatementDate);
		system.debug('--------------------- statementDatePlus1 = ' + statementDatePlus1);
		Datetime gtm = Datetime.newInstanceGmt(this.StatementDate, Time.newInstance(0, 0, 0, 0)).dateGMT();
		system.debug('--------------------- StatementDate GTM = ' + gtm);
		*/

		List<AggregateResult> billingCredited_List = [SELECT 
														AcctSeed__Billing_Invoice__c,
														SUM(AcctSeed__Amount__c) paidAmount
													FROM
														AcctSeed__Billing_Credit_Memo__c
													WHERE
														CreatedDate >=: this.StatementDate AND
														AcctSeed__Billing_Invoice__r.AcctSeed__Date__c <=: this.StatementDate AND 
														AcctSeed__Billing_Invoice__r.AcctSeed__Customer__c IN: this.AccountIds
													GROUP BY
														AcctSeed__Billing_Invoice__c];
													//HAVING
													//	COUNT(AcctSeed__Billing_Credit_Memo__c) > 0

		List<AggregateResult> billingCashed_List = [SELECT 
														AcctSeed__Billing__c,
														SUM(AcctSeed__Applied_Amount__c) paidAmount
													FROM
														AcctSeed__Billing_Cash_Receipt__c
													WHERE
														CreatedDate >=: this.StatementDate AND
														AcctSeed__Billing__r.AcctSeed__Date__c <=: this.StatementDate AND 
														AcctSeed__Billing__r.AcctSeed__Customer__c IN: this.AccountIds
													GROUP BY
														AcctSeed__Billing__c];
													//HAVING
													//	COUNT(AcctSeed__Cash_Receipt__c) > 0

		//system.assert(false, billingCredited_List + '  ' + billingCashed_List);


		Set<String> billingIds = new Set<String>();

		for(AggregateResult data : billingCredited_List) {
			String billingId = (String)data.get('AcctSeed__Billing_Invoice__c');
			Decimal paidAmount = Math.abs((Decimal)data.get('paidAmount'));
			
			billingIds.add(billingId);
			this.billing_amountPaidLater_Map.put(billingId, paidAmount);
		}

		for(AggregateResult data : billingCashed_List) {
			String billingId = (String)data.get('AcctSeed__Billing__c');
			Decimal paidAmount = Math.abs((Decimal)data.get('paidAmount'));
			
			Decimal runningAmount = this.billing_amountPaidLater_Map.get(billingId);
			runningAmount = runningAmount == null ? 0 : runningAmount;
			paidAmount += Math.abs(runningAmount);
			
			billingIds.add(billingId);
			this.billing_amountPaidLater_Map.put(billingId, paidAmount);
		}

		return new Map<Id, AcctSeed__Billing__c>([SELECT 
													Id,
													Name,
													CreatedDate,
													AcctSeed__Customer__c,
													AcctSeed__Date__c,
													AcctSeed__Proprietary_Billing_Number__c,
													AcctSeed__PO_Number__c,
													AcctSeed__Due_Date2__c,
													AcctSeed__Total__c,
													AcctSeed__Balance__c,
													AcctSeed__Type__c												
												FROM
													AcctSeed__Billing__c
												WHERE 
													Id IN: billingIds]);
	}

    /*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/11/2017
	* @description 	 - Loads all Billings of type "Credit Memo" that have been applied after the Statement date to a Billing created before Statement date.
	* 				   This is useful to get the balance of the Billing on the Statement date.
	*/
	private Map<Id, AcctSeed__Billing__c> LoadCreditMemos_LaterApplied() {

		//since Created Date is a Datetime, and Statement Date is a Date (with Time = 00:00) => 
		//when you expect CreatedDate = StatementDate, actually CreatedDate > StatementDate based on the Time part
		//i.e. CreatedDate = 2017-10-15 15:00:00, StatementDate = 2017-10-15 (same date, but time parts are different)
		//therefore, we need to add 1 day to the StatementDate in order to get what we want
		//Date statementDatePlus1 = this.StatementDate.addDays(1);

		List<AggregateResult> billingCredited_List = [SELECT 
														AcctSeed__Billing_Credit_Memo__c,
														SUM(AcctSeed__Amount__c) paidAmount
													FROM
														AcctSeed__Billing_Credit_Memo__c
													WHERE
														CreatedDate >=: this.StatementDate AND
														AcctSeed__Billing_Credit_Memo__r.AcctSeed__Date__c <=: this.StatementDate AND 
														AcctSeed__Billing_Credit_Memo__r.AcctSeed__Customer__c IN: this.AccountIds
													GROUP BY
														AcctSeed__Billing_Credit_Memo__c];


		Set<String> billingIds = new Set<String>();

		for(AggregateResult data : billingCredited_List) {
			String creditMemoId = (String)data.get('AcctSeed__Billing_Credit_Memo__c');
			Decimal paidAmount = (Decimal)data.get('paidAmount');

			billingIds.add(creditMemoId);
			this.creditMemo_amountPaidLater_Map.put(creditMemoId, paidAmount);
		}

		//System.assert(false, creditMemo_amountPaidLater_Map);

		return new Map<Id, AcctSeed__Billing__c>([SELECT 
													Id,
													Name,
													CreatedDate,
													AcctSeed__Customer__c,
													AcctSeed__Date__c,
													AcctSeed__Proprietary_Billing_Number__c,
													AcctSeed__PO_Number__c,
													AcctSeed__Due_Date2__c,
													AcctSeed__Total__c,
													AcctSeed__Balance__c,
													AcctSeed__Type__c												
												FROM
													AcctSeed__Billing__c
												WHERE 
													Id IN: billingIds]);
	}

    /*
	* @author    	 - Marcel Gomez
	* @date  	     - 10/11/2017
	* @description 	 - Generates a Statement__c record of type "Outstanding"  taking into account Invoices, Credit Memos and Cash Receipts active (Balance != 0) on Statement date
	*/
	public List<Statement__c> generateOutStandingStatement(Boolean insertDirectly) {
		this.LoadUnnaplied_Invoices_CreditMemos();
		this.LoadCashReceipts();

		Map<Id, AcctSeed__Billing__c> laterAppliedBillings 		= this.LoadBillings_LaterApplied();
		Map<Id, AcctSeed__Billing__c> laterAppliedCreditMemos 	= this.LoadCreditMemos_LaterApplied();

		for(Id accountId : this.AccountIds) {

			Decimal finalBalance = 0;
			List<Statement_Row> account_rows = new List<Statement_Row>();

			//filter the list of Invoices paid after Statement date to get only those of the current Account
			//Map<Id, AcctSeed__Billing__c> accountBillingsAppliedLater = new Map<Id, AcctSeed__Billing__c>();
			//for(Id billingId : laterAppliedBillings.keySet()) {
			//	AcctSeed__Billing__c currentBilling = laterAppliedBillings.get(billingId);
				
			//	if(currentBilling.AcctSeed__Customer__c == accountId)
			//		accountBillingsAppliedLater.put(billingId, currentBilling);
			//}

			//filter the list of Credit Memos applied after Statement date to get only those of the current Account
			//Map<Id, AcctSeed__Billing__c> accountCreditMemosAppliedLater = new Map<Id, AcctSeed__Billing__c>();
			//for(Id billingId : laterAppliedCreditMemos.keySet()) {
			//	AcctSeed__Billing__c currentBilling = laterAppliedCreditMemos.get(billingId);
				
			//	if(currentBilling.AcctSeed__Customer__c == accountId)
			//		accountCreditMemosAppliedLater.put(billingId, currentBilling);
			//}

			//merge Invoices with Balance > 0 with Invoices applied after Statement date to avoid dupes
			Map<Id, AcctSeed__Billing__c> uniqueInvoices = this.UnappliedInvoices.get(accountId);

			//merge Credit Memos with Balance > 0 with Credit Memos applied after Statement date to avoid dupes
			Map<Id, AcctSeed__Billing__c> uniqueCreditMemos = this.UnappliedCreditMemos.get(accountId);

			//analyze all Invoices to generate Debit Statement Line Item
			if(uniqueInvoices != null)
				for(Id billingId : uniqueInvoices.keySet()) {
					AcctSeed__Billing__c billing = uniqueInvoices.get(billingId);

					Decimal billingBalanceOnStatementDate = billing.AcctSeed__Credit_Memo_Applied_Amount__c;
					
					system.debug('-------------------- this.billing_amountPaidLater_Map.containsKey(billingId) = ' + this.billing_amountPaidLater_Map.containsKey(billingId));
					system.debug('-------------------- billing Balance Paid Later = ' + this.billing_amountPaidLater_Map.get(billingId));
					system.debug('-------------------- billing Balance = ' + billing.AcctSeed__Balance__c);
	 				system.debug('-------------------- billing Total = ' + billing.AcctSeed__Total__c);
					system.debug('-------------------- billingBalanceOnStatementDate = ' + billingBalanceOnStatementDate);

					//balance += billingBalanceOnStatementDate;

					if(!this.IsSetup) {
						Statement_Row row = StatementManager_Helper.CreateRowFromBilling(billing, 
						                                                                 billingBalanceOnStatementDate,
						                                                                 StatementManager_Helper.StatementActivity.Debit);

						system.debug('----------------------------- Statement_Row:');
						system.debug(row);

						account_rows.add(row);
						finalBalance += row.Total;
					}

				}

			//analyze all Credit Memos to generate Debit Statement Line Item
			if(uniqueCreditMemos != null)
				for(Id creditMemoId : uniqueCreditMemos.keySet()) {
					AcctSeed__Billing__c creditMemo = uniqueCreditMemos.get(creditMemoId);

					Decimal creditMemoBalanceOnStatementDate = creditMemo.AcctSeed__Credit_Memo_Applied_Amount__c;

					//balance += creditMemoBalanceOnStatementDate;

					if(!this.IsSetup) {
						Statement_Row row = StatementManager_Helper.CreateRowFromBilling(creditMemo, 
						                                                                 creditMemoBalanceOnStatementDate,
						                                                                 StatementManager_Helper.StatementActivity.Credit);

						account_rows.add(row);
						finalBalance += row.Total;
					}
				}

			//analyze all Cash Receipts to generate Debit Statement Line Item
			if(this.CashReceipts.containsKey(accountId)) {
				for(Id cashReceiptId : this.CashReceipts.get(accountId).keySet()) {
					AcctSeed__Cash_Receipt__c cashReceipt = this.CashReceipts.get(accountId).get(cashReceiptId);

					Decimal cashReceiptBalanceOnStatementDate = this.cashReceipt_amountPaidLater_Map.containsKey(cashReceipt.Id) ? 
																	cashReceipt.AcctSeed__Balance__c + Math.abs(this.cashReceipt_amountPaidLater_Map.get(cashReceipt.Id)) : 
																	cashReceipt.AcctSeed__Amount__c;


					//balance += creditMemoBalanceOnStatementDate;
					
					if(!this.IsSetup) {
						Statement_Row row = StatementManager_Helper.CreateRowFromCashReceipt(cashReceipt,
																							 -cashReceipt.AcctSeed__Amount__c,
						                                                                     StatementManager_Helper.StatementActivity.Credit);

						account_rows.add(row);
						finalBalance += row.Total;
					}
				}
			}

			this.Balance.put(accountId, finalBalance);//debits - credits);

			if(!this.IsSetup)
				this.LineItems.put(accountId, account_rows);
		}

		if(!this.IsSetup) {
			//sort Statement Line Items by Date
			for(Id accountId : this.LineItems.keySet()) {
				List<Statement_Row> rows = this.LineItems.get(accountId);
				rows = StatementManager_Helper.SortStatementsQuickSort(rows);

				this.LineItems.put(accountId, rows);

				//update Balance of every Statement Line Item based on chronological operations
				StatementManager_Helper.UpdateCumulativeBalance(this.LineItems.get(accountId), StatementManager_Helper.StatementType.Outstanding);
			}
		}

		//get the Billing Contact of the Accounts
		Map<Id, Account> billingContacts_Map = StatementManager_Helper.getBillingContacts(this.AccountIds);

		String sType = String.valueOf(StatementManager_Helper.StatementType.Outstanding);

		List<Statement__c> statements2Insert = new List<Statement__c>();
		List<Statement__c> statements2Delete = new List<Statement__c>();

		for(Id accountId : billingContacts_Map.keySet()) {
			
			Statement__c st 		= new Statement__c();
			st.Account__c       	= accountId;
			st.Balance__c       	= this.Balance.get(accountId);
			st.Type__c         		= sType;
			st.Period_Start_Date__c = null;
			st.Period_End_Date__c   = this.StatementDate;
			st.Account_Billing_Contact__c = billingContacts_Map.get(accountId).AcctSeed__Billing_Contact__c;
			st.Send__c = this.SendEmail;
				
			//Always set for deletion so the Statements are recreated.
			statements2Delete.add(st);

			//But only insert the Statement__c records if their Balance is greater than zero			
			if(this.Balance.get(accountId) != null && this.Balance.get(accountId) > 0) {			
				statements2Insert.add(st);
			}

		}

		//insert if needed
		if(insertDirectly && (statements2Insert.size() > 0 || statements2Delete.size() > 0)) {
			// Delete old ones that match with new ones
			delete StatementManager_Helper.getExistingStatements(statements2Delete, sType);
			// Insert new ones
			insert statements2Insert;

			List<Id> statementIds = new List<Id>();
			//generate the attached PDF file
			for(Statement__c statement : statements2Insert) {
				statementIds.add(statement.Id);
			}

			if(!this.GeneratedFromBatch)
				StatementManager_Helper.attachPDFToStatements_Future(statementIds);
		}

		return statements2Insert;
	}
}