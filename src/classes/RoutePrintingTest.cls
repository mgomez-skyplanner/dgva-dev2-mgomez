/**
 * Class to test route printing
 * @author William Garcia
 * @date   05/23/2017
 */
@isTest (seeAllData = false)
public class RoutePrintingTest {
	
	public static DateTime now = DateTime.now();
	public static String strNow = String.valueOf(now);

	public static Date nowD = now.date();
	public static String strNowD = String.valueOf(nowD.year()) + '-' + 
		String.valueOf(nowD.month()) + '-' + String.valueOf(nowD.day());

	/**
	 * Init test data
	 * @author William Garcia
 	 * @date   05/23/2017
	 * 
	 */
	@testSetup
    public static void init() {
		
    	Route__c route1 = new Route__c(Name = 'route1');
    	insert route1;

    	Account acc1 = new Account(
            Name = 'Account1',
            BillingCountry = 'C1',
            BillingCity = 'C2',
            Total_Balance__c = 50,
            Route__c = route1.Id
        );
        insert acc1;

    	Product2 prod1 = new Product2(
    		Name = 'Product1',
    		IsActive = true,
    		Price__c = 10,
    		Units_Pk__c = 'DOZ',
    		SKU__c = '123'
    	);
    	insert prod1;

    	PricebookEntry pbe1 = [SELECT Id 
    							FROM PricebookEntry 
    							WHERE Pricebook2Id = :[SELECT Price_Book__c 
    													FROM Account 
    													WHERE Id = :acc1.Id][0].Price_Book__c 
    							AND Product2Id = :prod1.Id];

        Order order1 = new Order (
            AccountId = acc1.Id,
            EffectiveDate = nowD,
            EndDate = nowD,
            Type = Constants.ORDER_TYPE_REGULAR,
            Status = Constants.ORDER_STATUS_DRAFT
        );
        insert order1;

        OrderItem orderItem1 = new OrderItem(
        	EndDate = nowD,
        	OrderId = order1.Id,
        	PricebookentryId = pbe1.Id,
        	Quantity = 2,
        	Total_Pieces__c = 2,
            SKU__c = '123',
            UnitPrice = 10
        );
        insert orderItem1;

        order1.Status = Constants.ORDER_STATUS_PREPARED;
        update order1;

        order1.Status = 'Prepared';
        update order1;
        order1.Status = 'Activated';
        update order1;

        order1 = [SELECT Id, ActivatedDate FROM Order WHERE Id =: order1.Id];

        RoutePrintingTest.now = order1.ActivatedDate;
        RoutePrintingTest.strNow = String.valueOf(RoutePrintingTest.now.getTime());
    }

    /**
     * Test get routes
     * @author William Garcia
 	 * @date   05/23/2017
     */
    static testMethod void getRoutesTest() {

        Order order1 = [SELECT Id, ActivatedDate FROM Order];

        String activatedDate = String.valueOf(order1.ActivatedDate.getTime());

        system.debug('----------- activatedDate:');
        system.debug(activatedDate);

    	// Working
    	List<RoutePrintingHelper.RouteWrapper> routes = RoutePrintingCtrl.getRoutes(strNowD, activatedDate);

    	System.assertEquals(routes.size(), 1);
    	System.assertEquals(routes[0].Name, 'route1');
    	System.assertEquals(routes[0].Quantity, 1);

    	// Exception 1
    	try {
	    	routes = RoutePrintingCtrl.getRoutes('123', '');
	    } catch(Exception ex) {
	    	System.assert(ex.getMessage().contains('There was an error getting routes.'));    	
	    }

	    // Exception 2
    	try {
	    	routes = RoutePrintingCtrl.getRoutes('', '');
	    } catch(Exception ex) {
	    	System.assert(ex.getMessage().contains('Delivery date missing.'));    	
	    }
    }

    /**
     * Test confirmPrinting method
     * @author Marcel Gomez
 	 * @date   09/17/2017
     */
    static testMethod void test_confirmPrinting() {

        Order order1 = [SELECT Id, ActivatedDate FROM Order];

        String activatedDate = String.valueOf(order1.ActivatedDate.getTime());

        system.debug('----------- activatedDate:');
        system.debug(activatedDate);

    	// Working
    	List<RoutePrintingHelper.RouteWrapper> routes = RoutePrintingCtrl.getRoutes(strNowD, activatedDate);

    	System.assertEquals(routes.size(), 1);

    	RoutePrintingHelper.RouteWrapper route1 = RoutePrintingCtrl.confirmPrinting(routes[0].Id, strNowD, activatedDate);

    	RoutePrintingHelper.RouteOrdersWrapper oWrapper = route1.Orders[0];

    	System.assertEquals(UserInfo.getUserId(), oWrapper.PrintedBy);
    	System.assert(oWrapper.PrintedOn != NULL);
    }
}