/**
* <p> 
* Version log:
* <ul>
* <li>1.00 07/06/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 07/06/2017
* @description 	 - Test class for class WebManager_Stripe (97% - 07/07/2017)
*/
 
@isTest
private class WebManager_Stripe_Test {

	@testSetup
	static void setup() {
		TestClass_Utility.createDefault_StripeSettings();
		TestClass_Utility.CreateDefaultSettingsAccountingSeed();
		TestClass_Utility.createCustomSettingsData();

		Route__c route 	= TestClass_Utility.createRoute('Test Route');
		insert route;

		Account account = TestClass_Utility.createAccount('Test Account', route);
		insert account;

		Opportunity opp = TestClass_Utility.createOpportunity(account);
		insert opp;

		Order order 			= TestClass_Utility.createOrder(account, opp, 'Draft');
		insert order;

		Product2 product1 		= TestClass_Utility.createProduct('Test Product 1');
		insert product1;

		Id standardPriceBookId = Test.getStandardPricebookId();
		PricebookEntry pbe 		= [SELECT Id FROM PricebookEntry WHERE Product2Id =: product1.Id AND PriceBook2Id =: standardPriceBookId];

		OrderItem orderProduct1 = TestClass_Utility.createOrderProduct(order, pbe);
		insert orderProduct1;

		TestClass_Utility.CreateDefaultDataForBillings(account, order, product1);
	}

	@isTest
	static void test_CreateCharge() {
		Test.startTest();

		Order order = [SELECT Id, Status FROM Order];

		order.Status = 'Prepared';
		update order;

		TriggerControlClass.stopOrderTriggers = true;

		order.Status = 'Activated';
		update order;

		TriggerControlClass.stopOrderTriggers = false;

		//reload order
		BillingManager.LoadCustomSettings();
		order = BillingManager.ReloadOrder(order.Id);

		AcctSeed__Billing__c billing = BillingManager.GenerateBillingFromOrder(order, true);

		Test.stopTest();

		billing = [SELECT
						Id,
						AcctSeed__Customer__c,

						AcctSeed__Balance__c,
						AcctSeed__Total__c,
						AcctSeed__Received_Amount__c,
						AcctSeed__Cash_Application_Adjustment_Amount__c,
						AcctSeed__Credit_Memo_Applied_Amount__c,
						AcctSeed__Accounting_Period__c,
						(
							SELECT
								Id,
								AcctSeed__Hours_Units__c,
								AcctSeed__Rate__c
							FROM 
								AcctSeed__Project_Billing_Lines__r
						)
					FROM
						AcctSeed__Billing__c
					WHERE
						Id =: billing.Id
					];

		if(billing.AcctSeed__Balance__c > 0) {
			system.debug('------------------------------- Billing: ');
			system.debug(billing);
			system.debug(billing.AcctSeed__Project_Billing_Lines__r);

			WebManager_Stripe.IsTest = true;
			StripeCharge charge = WebManager_Stripe.CreateCharge(billing.Id);

			AcctSeed__Cash_Receipt__c cashReceipt = WebManager_Stripe.CreateCashReceipt(billing.AcctSeed__Customer__c, charge.customer_id, billing.AcctSeed__Balance__c);
			insert cashReceipt;

			AcctSeed__Billing_Cash_Receipt__c billingCashReceipt = WebManager_Stripe.CreateBillingCashReceipt(billing.Id, 
			                                                                                                  billing.AcctSeed__Accounting_Period__c,
			                                                                                                  cashReceipt);
			insert billingCashReceipt;

			system.debug('----------------- StripeCharge:');
			system.debug(charge);
		}
	}	
}