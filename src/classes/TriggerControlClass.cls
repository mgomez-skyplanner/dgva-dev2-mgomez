/**
* @author    	 - Marcel Gomez
* @date  	     - 06/09/2017
* @description 	 - This class is intended to provide the same functionality the custom setting Trigger_Control_Setting__c provides,
* 				   but it only works on in-memory scenarios (same execution thread) and related to a specific custom or standar object.
*/

public class TriggerControlClass {

	public static Boolean stopOrderTriggers = false;
	public static Boolean stopOrderItemTriggers = false;
	public static Boolean stopAccountTriggers = false;
}