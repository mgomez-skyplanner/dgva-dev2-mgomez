/**
 * Helper class for route printing
 * @author William Garcia
 * @date   05/23/2017
 */
public class RoutePrintingHelper {
	
	/**
	 * 	Retrieve route list
	 *  @author    William Garcia
	 *  @date      05/23/2017
	 *  @param     deliveryDate
	 *  @return    List<RouteWrapper> Return route list
	 */
	public static List<RouteWrapper> getRoutes(String deliveryDate, String activationDate) {
		List<RouteWrapper> result = new List<RouteWrapper>();
		
		Date dd = strToDate(deliveryDate);
		Datetime actDatetime = strToDateTime(activationDate);

		OrderManager orderManager = new OrderManager();
		List<Order> ordersList = orderManager.fetchByActivationDate(dd, actDatetime);

		Map<Id, List<Order>> route_orders = new Map<Id, List<Order>>();
		for(Order order : ordersList) {
			if(!route_orders.containsKey(order.Route__c))
				route_orders.put(order.Route__c, new List<Order> { order });
			else
				route_orders.get(order.Route__c).add(order);
		}

		for(Id routeId : route_orders.keySet()) {
			List<Order> orders 	= route_orders.get(routeId);

			result.add(new RouteWrapper(routeId, orders));
		}

		return result;
	}

	/**
	* @author 		Marcel Gomez
	* @date			09/15/2017
	* @description 	Confirm Printing of Orders
	*/
	public static RouteWrapper confirmPrinting(Id routeId, String deliveryDate, String activationDate) {
		Date dd = strToDate(deliveryDate);
		Datetime actDatetime = strToDateTime(activationDate);

		OrderManager orderManager = new OrderManager();
		List<Order> ordersList = orderManager.fetchByRouteActivationDate(routeId, dd, actDatetime);

		Id userId = UserInfo.getUserId();
		Datetime dNow = Datetime.now();

		for(Order order : ordersList) {
			order.Printed_By__c = userId;
			order.Printed_On__c = dNow;
		}

		update ordersList;

		system.debug('------------------------------- ordersList: ');
		system.debug(ordersList);

		return new RouteWrapper(routeId, ordersList);
	}

	/**
	 *  m/d/y to Date
	* @author 		William Garcia
	* @date			05/23/2017
	* @description 	Route wrapper
	*/
	public static Date strToDate(String strDate) {
		String[] arr = strDate.split('-');
		return Date.newInstance(integer.valueOf(arr[0]), integer.valueOf(arr[1]), 
			integer.valueOf(arr[2]));
	}	

	/**
	* @author 		Marcel Gomez
	* @date			09/16/2017
	* @description 	Converts tickcount to Datetime
	*/
	public static Datetime strToDateTime(String strTickCount) {
		return Datetime.newInstance(Long.valueOf(strTickCount));
	}

	/**
	* @author 		William Garcia
	* @date			05/23/2017
	* @description 	Route wrapper
	*/
	public class RouteWrapper {
		public String Id {get; set;}
		public String Name {get; set;}

		public Integer Quantity {get; set;}
		public Integer PrintedQuantity {get; set;}
		public Integer UnprintedQuantity {get; set;}

		public List<RouteOrdersWrapper> Orders {get; set;}
		public Boolean PrintedButtonPressed { get; set; }
		public Boolean ConfirmPrintingButtonPressed { get; set; }
		public Datetime OperationStartDatetime { get; set; }

		public RouteWrapper() {
			this.PrintedButtonPressed = false;
			this.ConfirmPrintingButtonPressed = false;
			this.OperationStartDatetime = Datetime.now();
		}

		public RouteWrapper(Id routeId, List<Order> orders) {
			this.Id 		= routeId;
			this.Name 		= orders[0].Route__r.Name;

			Integer printedQty = 0;
			Integer unprintedQty = 0;

			List<RouteOrdersWrapper> ordersWrappers = new List<RouteOrdersWrapper>();
			for(Order order : orders) {
				ordersWrappers.add(new RouteOrdersWrapper(order.Id, 
				                                          (order.Printed_By__c != NULL) ? (String)order.Printed_By__c : '', 
				                                          (order.Printed_On__c != NULL) ? order.Printed_On__c.format('MM/dd/yyyy HH:mm:ss') : ''));

				if(order.Printed_By__c != NULL)
					printedQty += 1;
				else 
					unprintedQty += 1;
			}

			this.PrintedQuantity 	= printedQty;
			this.UnprintedQuantity 	= unprintedQty;
			this.Quantity 			= unprintedQty;
			this.OperationStartDatetime = Datetime.now();

			this.Orders	= ordersWrappers;
		}
	}

	public class RouteOrdersWrapper {
		public String OrderId 	{ get; set; }
		public String PrintedBy { get; set; }
		public String PrintedOn { get; set; }

		public RouteOrdersWrapper(String orderId, String printedBy, String printedOn) {
			this.OrderId = orderId;
			this.PrintedBy = printedBy;
			this.PrintedOn = printedOn;
		}
	}
}