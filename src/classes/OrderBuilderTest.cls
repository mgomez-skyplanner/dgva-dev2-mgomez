/**
 * Class to test order builder
 * @author William Garcia
 * @date   03/21/2017
 */
@isTest (seeAllData = false)
public class OrderBuilderTest {
	/**
	 * Init test data
	 * @author William Garcia
	 * @date   03/21/2017
	 * 
	 */
	@testSetup
	public static void init() {
		DateTime now = DateTime.now();
		Date nowD = now.date() + 1;

		Route__c route1 = new Route__c(Name = 'route1');
		insert route1;

		Account acc1 = new Account(
			Name = 'Account1',
			BillingCountry = 'C1',
			BillingCity = 'C2',
			Total_Balance__c = 50,
			Route__c = route1.Id
		);
		insert acc1;

		Product2 prod1 = new Product2(
			Name = 'Product1',
			IsActive = true,
			Price__c = 10,
			Units_Pk__c = 'DOZ',
			SKU__c = '123',
			Family = 'Pastry',
			Subcategory__c = 'Pies & Cakes'
		);
		insert prod1;

		Product2 prod2 = new Product2(
			Name = 'Product2',
			IsActive = true,
			Price__c = 2,
			Units_Pk__c = 'EACH',
			SKU__c = '456',
			Family = 'Pastry',
			Subcategory__c = 'Pies & Cakes',
			IsAddon__c = true
		);
		insert prod2;

		Product2 prod3 = new Product2(
			Name = 'Product3',
			IsActive = true,
			Price__c = 50,
			Units_Pk__c = 'EACH',
			SKU__c = '456',
			Family = 'Pastry',
			Subcategory__c = 'Pies & Cakes',
			SupportAddons__c = true,
			SingleOrder__c = true
		);
		insert prod3;

		PricebookEntry pbe1 = [
			SELECT Id 
			FROM PricebookEntry 
			WHERE Pricebook2Id = :[
				SELECT Price_Book__c 
				FROM Account 
				WHERE Id = :acc1.Id
			][0].Price_Book__c 
			AND Product2Id = :prod1.Id];

		Order order1 = new Order (
			AccountId = acc1.Id,
			Status = 'Draft', 
			EffectiveDate = nowD,
			EndDate = nowD
		);
		insert order1;

		OrderItem orderItem1 = new OrderItem(
			EndDate = nowD,
			OrderId = order1.Id,
			PricebookentryId = pbe1.Id,
			Quantity = 2,
			Total_Pieces__c = 2,
			SKU__c = '123',
			UnitPrice = 10
		);
		insert orderItem1;

		insert new List<Addon__c> {
			new Addon__c(
				Name = 'addon 1',
				AppliesToFamily__c = 'Pastry',
				AppliesToSubcategory__c = 'Pies & Cakes',
				Category__c = 'Extra Layer'
			),
			new Addon__c(
				Name = 'addon 2',
				AppliesToFamily__c = 'Pastry',
				AppliesToSubcategory__c = 'Pies & Cakes',
				Category__c = 'Filling'
			),
			new Addon__c(
				Name = 'addon 3',
				AppliesToFamily__c = 'Pastry',
				AppliesToSubcategory__c = 'Pies & Cakes',
				Category__c = 'Toppings',
				Product__c = prod2.Id
			)
		};
	}

	/**
	 * Test account lookup
	 * @author William Garcia
	 * @date   03/21/2017
	 */
	static testMethod void lookupObject() {
		System.assertEquals(
			OrderBuilderCtrl.lookupObject(
				'Account', 'Name', 'Account', null).size(), 1);
	}

	/**
	 * Test account lookup exception
	 * @author William Garcia
	 * @date   03/21/2017
	 */
	static testMethod void lookupObjectException() {
		OrderBuilderHelper.WhereClauseStatement w =
				new OrderBuilderHelper.WhereClauseStatement();

		w.fieldName = 'a';
		w.comparator = '=';
		w.useQuotes = true;
		w.value = 'yes';

		try {
			OrderBuilderCtrl.lookupObject('Object1', 'a', 'b', 
				new List<OrderBuilderHelper.WhereClauseStatement> { w });
			System.assert(false);
		} catch(Exception ex) {
			System.assert(ex.getMessage().contains(
				'sObject type \'Object1\' is not supported.'));
		}
	}

	/**
	 * Test select account
	 * @author William Garcia
	 * @date   03/21/2017
	 */
	static testMethod void selectAccount() {
		OrderBuilderHelper.AccountWrapper accWrapper = 
			OrderBuilderCtrl.selectAccount([
				SELECT Id 
				FROM Account 
				WHERE Name = 'Account1'].Id);

		system.assert(accWrapper.account.Total_Balance__c == 50);
		system.assert(accWrapper.products.size() == 1);
		system.assert(accWrapper.products[0].price == 10);
	}

	/**
	 * Test select account exception
	 * @author William Garcia
	 * @date   03/21/2017
	 */
	static testMethod void selectAccountException() {
		try {
			OrderBuilderCtrl.selectAccount(null);
			System.assert(false);
		} catch(Exception ex) {
			system.assertEquals(ex.getMessage(), 'Account\'s id missing.');
		}
	}

	/**
	 * Test search products
	 * @author William Garcia
	 * @date   03/21/2017
	 */
	static testMethod void searchProducts() {
		Account acc = [
			SELECT Price_Book__c 
			FROM Account 
			WHERE Name = 'Account1'
		];

		List<OrderBuilderHelper.ProductWrapper> prods = 
			OrderBuilderCtrl.searchProducts(
				acc.Id, acc.Price_Book__c, 'Product1');

		system.assert(prods.size() == 1);
		system.assert(prods[0].sku == '123');
	}

	/**
	 * Test search products exception
	 * @author William Garcia
	 * @date   03/21/2017
	 */
	static testMethod void searchProductsException() {
		try {
			OrderBuilderCtrl.searchProducts(null, null, 'Product1');
		} catch(Exception ex) {
			system.assert(ex.getMessage().contains('id missing.'));
		}
	}

	/**
	 * Test get products from account last orders
	 * @author William Garcia
	 * @date   03/21/2017
	 */
	static testMethod void getProducts() {
		Account acc1 = [
			SELECT Id, Price_Book__c 
			FROM Account 
			WHERE Name = 'Account1'
		];

		List<OrderBuilderHelper.ProductWrapper> prods = 
			OrderBuilderCtrl.getProducts(acc1.Price_Book__c, acc1.Id);

		system.assert(prods.size() == 1);
		system.assert(prods[0].sku == '123');
	}

	/**
	 * Test get products exception
	 * @author William Garcia
	 * @date   03/21/2017
	 */
	static testMethod void getProductsException() {
		try {
			OrderBuilderCtrl.getProducts(null, null);
			System.assert(false);
		} catch(Exception ex) {
			system.assertEquals(ex.getMessage(), 'PriceBook\'s id missing.');
		}
	}

	/**
	 * Test save order
	 * @author William Garcia
	 * @date   03/21/2017
	 */
	static testMethod void saveOrder() {
		Account acc1 = [
			SELECT 
				Id, 
				Name, 
				Price_Book__c 
			FROM Account 
			WHERE Name = 'Account1'
		];
		Product2 prod1 = [
			SELECT Id 
			FROM Product2 
			WHERE Name = :'Product1'
		];
		Product2 prod3 = [
			SELECT Id 
			FROM Product2 
			WHERE Name = :'Product3'
		];
		PricebookEntry pbe1 = [
			SELECT Id,
				  Product2Id,
				  Product2.Name,
				  UnitPrice,
				  Product2.SKU__c,
				  Product2.Units_Pk__c,
				  Product2.Family,
				  Product2.Subcategory__c,
				  Product2.SupportAddons__c,
				  Product2.SingleOrder__c
			FROM PricebookEntry 
			WHERE Pricebook2Id = : [
				SELECT Price_Book__c 
				FROM Account 
				WHERE Id = :acc1.Id
			][0].Price_Book__c 
			AND Product2Id = :prod1.Id
		];
		PricebookEntry pbe3 = [
			SELECT Id,
				  Product2Id,
				  Product2.Name,
				  UnitPrice,
				  Product2.SKU__c,
				  Product2.Units_Pk__c,
				  Product2.Family,
				  Product2.Subcategory__c,
				  Product2.SupportAddons__c,
				  Product2.SingleOrder__c
			FROM PricebookEntry 
			WHERE Pricebook2Id = : [
				SELECT Price_Book__c 
				FROM Account 
				WHERE Id = :acc1.Id
			][0].Price_Book__c 
			AND Product2Id = :prod3.Id
		];

		// Create product wrapper
		OrderBuilderHelper.ProductWrapper prodW =
				new OrderBuilderHelper.ProductWrapper(pbe1, 0);
		prodW.totalPieces = 10;

		OrderBuilderHelper.ProductWrapper prodC =
				new OrderBuilderHelper.ProductWrapper(pbe3, 1);
		prodC.totalPieces = 1;

		// Create order wrapper
		OrderBuilderHelper.OrderWrapper orderW =
				new OrderBuilderHelper.OrderWrapper();
		orderW.account = acc1;
		orderW.deliveryDate = System.now().addDays(3).getTime();
		orderW.products = new List<OrderBuilderHelper.ProductWrapper>{
				prodW, prodC };

		OrderBuilderHelper.ProductAddonWrapper paddon = 
				new OrderBuilderHelper.ProductAddonWrapper();
		List<AddonManager.Addon> m = OrderBuilderCtrl.getAddons(acc1.Price_Book__c,
				'Pastry', 'Pies & Cakes', 'Toppings');

		paddon.addonId = m[0].addonId;
		paddon.addonName = m[0].name;
		paddon.pricebookEntryId = m[0].pricebookEntryId;
		paddon.productId = m[0].productId;
		paddon.productSku = m[0].productSku;
		paddon.productName = m[0].productName;
		paddon.quantity = 1;
		paddon.unitPrice = m[0].price;
		paddon.totalPrice = m[0].price;
		paddon.supportsMultiple = m[0].supportsMultiple;

		prodC.addons = new Map<Id, OrderBuilderHelper.ProductAddonWrapper> { 
			paddon.addonId => paddon 
		};

		// Save new order
		Order newOrder1, newOrder2;
		newOrder1 = OrderBuilderCtrl.saveOrder(orderW);
		newOrder1 = [SELECT Order_Total__c FROM Order WHERE Id = :newOrder1.Id];
		newOrder2 = [SELECT Order_Total__c FROM Order
			WHERE Id != :newOrder1.Id ORDER BY CreatedDate DESC LIMIT 1];			
		
		system.assertEquals(152, newOrder1.Order_Total__c + newOrder2.Order_Total__c);
	}

	/**
	 * Test save order exception
	 * @author William Garcia
	 * @date   03/21/2017
	 */
	static testMethod void saveOrderException() {
		try {
			Order newOrder = OrderBuilderCtrl.saveOrder(null);
			System.assert(false);
		} catch(Exception ex) {
			system.assertEquals('Order or Account missing.', ex.getMessage());
		}
	}

	/**
	 * Test:
	 * public static void updatePreferences(Id accountId, Id productId, 
	 *		Boolean isFavorite, Boolean isShownByDefault)
	 * @author Fernando Gomez
	 * @date 
	 */
	@isTest
	static void updatePreferences() {
		OrderBuilderCtrl ctrl = new OrderBuilderCtrl();
		ProductPreference__c p;
		Account acc1 = [
			SELECT 
				Id, 
				Name, 
				Price_Book__c 
			FROM Account 
			WHERE Name = 'Account1'
		];
		Product2 prod1 = [
			SELECT Id 
			FROM Product2 
			WHERE Name = :'Product1'
		];

		OrderBuilderCtrl.updatePreferences(acc1.Id, prod1.Id, true, true);
		p = [
			SELECT
				IsFavorite__c,
				ShowByDefault__c
			FROM ProductPreference__c
			WHERE Account__c = :acc1.Id
			AND Product__c = :prod1.Id
		];

		System.assert(p.IsFavorite__c);
		System.assert(p.ShowByDefault__c);
	}

	/**
	 * Test:
	 * public static List<AggregateResult> getCategories(
	 *		String family, String subcategory)
	 * @author Fernando Gomez
	 * @date 
	 */
	@isTest
	static void getCategories() {
		System.assertEquals(3, OrderBuilderCtrl.getCategories(
				'Pastry', 'Pies & Cakes').size());
	}

	/**
	 * Test:
	 * public static List<AddOn__c> getAddons(String family,
	 *		String subcategory, String category)
	 * @author Fernando Gomez
	 * @date 
	 */
	@isTest
	static void getAddons() {
		System.assertEquals(1, OrderBuilderCtrl.getAddons(null,			
				'Pastry', 'Pies & Cakes', 'Extra Layer').size());

		System.assertEquals(1, OrderBuilderCtrl.getAddons(null,
				'Pastry', 'Pies & Cakes', 'Filling').size());

		Account acc1 = [
			SELECT 
				Id, 
				Name, 
				Price_Book__c 
			FROM Account 
			WHERE Name = 'Account1'
		];

		System.assertEquals(1, OrderBuilderCtrl.getAddons(acc1.Price_Book__c,
				'Pastry', 'Pies & Cakes', 'Toppings').size());
	}
}