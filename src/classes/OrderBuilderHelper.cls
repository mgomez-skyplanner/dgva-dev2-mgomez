/**
 * Helper class for order builder controller
 * @author William Garcia
 * @date   03/21/2017
 * @version	1.0	William Garica, 03/21/2017
 *			1.1 Fernando Gomez, 05/11/2017
 *			1.2 Fernando Gomez, 06/06/2017
 *			1.3 Fernando Gomez, 07/11/2017
 */
public class OrderBuilderHelper {
	
	/**
	 * Custom setting for configurations 
	 * @author William Garcia
	 * @date   03/21/2017
	 */
	public static Order_Builder_Config__c settings = 
			Order_Builder_Config__c.getOrgDefaults();

	/**
	 *  @author William Garcia
	 *	@modified Fernando Gomez
	 *  @date      03/16/2017
	 *  @param     objName     		The name of the object to use in the query  
	 *  @param     field       		The field to be used in the comparison
	 *  @param     criteria    		The criteria to compare
	 *  @param     whereConditions 	Additionali consideration to add to the 
	 *								where clause.
	 *  @return    List<sObject>	Returns up to 50 records of the specified 
	 *									object that	matches the criteria
	 */
	public static List<sObject> lookupObject(
			String objName, String field, String criteria,
			List<WhereClauseStatement> whereConditions) {
		List<String> fields = field.split(',');
		String w = '', comp = '', 
			searchText = criteria == null ? '' :
				String.escapeSingleQuotes(criteria),
			whereConditionsString = '';
		
		// first, we add all fields that are included in the criteria comparise
		// and OR then
		for (Integer i = 0; i < fields.size(); i++) {
			comp += (i > 0 ? ', ' : '') + String.escapeSingleQuotes(fields[i]);
			w += (w == '' ? ' WHERE (' : ' OR ') + 
				String.escapeSingleQuotes(fields[i]) + 
				' LIKE \'%' + searchText + '%\' ';
			// if the last and )
			if (i + 1 == fields.size())
				w += ') ';
		}
		
		// the, each where condition is ANDed
		if (whereConditions != null)
			for(WhereClauseStatement condition : whereConditions)
				whereConditionsString += ' AND ' +
					String.escapeSingleQuotes(condition.fieldName) + ' ' + 
					String.escapeSingleQuotes(condition.comparator) + ' ' +
					(condition.useQuotes ? '\'' : '') +
					String.escapeSingleQuotes(condition.value) +
					(condition.useQuotes ? '\' ' : ' ');
		
		// finally, we build the query
		return Database.query(
			'SELECT Id, ' + comp + 
			' FROM ' + String.escapeSingleQuotes(objName) + 
				w + whereConditionsString +
			' ORDER BY ' + String.escapeSingleQuotes(fields[0]) +
			' LIMIT 100');
	}

	/**
	 * 	Select account and retrieve all required info.
	 *  @author William Garcia
	 *	@modified Fernando Gomez
	 *  @date 03/17/2017
	 *  @param accId Account's id
	 *  @return AccountWrapper
	 */
	public static AccountWrapper selectAccount(Id accId) {
		Account acc = getAccount(accId);
		List<ProductWrapper> prods = getProducts(acc.Price_Book__c, accId);
		return new AccountWrapper(acc, prods);
	}

	/**
	 * Search products from some SKU and Name
	 * @author Fernando Gomez
	 * @date 03/17/2017
	 * @param accId
	 * @param pbId 		Account price book id
	 * @param criteria     Search criteria
	 * @return List<ProductWrapper> Product wrapper list
	 */
	public static List<ProductWrapper> searchProducts(
			Id accId, Id pbId, String criteria) {
		List<ProductWrapper> r;
		String cleaned;
		List<String> words;
		Set<String> strings;
		List<List<String>> permuted;
		Set<Id> productsAdded = new Set<Id>();
		Integer i = 0;
		ProductPreferenceManager preferences;
		// we throw an exception if the pricebook Id was
		// not provided...
		OrderBuilderException.customAssert(
				!String.isBlank(pbId), 'PriceBook\'s id missing.');
		// we create the list, so we can add result later..
		r = new List<ProductWrapper>();
		// if criteria is null or empty,
		// we return the empty list
		if (String.isBlank(criteria))
			return r;
		// first, we clean the criteria by removing all extra
		// spaces, line-breaks, tabs.. leaving only one space between words
		cleaned = criteria.replaceAll('[\\r\\n?|\\n|\\t| ]+', ' ');
		// we create the words list by splitting
		// the cleaned list on the blank spaces,
		words = cleaned.split(' ');
		// but we only use the first 5 words to keep 
		// the process fast, and to avoid blowing the heap limit.
		chunk(words, 5);
		// we create the source of permutations
		permuted = new List<List<String>>();
		strings = new Set<String>();
		// and we obtain all possible combinations of
		// words (permutations), using up to the first
		// five elements to reduce heap usage and increase speed.
		permute(words.size(), words, permuted);
		// we now obtain a single search text per permutation,
		// e.g. [(a,b),(b,a)] will be used as %a%b% and %b%a%
		// in the search query
		for (List<String> strs : permuted)
			strings.add('%' +  String.join(strs, '%') + '%');
		// we perform the query, returning records that
		// match at least one word
		for (PricebookEntry pe : [
				SELECT 
					Id,
					Product2Id,
					Product2.Name,
					UnitPrice, 
					Product2.SKU__c,
					Product2.Units_Pk__c,
					Product2.Family,
					Product2.Subcategory__c,
					Product2.SupportAddons__c,
					Product2.SingleOrder__c
				FROM PricebookEntry
				WHERE Pricebook2Id = :pbId 
				AND Product2.IsActive = true
				AND Product2.IsAddon__c = false
				AND (
					Product2.Name LIKE :strings
					OR Product2.SKU__c LIKE :strings
				)
				ORDER BY Product2.Name
				LIMIT 500
			]) {
			productsAdded.add(pe.Product2Id);
			r.add(new ProductWrapper(pe, i++));
		}
		// we still need to setup the preferences
		preferences = new ProductPreferenceManager(accId, productsAdded);
		setPreferences(accId, r, preferences);
		// we finally return the list wrappers
		return r;
	}
	
	/**
	 * 	Get account's product list
	 *  @author    William Garcia
	 *	@modified Fernando Gomez
	 *  @date      03/17/2017
	 *  @param     accId       		Account's id
	 *  @return    List<ProductWrapper>	Return account product list
	 */
	public static List<ProductWrapper> getProducts(Id pbId, Id accId) {
		Map<Id, Integer> indexes = new Map<Id, Integer>();
		Set<Id> products = new Set<Id>();
		List<ProductWrapper> result = new List<ProductWrapper>();
		Date maxDt;
		Integer i = 0;
		ProductPreferenceManager preferences;
		
		// we throw exeption if eith pricebook
		// or account is empty
		OrderBuilderException.customAssert(
				!String.isBlank(pbId), 'PriceBook\'s id missing.');
		OrderBuilderException.customAssert(
				!String.isBlank(accId), 'Account\'s id missing.');

		// we would be limiting the products to items
		// that have been ordered in the last 90 days
		maxDt = DateTime.now().addDays(
			settings.MaxOrdersDate__c == null ? -90 : 
				-1 * settings.MaxOrdersDate__c.intValue()).date();

		// we need to find all products that have been oredered in
		// the last 90 days.. we find them through the ordered items
		for (OrderItem ot : [
					SELECT PricebookEntry.Product2Id
					FROM OrderItem
					WHERE Order.AccountId = :accId
					AND Order.EffectiveDate >= :maxDt
					AND PricebookEntry.Product2.IsActive = true
					AND PricebookEntry.Product2.IsAddon__c = false
					ORDER BY PricebookEntry.Product2.Name,
						Order.EffectiveDate DESC 
					LIMIT 1000
				])
			// we only add the item if it is the first of its product..
			// since items are ordered by effective date descending,
			// the first of a products will be the last ordered
			if (!products.contains(ot.PricebookEntry.Product2Id)) {
				// we add the product to avoid repetition
				products.add(ot.PricebookEntry.Product2Id);
				// we specify its order in the result
				indexes.put(ot.PricebookEntry.Product2Id, ++i);
			}

		// we need to produce only one item per product,
		// the one that was ordered must currently
		// we build a map with all items mapped by product
		for (PricebookEntry pbe : [
					SELECT
						Product2Id,
						Product2.Name,
						UnitPrice, 
						Product2.SKU__c,
						Product2.Units_Pk__c,
						Product2.Family,
						Product2.Subcategory__c,
						Product2.SupportAddons__c,
						Product2.SingleOrder__c
					FROM PricebookEntry
					WHERE Pricebook2Id = :pbId
					AND Product2Id IN :products
				])
			// we create the wrapper
			// we add the product 
			result.add(new ProductWrapper(pbe,
					indexes.get(pbe.Product2Id)));

		// we still need to setup the preferences
		preferences = new ProductPreferenceManager(accId, products);
		setPreferences(accId, result, preferences);
			
		// we sort and return the list
		result.sort();
		return result;
	}

	/**
	 *  Get account from id
	 *  @author William Garcia
	 *  @date 03/17/2017
	 *  @param accId       	Account's id
	 *  @return Account		Account record
	 */
	public static Account getAccount(Id accId) {
		OrderBuilderException.customAssert(
				!String.isBlank(accId), 'Account\'s id missing.');
		List<Account> accList = [
			SELECT Id,
				Name,
				Past_Due__c, 
				Total_Balance__c,
				Price_Book__c,
				Order_Minimum_Amount__c,
				Route__r.Name
			FROM Account 
			WHERE Id = :accId
		];

		OrderBuilderException.customAssert(
			!accList.isEmpty(), 'Account not found.');

		Account acc = accList[0];

		OrderBuilderException.customAssert(
			!String.isBlank(acc.Price_Book__c), 
				'There is no Price Book asociated with this Account. ' +
				'Please contact System Administrator.');

		return acc;
	}

	/**
	 * Save new order
	 * @author Fernando Gomez
	 * @date july 11th, 2017
	 * @param order
	 */
	public static Order saveOrder(OrderWrapper order) {
		Date endDate;
		Order newOrder, currentOrder;
		List<Order> isolatedOrders, toPrepare;
		OrderItem newItem;
		List<OrderItem> orderItems, isolatedItems, addonItems;
		List<List<ProductAddonWrapper>> addons;
		Boolean ignoreMinimum;
		System.SavePoint sp;

		OrderBuilderException.customAssert(order != null &&
			order.account != null && order.account.Id != null,
				'Order or Account missing.');
		
		// end date need to be converted
		endDate = DateTime.newInstance(order.deliveryDate).date();

		// Create and insert the new order
		newOrder = new Order(
			AccountId = order.account.Id,
			Pricebook2Id = order.account.Price_Book__c,
			Status = 'Draft', 
			EffectiveDate = System.today(),
			EndDate = endDate,
			PoNumber = order.poNumber,
			Description = order.description,
			IgnoreMinimumAmount__c = false
		);

		// Create and insert new order items,
		// and we place the cakes separately
		isolatedOrders = new List<Order>();
		orderItems = new List<OrderItem>();
		isolatedItems = new List<OrderItem>();
		addonItems = new List<OrderItem>();
		addons = new List<List<ProductAddonWrapper>>();
		toPrepare = new List<Order>();

		for (ProductWrapper pw : order.products) {
			// we create the new item
			newItem = new OrderItem(
				PricebookentryId = pw.id,
				Delivery_Date__c = endDate,
				Quantity = pw.totalPieces,
				SKU__c = pw.sku,
				UnitPrice = pw.price,
				Description = pw.comments
			);

			if (pw.isSingleOrder) {
				// product with this flag must be placed 
				// in a separate order...
				isolatedOrders.add(newOrder.clone());
				isolatedItems.add(newItem);				
			} else
				// the rest of the items will all be part
				// of a single order
				orderItems.add(newItem);

			// we need to determine if addons are allowed
			// and selected, so we insert them properly
			if (pw.supportAddons && pw.addons != null && !pw.addons.isEmpty()) {
				// we keep the order item and the addon list
				// in lists with same index for future references
				addonItems.add(newItem);
				addons.add(pw.addons.values());
			}
		}

		// we need a savepoint to recover in case or dml exception
		sp = Database.setSavepoint();
		try {
			// if there are any non cake items, they will
			// all be included in the same order
			if (!orderItems.isEmpty()) {
				// we insert a new order to serve
				// as the parent of non items
				insert newOrder;
				toPrepare.add(newOrder);
				// we set the new order as parent of all
				// non cake items... and we insert the iterms
				insert setParentOrder(newOrder, orderItems);
			}

			// isolated products will have a single order per item..
			if (!isolatedItems.isEmpty()) {
				// we insert all item orders
				insert isolatedOrders;
				toPrepare.addAll(isolatedOrders);		
				// and those orders were placed in a list with the same
				// index as each corresponding item and we insert the cakes
				insert setParentOrder(isolatedOrders, isolatedItems);
			}

			// we now insert the selected addons, if any
			if (!addons.isEmpty()) {
				// the parent items have already been inserted.
				// we insert all addons
				insert getAddonItems(addonItems, addons);
				// some addond (products) will require the insertion
				// of extra items as they are not free and linked
				// to a products... we insert those, if any
				insert getProductAddonsItems(addonItems, addons);
			}

			// we need to place all orders in Prepared status
			ignoreMinimum = !isolatedOrders.isEmpty();
			// we override the status to comply with flow
			for (Order o : toPrepare) {
				o.Status = 'Prepared';
				o.IgnoreMinimumAmount__c = ignoreMinimum; 
			}
			// and we update the orders
			update toPrepare;

			// we return the first order: if new order was inserted,
			// we return that one, if not, we return the first of the cakes
			return newOrder.Id != null ? newOrder : 
				!isolatedOrders.isEmpty() && isolatedOrders[0].Id != null ? 
					isolatedOrders[0] : null;
		} catch (Exception ex) {
			Database.rollback(sp);
			throw ex;
		}
	}

	/**
	 * This method uses the Heap's algorythm to
	 * obtain all possible permutations of a list of 
	 * strings.
	 * @author Fernando Gomez
	 * @date June 6th, 2017
	 * @param n 		Index in the permutation tree
	 * @param origin 	original list of strings
	 * @param source	the permutation elements will be store here
	 */
	private static void permute(Integer n,
			List<String> origin, List<List<String>> source) {
		if (n == 1)
			source.add(origin.clone());
		else {
			for (Integer i = 0; i < n - 1; i++) {
				permute(n - 1, origin, source);
				swap(origin, Math.mod(n, 2) == 0 ? i : 0, n - 1);
			}
			permute(n - 1, origin, source);
		}
	}

	/**
	 * Swaps the values on two different indexes
	 * within the specified list
	 * @author Fernando Gomez
	 * @date June 6th, 2017
	 * @param origin
	 * @param index1
	 * @param index2
	 */
	private static void swap(List<String> origin, 
			Integer index1, Integer index2) {
		String temp = origin[index1];
		origin[index1] = origin[index2];
		origin[index2] = temp;
	}

	/**
	 * Ensures the list's size is not larger than the size
	 * specified. Basically, it removes all otuside the
	 * specified range, cutting the list to the size.
	 * @author Fernando Gomez
	 * @date June 6th, 2017
	 * @param origin
	 * @param size
	 */
	private static void chunk(List<String> origin, Integer size) {
		while (origin.size() > size)
			origin.remove(origin.size() - 1);
	}

	/**
	 * Inner helper to setup the preference values
	 * of each wrapper for the specified account
	 * @author Fernando Gomez
	 * @date June 6th, 2017
	 * @param accountId
	 * @param products
	 */
	private static void setPreferences(Id accountId,
			List<ProductWrapper> products,
			ProductPreferenceManager preferences) {
		for (ProductWrapper w : products)
			w.setPreferences(preferences.isFavorite(w.prodId),
				preferences.isShownByDefault(w.prodId));
	}

	/**
	 * Inner helper to set the parent
	 * order of all specified elements.
	 * The processed list is returned for chain calls.
	 * @author Fernando Gomez
	 * @date July 11th, 2017
	 * @param parentOrder
	 * @param items
	 */
	private static List<OrderItem> setParentOrder(
				Order parentOrder, List<OrderItem> items) {
		for (OrderItem item : items)
			item.OrderId = parentOrder.Id;

		return items;
	} 

	/**
	 * Inner helper to set the parent
	 * order of all specified elements as the order
	 * that is located in the same index as the item
	 * in the orders list. 
	 * The processed list is returned for chain calls.
	 * @author Fernando Gomez
	 * @date July 11th, 2017
	 * @param orders
	 * @param items
	 */
	private static List<OrderItem> setParentOrder(
				List<Order> orders, List<OrderItem> items) {
		Integer i = 0;
		for (OrderItem item : items)
			item.OrderId = orders[i++].Id;

		return items;
	}

	/**
	 * Inner helper to set the parent order item to all addons
	 * contained in the same index in the addons list. After process,
	 * a single list of prepared addons is returned for chain calls.
	 * @author Fernando Gomez
	 * @date July 11th, 2017
	 * @param items
	 * @param addons
	 */
	private static List<OrderProductAddOn__c> getAddonItems(
				List<OrderItem> items, List<List<ProductAddonWrapper>> addons) {
		Id itemId;	
		Integer i = 0;
		List<OrderProductAddOn__c> result = new List<OrderProductAddOn__c>();

		for (List<ProductAddonWrapper> asl : addons) 
			if (asl != null) {
				itemId = items[i++].Id; 
				for (ProductAddonWrapper a : asl)
					result.add(new OrderProductAddOn__c(
						OrderItem__c = itemId,
						AddOn__c = a.addonId,
						AddOnName__c = a.addonName,
						Product__c = a.productId,
						ProductName__c = a.productName,
						Quantity__c = a.quantity,
						UnitPrice__c = a.unitPrice,
						TotalPrice__c = a.totalPrice
					));
			}

		return result;
	}

	/**
	 * Inner helper that will determine if any of the addons is linked
	 * to a product. Addons linked to a product will be inserted as a single
	 * item, in the parent order. This method creates ans return any new
	 * item that needs to be inserted based on the addons.
	 * @author Fernando Gomez
	 * @date Aug 9th, 2017
	 * @param items
	 * @param addons
	 */
	private static List<OrderItem> getProductAddonsItems(
				List<OrderItem> items, List<List<ProductAddonWrapper>> addons) {
		OrderItem item;
		Integer i = 0;
		List<OrderItem> result = new List<OrderItem>();

		for (List<ProductAddonWrapper> asl : addons) 
			if (asl != null) {
				item = items[i++]; 
				for (ProductAddonWrapper a : asl) 
					// if a product name is specified
					if (a.productId != null)
						result.add(new OrderItem(
							OrderId = item.OrderId,
							Delivery_Date__c = item.Delivery_Date__c,
							PricebookentryId = a.pricebookEntryId,
							Quantity = a.quantity * item.Quantity,
							SKU__c = a.productSku,
							UnitPrice = a.unitPrice,
							Description = a.addonName
						));
			}

		return result;
	} 

	/**
	 * @author 		William Garcia
	 * @date		03/17/2017
	 * @description Account wrapper
	 */
	public class AccountWrapper {
		public Account account { get; set; }
		public List<ProductWrapper> products { get; set; }
		
		public AccountWrapper(Account acc, List<ProductWrapper> prods){
			this.account = acc;
			this.products = prods;
		}
	}

	/**
	 * @author Fernando Gomez
	 * @date Aug 9th, 2017
	 * @description wrapper conatinf information about product addons
	*/
	public class ProductAddonWrapper {
		public Id addonId { get; set; }
		public String addonName { get; set; }
		public Id pricebookEntryId { get; set; }
		public Id productId { get; set; }
		public String productSku { get; set; }
		public String productName { get; set; }
		public Decimal quantity { get; set; }
		public Decimal unitPrice { get; set; }
		public Decimal totalPrice { get; set; }
		public Boolean supportsMultiple { get; set; }
	}

	/**
	 * @author William Garcia
	 * @modified Fernando Gomez
	 * @date 03/17/2017
	 * @description 	Product wrapper
	*/
	public class ProductWrapper implements Comparable {
		public Id id { get; set; }
		public Id itemId { get; set; }
		public Id prodId { get; set; }
		public String name { get; set; }
		public String sku { get; set; }
		public String family { get; set; }
		public String subcategory { get; set; }
		public Decimal totalPieces { get; set; }
		public Decimal basePrice { get; set; }
		public Decimal price { get; set; }
		public String unitsPk { get; set; }

		public Boolean isFavorite { get; set; }
		public Boolean showByDefault { get; set; }
		public Boolean isSingleOrder { get; set; }
		public Boolean supportAddons { get; set; }
		public Boolean isOpen { get; set; }

		public Integer order { get; set; }
		public String comments { get; set; }
		public Map<Id, ProductAddonWrapper> addons { get; set; }

		public ProductWrapper(PricebookEntry pe, Integer order) {
			id = pe.Id;
			itemId = pe.Id;
			prodId = pe.Product2Id;
			name = pe.Product2.Name;
			sku = pe.Product2.SKU__c;
			family = pe.Product2.Family == null ? null :
					pe.Product2.Family.unescapeHtml4();
			subcategory = pe.Product2.Subcategory__c == null ? null :
					pe.Product2.Subcategory__c.unescapeHtml4();
			basePrice = pe.UnitPrice;
			price = pe.UnitPrice;
			unitsPk = pe.Product2.Units_Pk__c; 
			supportAddons = pe.Product2.SupportAddons__c;			
			isSingleOrder = pe.Product2.SingleOrder__c;

			this.order = order;
			totalPieces = null;
			isOpen = false;
			comments = null;
		}

		public void setPreferences(Boolean isFavorite,
				Boolean showByDefault) {
			this.isFavorite = isFavorite;
			this.showByDefault = showByDefault;
		}

		public Integer compareTo(Object obj) {
			ProductWrapper pw = ((ProductWrapper)obj);
			Integer c = name.compareTo(pw.name);
			return c != 0 ? c : order - pw.order;
		}
	}

	/**
	 * @author Fernando Gomez
	 * @date June 5th, 2017
	 * @description Wraps info on a lookup clauses
	 */
	public class WhereClauseStatement {
		public String fieldName { get; set; }
		public String value { get; set; }
		public String comparator  { get; set; }
		public Boolean useQuotes { get; set; }
	}

	/**
	 * @author       William Garcia
	 * @date         03/20/2017
	 * @description  Order wrapper
	 */
	public class OrderWrapper {
		public Account account { get; set; }
		public String poNumber { get; set; }
		public String description { get; set; }
		public Long deliveryDate { get; set; }
		public Boolean productsAdded { get; set; }
		public Decimal amount { get; set; }
		public Decimal dueToMinimal { get; set; }
		public List<ProductWrapper> products { get; set; }
		public Boolean completed { get; set; }
	}
}