global class SP_ChangeOrderStatus_Batch implements Database.Batchable<Order> {
	
	global Date EndDate;
	global String CurrentStatus;
	global String NewStatus;
	
	global SP_ChangeOrderStatus_Batch(Date endDate, String currentStatus, String newStatus) {
		this.EndDate = endDate;
		this.CurrentStatus = currentStatus;
		this.NewStatus = newStatus;
	}
	
	global Order[] start(Database.BatchableContext BC) {
		
		Order[] items = [select id from order
						 where status = :CurrentStatus and EndDate >= 2017-11-01 
						 							   and EndDate <= :EndDate
													   and Billing__c = null];

		return items;
	}

   	global void execute(Database.BatchableContext BC, List<Order> scope) {
		
		for(Order item: scope){
		    item.Status = NewStatus;
		}
		update scope;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}