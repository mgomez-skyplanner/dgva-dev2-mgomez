/**
 * Class to handle route printing methods
 * @author William Garcia
 * @date   05/23/2017
 */
public class RoutePrintingCtrl {
	
	/**
	 *  Get routes by delivery date
	 *  @author William Garcia
	 *  @date   05/23/2017
	 *  @param deliveryDate 
	 *  @return List<OrderBuilderHelper.RouteWrapper> Route wrapper list
	 */
	@RemoteAction  
	public static List<RoutePrintingHelper.RouteWrapper> getRoutes(String deliveryDate, String activationDate) {
		try {
			RoutePrintingException.customAssert(!String.isBlank(deliveryDate), 'Delivery date missing.');
			
			return RoutePrintingHelper.getRoutes(deliveryDate, activationDate);
		}
		catch(Exception ex) {
			throw new RoutePrintingException(ex, 'There was an error getting routes.');
		}
	}

	@RemoteAction  
	public static RoutePrintingHelper.RouteWrapper confirmPrinting(Id routeId, String deliveryDate, String activationDate) {
		return RoutePrintingHelper.confirmPrinting(routeId, deliveryDate, activationDate);
	}
}