/**
* <p>
* Version log:
* <ul>
* <li>1.00 10/10/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 10/10/2017
* @description 	 - This class represents a scheduler for batch StatementsGeneration_Batch
*/
 
global class StatementsGeneration_Scheduler {
	
    global void execute(SchedulableContext sc) {
        StatementsGeneration_Batch batch = new StatementsGeneration_Batch();
        Database.executeBatch(batch, 1);
    }
}