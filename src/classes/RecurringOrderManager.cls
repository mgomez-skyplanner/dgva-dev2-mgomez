/**
 * Class to manager Recurring orders
 * @author  William Garcia
 * @date    06/12/2017
 */
global class RecurringOrderManager {
    
    public List<Order> Orders {get;set;}

    private final String SUCCESS = 'SUCCESS';

    public RecurringOrderManager() {
        Orders = retrieveOrders();
    }

    public RecurringOrderManager(List<Order> ords) {
        Orders = ords;
        system.debug('Orders-->> ' + Orders);
    }

    /**
     * Create real order from recurring orders
     * @author  William Garcia
     * @date    05/09/2017
     */
    public void createOrders() {
    
        createOrders(Date.Today().Year(), Date.Today().Month(), Date.Today().Day());
    }
    
    /**
     * Create real order from recurring orders
     * @author  William Garcia
     * @date    05/09/2017
     */
    public void createOrders(Integer year, Integer month, Integer day) {
        if(!Orders.isEmpty()) {
            Map<Id, Order> orderMap = new Map<Id, Order>(Orders);

            /*
            Datetime today = Datetime.newInstance(year, month, day);
            Date dToday = today.date();
            Datetime prodDay = getProductionDay(today);
            Date dProdDay = prodDay.date();
            String strProdDay = prodDay.format('EEEE');
            */
            
            
            Date today = Date.newInstance(year, month, day);
            Date prodDay = getProductionDay(today);
            String strProdDay = Datetime.newInstance(prodDay, Time.newInstance(10, 0, 0, 0)).format('EEEE');

                
            System.debug('===> today: ' + today);
            System.debug('===> prodDay: ' + prodDay);
            System.debug('===> strProdDay: ' + strProdDay);
            
            
            if(isWorkingDay(today) || Test.isRunningTest()) {
                Map<String, List<OrderItem>> orderItemMap = new Map<String, List<OrderItem>>();
                Map<String, Order> recurrId_RegOrderMap = new Map<String, Order>();
                List<Order> rOrders = new List<Order>();
                List<OrderItem> rItems = new List<OrderItem>();
                Map<Integer, String> orderPosMap = new Map<Integer, String>();
                Set<Id> pbIds = new Set<Id>();
                Set<Id> prodIds = new Set<Id>();

                // Retrieve order items
                List<OrderItem> items = retrieveItems(orderMap.keySet(), strProdDay);
                system.debug('items-->> ' + items);

                for(Order order :orders)
                    pbIds.add(order.Account.Price_Book__c);
                system.debug('pbIds-->> ' + pbIds);

                // Fill a orderId->items map
                for(OrderItem item :items){
                    if(orderItemMap.containsKey(item.OrderId))
                        orderItemMap.get(item.OrderId).add(item);
                    else
                        orderItemMap.put(item.OrderId, new List<OrderItem>{item});  
                    // Fill a set of product ids
                    prodIds.add(item.PricebookEntry.Product2Id);
                }

                system.debug('orderItemMap-->> ' + orderItemMap);
                system.debug('prodIds-->> ' + prodIds);

                Map<Id, Map<Id, PriceBookEntry>> pbId_prodId_pe_map = getEntriesByPBsAndProds(pbIds, prodIds);
                system.debug('pbId_prodId_pe_map-->> ' + pbId_prodId_pe_map);

                // Create new orders
                for(String orderId : orderItemMap.keySet()) {
                    Order newOrder = cloneOrder(orderId, orderMap, today, prodDay);
                    recurrId_RegOrderMap.put(orderId, newOrder);
                    rOrders.add(newOrder);

                    // Save (new orders positions in the list -> recurring order id) MAP
                    orderPosMap.put(rOrders.size()-1, orderId);
                }
                Database.SaveResult[] insertOrdersResults = Database.insert(rOrders, false);
                system.debug('New Orders -->> ' + rOrders);

                // Build recurring orders to update regarding new orders insertion      
                Map<String, Order> ordersToUpdate = buildRecurrOrdersToUpdate(null, orderPosMap, insertOrdersResults, today);

                // Create new order items
                orderPosMap = new Map<Integer, String>();
                for(String orderId : orderItemMap.keySet()) {
                    String accPbId = orderMap.get(orderId).Account.Price_Book__c;
                    for(OrderItem item : orderItemMap.get(orderId)) {
                        OrderItem newItem = cloneOrderItem(orderId, item, 
                            recurrId_RegOrderMap, pbId_prodId_pe_map, accPbId, today, prodDay);
                        if(newItem != null) {
                            rItems.add(newItem);
                            // Save (new items positions in the list -> recurring order id) MAP
                            orderPosMap.put(rItems.size()-1, orderId);
                        }
                    }
                }
                
                Database.SaveResult[] insertItemsResults = Database.insert(rItems, false);
                system.debug('New Order Items -->> ' + rItems);

                // Build recurring orders to update regarding items insertion       
                ordersToUpdate = buildRecurrOrdersToUpdate(ordersToUpdate, orderPosMap, insertItemsResults, today);

                // Update recurring orders
                system.debug('ordersToUpdate-->> ' + ordersToUpdate);
                Database.update(ordersToUpdate.values(), false);

                // Change new orders status to prepared
                updateNewOrders(insertOrdersResults);   
            }
        }
    }


    /**
     * Update new order status
     * @author  William Garcia
     * @date    05/15/2017
     */
    private void updateNewOrders(Database.SaveResult[] srs) {
        Set<Id> ids = new Set<Id>();
        for(Database.SaveResult sr : srs)
            if(sr.isSuccess())
                ids.add(sr.getId());
            
        List<Order> orders = [SELECT Status FROM Order WHERE Id IN :ids];
        for(Order order : orders)
            order.Status = Constants.ORDER_STATUS_PREPARED;
        update orders;
    }

    /**
     * Clone order
     * @author  William Garcia
     * @date    05/09/2017
     */
    private Order cloneOrder(String orderId, Map<Id, Order> orderMap, 
        Date startDate, Date endDate) {
        Order n = orderMap.get(orderId).clone(false, true);
        n.Recurring_Order__c = orderId;
        n.Status = Constants.ORDER_STATUS_DRAFT;
        n.Type = Constants.ORDER_TYPE_REGULAR;
        n.EffectiveDate = startDate;
        n.EndDate = endDate;
        return n;
    }

    /**
     * Clone order items
     * @author  William Garcia
     * @date    05/09/2017
     */
    private OrderItem cloneOrderItem(String orderId, OrderItem item, 
        Map<String, Order> recurrId_RegOrderMap, Map<Id, Map<Id, PricebookEntry>> pbId_prodId_pe_map, 
        String accPbId, Date startDate, Date endDate) {
            OrderItem n = null;
            if(pbId_prodId_pe_map.containsKey(accPbId)) {
                Map<Id, PricebookEntry> prodId_Pe_Map = pbId_prodId_pe_map.get(accPbId);
                if(prodId_Pe_Map.containsKey(item.PricebookEntry.Product2Id)) {
                    PriceBookEntry pe = prodId_Pe_Map.get(item.PricebookEntry.Product2Id);
                    n = item.clone(false, true);
                    n.OrderId = recurrId_RegOrderMap.get(orderId).Id;
                    n.PriceBookEntryId = pe.Id;
                    n.UnitPrice = pe.UnitPrice;
                    n.ServiceDate = startDate;
                    n.Delivery_Date__c = endDate;
                    n.EndDate = endDate;
                }
            }
            return n;
    }

    /**
     * Get pricebook entries map
     * priceBookId-productId-List(priceBookEntryId)
     * @author  William Garcia
     * @date    06/12/2017
     */
    private Map<Id, Map<Id, PriceBookEntry>> getEntriesByPBsAndProds(Set<Id> pbIds, Set<Id> prodIds) {
        Map<Id, Map<Id, PriceBookEntry>> r = new Map<Id, Map<Id, PriceBookEntry>>();
        List<PricebookEntry> pbes = [SELECT Id, 
                                            Pricebook2Id, 
                                            Product2Id,
                                            UnitPrice
                                        FROM PricebookEntry 
                                        WHERE Pricebook2Id IN :pbIds 
                                        AND Product2Id IN :prodIds];
        for(PricebookEntry pe :pbes){
            if(r.containsKey(pe.Pricebook2Id))
                    r.get(pe.Pricebook2Id).put(pe.Product2Id, pe);
            else
                r.put(pe.Pricebook2Id, 
                    new Map<Id, PriceBookEntry>{pe.Product2Id => pe});
        }
        return r;
    }

    /**
     * Update recurring orders
     * @author  William Garcia
     * @date    05/09/2017
     */
    private Map<String, Order> buildRecurrOrdersToUpdate(Map<String, Order> ordersToUpdate, 
        Map<Integer, String> orderPosMap, Database.SaveResult[] results, DateTime dt) {
        
        system.debug('buildRecurrOrdersToUpdate ordersToUpdate-->> ' + ordersToUpdate);
        system.debug('buildRecurrOrdersToUpdate orderPosMap-->> ' + orderPosMap);
        system.debug('buildRecurrOrdersToUpdate results-->> ' + results);

        if(ordersToUpdate == null)
                ordersToUpdate = new Map<String, Order>();
        
        Integer index = 0;      
        for(Database.SaveResult sr : results) {
            String orderId = orderPosMap.get(index);
            Order order = null;

            if(ordersToUpdate.containsKey(orderId)) {
                order = ordersToUpdate.get(orderId);
            } else
                order = new Order(Id = orderId, 
                    Last_Recurring_Order_Generated_On__c = dt,
                    Last_Recurring_Order_Generation_Message__c = SUCCESS);
            
            if(!sr.isSuccess())
                for(Database.Error err : sr.getErrors())
                    setTextError(err, order);
                
            ordersToUpdate.put(order.Id, order);
            index ++;
        }

        return ordersToUpdate;
    }

    /**
     * Set order record error
     * @author  William Garcia
     * @date    05/10/2017
     */
    private void setTextError(Database.Error err, Order order) {
        // Reset if was success
        if(order.Last_Recurring_Order_Generation_Message__c == SUCCESS)
            order.Last_Recurring_Order_Generation_Message__c = '';
        // Set error
        order.Last_Recurring_Order_Generation_Message__c += 
            (String.isBlank(order.Last_Recurring_Order_Generation_Message__c)
            ? 'ERRORS:' : '') + 
            '\n - ' + err.getStatusCode() + ' | ' + err.getMessage();
    }

    /**
     * Get production day. 
     * This is because saturday process get Monday products
     * @author  William Garcia
     * @date    05/27/2017
     */
    public static DateTime getProductionDay(DateTime d) {
        return d.format('EEEE') == 'Saturday' ? 
            d.addDays(2) : d.addDays(1);
    }
    
     
    public static Date getProductionDay(Date d) {
        
        DateTime dTime = Datetime.newInstance(d, Time.newInstance(10, 0, 0, 0));
        
        System.debug('===> ProdDay Param (Today to Datetime): '   + dTime);
        System.debug('===> ProdDay Param (EEEE format): '         + dTime.format('EEEE'));
        
        if(dTime.format('EEEE') == 'Saturday')
            return d.addDays(2);
        
        return d.addDays(1);
        
    }

    /**
     * know if is working day
     * @author  William Garcia
     * @date    05/27/2017
     */
    public static Boolean isWorkingDay(DateTime d) {
        return d.format('EEEE') != 'Sunday'; 
    }
    
    public static Boolean isWorkingDay(Date d){
        return Datetime.newInstance(d, Time.newInstance(10, 0, 0, 0)).format('EEEE') != 'Sunday'; 
    }

    /**
     * Retrieve recurring orders query locator
     * @author  William Garcia
     * @date    05/09/2017
     * @return  Order list
     */
    public static Database.QueryLocator retrieveOrdersQL() {
        return Database.getQueryLocator([
            SELECT Id,
                    Name,
                    Pricebook2Id,
                    AccountId,
                    BillToContactId,
                    BillingAddress,
                    CompanyAuthorizedById,
                    CompanyAuthorizedDate,
                    ContractId,
                    Description,
                    OpportunityId,
                    CustomerAuthorizedById,
                    CustomerAuthorizedDate,
                    OwnerId,
                    OrderReferenceNumber,
                    PoNumber,
                    Route__c,
                    Route_No__c,
                    SendConfirmationEmailTo__c,
                    ShipToContactId,
                    ShippingAddress,
                    Account.Price_Book__c
                FROM Order
                WHERE Type = :Constants.ORDER_TYPE_RECURRING
                AND Status != :Constants.ORDER_STATUS_CANCELLED
        ]);
    }

    /**
     * Retrieve recurring orders query locator
     * @author  William Garcia
     * @date    05/09/2017
     * @return  Order list
     */
    public List<Order> retrieveOrders() {
        return [SELECT Id,
                    Name,
                    Pricebook2Id,
                    AccountId,
                    BillToContactId,
                    BillingAddress,
                    CompanyAuthorizedById,
                    CompanyAuthorizedDate,
                    ContractId,
                    Description,
                    OpportunityId,
                    CustomerAuthorizedById,
                    CustomerAuthorizedDate,
                    OwnerId,
                    OrderReferenceNumber,
                    PoNumber,
                    Route__c,
                    Route_No__c,
                    SendConfirmationEmailTo__c,
                    ShipToContactId,
                    ShippingAddress,
                    Account.Price_Book__c
                FROM Order
                WHERE Type = :Constants.ORDER_TYPE_RECURRING
                AND Status != :Constants.ORDER_STATUS_CANCELLED];
    }

    /**
     * Retrieve order items that delivery days include some day
     * @author  William Garcia
     * @date    05/09/2017
     * @param  orderIds Order ids
     * @param  day      Delivery day
     * @return          Order item list
     */
    private static List<OrderItem> retrieveItems(Set<Id> orderIds, String day) {
        system.debug('retrieveItems orderIds-->> ' + orderIds);
        system.debug('retrieveItems day:-->> ' + day);
        return [SELECT Id,
                       OrderId,
                       Billing_City__c,
                       Billing_Country__c,
                       Billing_State__c,
                       Billing_Street__c,
                       Billing_Postal_Code__c,
                       Description,
                       No_of_Full_Pans__c,
                       PricebookEntryId,
                       PricebookEntry.Product2Id,
                       PricebookEntry.Pricebook2Id,
                       Quantity,
                       QuoteLineItemId,
                       Reminder__c,
                       SKU__c,
                       Slice__c,
                       Total_Pieces__c,
                       UnitPrice
                    FROM OrderItem
                    WHERE OrderId IN :orderIds
                    AND Delivery_Days__c includes (:day)];
    }
}