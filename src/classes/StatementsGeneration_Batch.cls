/**
* <p>
* Version log:
* <ul>
* <li>1.00 10/10/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 10/10/2017
* @description 	 - Generates Statements for Accounts if this batch is executed on a date = Account.Next_Statement_Date__c
* @version 
* 	1.01		 10/26/2017 William Garcia: Statement develoment, continuation...
*/
 
public with sharing class StatementsGeneration_Batch implements Database.Batchable<sObject>, Database.Stateful {

	public List<Statement__c> StatementsGenerated { get; set; }
	public List<String> StatementsGeneratedIds { get; set; }

	public List<Id> AccountIds { get; set; }
	public StatementManager_Helper.StatementType StatementType { get; set; }
	public Boolean SendEmail { get; set; }
	public Date StartDate { get; set; }
	public Date EndDate { get; set; }
	public Boolean InformFinish { get; set; }

	/**
	 * Used normally by schedule class to generate accouhnt statement
	 * @author Marcel Gómez
	 */
	public StatementsGeneration_Batch() {
		this.AccountIds = new List<Id>();
		this.StatementsGenerated = new List<Statement__c>();
		this.StatementsGeneratedIds = new List<String>();
		this.StatementType = NULL;
		this.SendEmail = true;
		this.InformFinish = false;
		this.EndDate = Date.today();
	}

	/**
	 * Contructor used normally from StatementManager tool.
	 * Where if you do not select some account and set some date
	 * period, you will generate accounts statement (Asyn) for that period
	 * @author William Garcia
	 * @date   10/26/2017
	 * @param  accountIds Account ids to generate statements
	 * @param  sType      Statement type
	 * @param  sendEmail  If you want to send 
	 *                    statement by email to paperLess accounts
	 * @param  startDate  Startement start date
	 * @param  endDate    Startement end date
	 */
	public StatementsGeneration_Batch(List<Id> accountIds, StatementManager_Helper.StatementType sType, 
		Boolean sendEmail, Date startDate, Date endDate, Boolean informFinish) {
			this.AccountIds = accountIds;
			this.StatementType = sType;
			this.SendEmail = sendEmail;
			this.StartDate = startDate;
			this.EndDate = endDate;
			this.InformFinish = informFinish;
			this.StatementsGenerated = new List<Statement__c>();
			this.StatementsGeneratedIds = new List<String>();

		System.debug('StatementsGeneration_Batch-contructor-->> ' + this);
	}

	public Database.QueryLocator start(Database.BatchableContext BC) {
		if(!this.AccountIds.isEmpty())
			return Database.getQueryLocator(
				[SELECT Id, 
						Default_Statement_Type__c 
					FROM Account 
					WHERE Id IN: this.AccountIds
					AND Level__c != 'Inactive']);

		return Database.getQueryLocator(
			[SELECT Id, 
					Default_Statement_Type__c 
					FROM Account 
					WHERE Next_Statement_Date__c =: Date.today()
					AND Level__c != 'Inactive']);
	}

   	public void execute(Database.BatchableContext bc, List<Account> scope) {
   		List<Id> accountIds_outstanding = new List<Id>();
   		List<Id> accountIds_activity = new List<Id>();

   		for(Account account : scope) {
   			if(this.StatementType == NULL){
	   			if(account.Default_Statement_Type__c == 'Activity')
	   				accountIds_activity.add(account.Id);
	   			else
	   				accountIds_outstanding.add(account.Id);
   			} else {
	   			if(this.StatementType == StatementManager_Helper.StatementType.Activity)
	   				accountIds_activity.add(account.Id);
	   			else
	   				accountIds_outstanding.add(account.Id);
   			}
   		}

   		System.debug('accountIds_activity-->> ' + accountIds_activity);
   		System.debug('accountIds_outstanding-->> ' + accountIds_outstanding);

   		if(!accountIds_activity.isEmpty()) {
			StatementBuilder_Activity builderActivity = new StatementBuilder_Activity(
				accountIds_activity, StartDate, EndDate, SendEmail);
			builderActivity.GeneratedFromBatch = true;
			
			List<Statement__c> activityStatements_List = builderActivity.generateActivityStatement(true);
			this.StatementsGenerated.addAll(activityStatements_List);
		}
		
   		if(!accountIds_outstanding.isEmpty()) {
			StatementBuilder_Outstanding builderOutstanding = new StatementBuilder_Outstanding(
				accountIds_outstanding, EndDate, SendEmail);
			builderOutstanding.GeneratedFromBatch = true;
			
			List<Statement__c> outstandingStatements_List = builderOutstanding.generateOutStandingStatement(true);
			this.StatementsGenerated.addAll(outstandingStatements_List);
		}

		System.debug('StatementsGenerated-->> ' + StatementsGenerated);
	}
	
	public void finish(Database.BatchableContext bc) {
		for(Statement__c st : this.StatementsGenerated)
			StatementsGeneratedIds.add(st.Id);

		// After generate all statements...generate statements attachments(pdf) 
		// asyn by batch to avoid hit SF limits.
		StatementAttachmentGeneration_Batch batch = new StatementAttachmentGeneration_Batch(
			StatementsGeneratedIds, StatementType, StartDate, EndDate, InformFinish);
        Database.executeBatch(batch, 1);

		informFinish();
	}

	/**
	 * Inform user in charge that the batch finished 
	 * and what statements were generated
	 * @author William Garcia
	 * @date   10/26/2017
	 */
	private void informFinish() {
		System.debug('informFinish-----');
		
		List<EmailTemplate> etList = [SELECT Id,
                                             Name,
                                             Subject,
                                             HtmlValue                                                               
                                      FROM EmailTemplate
                                      WHERE Name = 'Statements Generation Batch Finished'];

		if(InformFinish && !StatementsGeneratedIds.isEmpty() && !etList.isEmpty()) {
			EmailTemplate et = etList[0];

			System.debug('EmailTemplate-->> ' + et);

			Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
			message.toAddresses = new String[] { UserInfo.getUserEmail() };
			message.subject = et.Subject;
			message.htmlBody = getFinishNotifCont(et.HtmlValue);
			
			Messaging.SendEmailResult[] results = Messaging.sendEmail(
				new List<Messaging.SingleEmailMessage> {message});

			if (results[0].success)
			    System.debug('informFinish: The email was sent successfully.');
			else
			    System.debug('informFinish: The email failed to send: ' + results[0].errors[0].message);
		}
	}

	/**
	 * Get str batch date period
	 * @author William Garcia
	 * @date   10/26/2017
	 */
	private String getStrPeriod() {
		String p = '';

		if(StartDate == null)
			p = EndDate.format();
		else
			p = (StartDate.format() + ' / ' + EndDate.format());	
		
		return p.remove('00:00:00');
	}

	/**
	 * Build finish notification email body content
	 * @author William Garcia
	 * @date   10/26/2017
	 */
	private String getFinishNotifCont(String cont) {
		return cont.replace('[TYPE]', String.valueOf(this.StatementType)).
				replace('[DATE]', Datetime.now().format()).
				replace('[PERIOD]', getStrPeriod()).
				replace('[CNT]', String.valueOf(StatementsGeneratedIds.size())).
				replace('[STATEMENT_MANAGEMENT_LINK]', '<a href="' + 
					URL.getSalesforceBaseUrl().toExternalForm() +'/apex/statementmanagementPage'
					+ '">Statements Manager</a>');
	}
}