/**
 * Batch to generate statement pdfs
 * @author    	 William Garcia
 * @date  	     10/31/2017
 */
public class StatementAttachmentGeneration_Batch implements Database.Batchable<sObject>, Database.Stateful {
	
	public List<String> StatementIds {get; set;}
	public Date StartDate {get; set;}
	public Date EndDate {get; set;}
	public StatementManager_Helper.StatementType StatementType {get; set;}
	public Boolean InformFinish { get; set; }

	public StatementAttachmentGeneration_Batch(List<String> sIds, 
		StatementManager_Helper.StatementType sType, Date startDate, 
		Date endDate, Boolean informFinish) {
			this.StatementIds = sIds;
			this.StatementType = sType;
			this.StartDate = startDate;
			this.EndDate = endDate;
			this.InformFinish = informFinish;
	}

	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(
			[SELECT Id, 
					Type__c, 
					Period_Start_Date__c, 
					Period_End_Date__c 
				FROM Statement__c 
				WHERE Id IN: StatementIds]);
	}

	public void execute(Database.BatchableContext bc, List<Statement__c> statements) {
   		StatementManager_Helper.attachPDFToStatements(statements);
	}

	public void finish(Database.BatchableContext bc) {}
	
}