/**
 * Test class for OrderManager.
 * @author Fernando Gomez, SkyPlanner LLC
 * @date May 9th, 2017
 * @version 1.0.0 Initial version.
 */
@isTest
private class OrderManagerTest {
	/**
	 * Setup test data
	 * @author 
	 * @date   03/21/2017
	 * 
	 */
	@testSetup
	public static void setup() {
		Route__c route1;
		Account acc1;
		Product2 prod1;
		PricebookEntry pbe1;
		Order order1;
		OrderItem orderItem1;
		
		route1 = new Route__c(
			Name = 'route1'
		);
		insert route1;

		acc1 = new Account(
			Name = 'Account1',
			BillingCountry = 'C1',
			BillingCity = 'C2',
			Total_Balance__c = 50,
			Route__c = route1.Id
		);
		insert acc1;

		prod1 = new Product2(
			Name = 'Product1',
			IsActive = true,
			Price__c = 10,
			Units_Pk__c = 'DOZ',
			SKU__c = '123'
		);
		insert prod1;

		pbe1 = [
			SELECT Id 
			FROM PricebookEntry 
			WHERE Pricebook2Id = :[
				SELECT Price_Book__c 
				FROM Account 
				WHERE Id = :acc1.Id][0].Price_Book__c 
			AND Product2Id = :prod1.Id
		];

		order1 = new Order (
			AccountId = acc1.Id,
			Status = 'Draft', 
			EffectiveDate = System.today(),
			EndDate = System.today()
		);
		insert order1;

		orderItem1 = new OrderItem(
			EndDate = System.today(),
			OrderId = order1.Id,
			PricebookentryId = pbe1.Id,
			Quantity = 2,
			Total_Pieces__c = 2,
			SKU__c = '123',
			UnitPrice = 10
		);
		insert orderItem1;
	}

	/**
	 * Test:
	 * global void activate(OrderInformation info)
	 * global void activate(Id orderId)
	 * @author Fernando Gomez, SkyPlanner LLC
	 * @date May 9th, 2017
	 */
	@isTest 
	static void activate() {
		Order ord;
		OrderManager m;

		ord = [
			SELECT Id
			FROM Order
			WHERE Account.Name = 'Account1'
		];
		ord.Status = 'Prepared';
		update ord;

		m = new OrderManager();
		m.activate(ord.Id);

		System.assertEquals('Activated', [
				SELECT Status
				FROM Order
				WHERE Id = :ord.Id
			].Status);
	}

	/**
	 * Test:
	 * global OrderInformation getById(Id orderId)
	 * @author Fernando Gomez, SkyPlanner LLC
	 * @date May 9th, 2017
	 */
	@isTest 
	static void getById() {
		Order ord;
		OrderManager m;

		ord = [
			SELECT Id
			FROM Order
			WHERE Account.Name = 'Account1'
		];

		m = new OrderManager();
		System.assertEquals(ord.Id, m.getById(ord.Id).orderId);
	}

	/**
	 * Test for methods OrderManager.fetchByActivationDate, OrderManager.fetchByRouteActivationDate and OrderManager.fetchByRoute
	 * @author Marcel Gomez
	 * @date 09/17/2017
	 */
	@isTest 
	static void test_fetchByActivationDate() {
		Test.startTest();

		Order order = [SELECT
						Id,
						EndDate,
						ActivatedDate,
						Route__c
					FROM 
						Order
					WHERE
						Account.Name = 'Account1'];

		order.Status = 'Prepared';
		update order;

		Test.stopTest();

		OrderManager m = new OrderManager();
		List<Order> ordersList = m.fetchByActivationDate(order.EndDate, order.ActivatedDate);

		System.assertEquals(1, ordersList.size());
		System.assertEquals(order.Id, ordersList[0].Id);

		ordersList = m.fetchByRouteActivationDate(order.Route__c, order.EndDate, order.ActivatedDate);

		System.assertEquals(1, ordersList.size());
		System.assertEquals(order.Id, ordersList[0].Id);

		ordersList = m.fetchByRoute(order.Route__c, order.EndDate);

		System.assertEquals(1, ordersList.size());
		System.assertEquals(order.Id, ordersList[0].Id);
	}
}