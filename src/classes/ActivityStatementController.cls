/**
* <p>
* Version log:
* <ul>
* <li>1.00 10/04/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 10/04/2017
* @description 	 - Represents the controller for VF page ActivityStatementPage
*/
 
global with sharing class ActivityStatementController {

	global Decimal ItemsPerPage1 { get; set; }
	global Decimal ItemsPerPage { get; set; }

	private String targetStatementId;
	global String StatementId { 
		get { return this.targetStatementId; }
		set { 
			this.targetStatementId = value;

			system.debug('---------------------- ActivityStatementController.StatementId set property');
			this.InitByStatement(this.targetStatementId);
		}
	}

	global List<ActivityStatement_Wrapper> WrappersList { get; private set; }

	public List<ActivityStatement_Wrapper> VisibleWrappersList { 
		get {
			List<ActivityStatement_Wrapper> result = new List<ActivityStatement_Wrapper>();
			for(ActivityStatement_Wrapper item: WrappersList) {
				//if(item.BalanceDue > 0)
					result.add(item);
			}
			return result;
		}
	}

	public ActivityStatementController() {
		Init();
	}

	public ActivityStatementController(ApexPages.StandardController stdController) {
		Init();
	}

	private void Init() {		
		Map<String, String> params;

		try{
			//initialize page settings
			Statements_Settings__c settings = Statements_Settings__c.getOrgDefaults();
			this.ItemsPerPage1 = settings.Items_Per_Page1_Activity__c;
			this.ItemsPerPage = settings.Items_Per_Page_Activity__c;

			//initialize Wrappers List
			this.WrappersList = new List<ActivityStatement_Wrapper>();

			//get the query string parameters
			params = ApexPages.currentPage().getParameters();

			system.debug('---------------------- Page params:');
			system.debug(params);

			//parse query string parameters
			StatementManager_Helper.StatementManagement_Filters queryStringFilters = StatementManager_Helper.ParsePageParams(params);

			system.debug('---------------------- queryStringFilters:');
			system.debug(queryStringFilters);

			//decide how to generate the PDF based on the page parameters
			if(!String.isBlank(queryStringFilters.StatementId) || !String.isBlank(queryStringFilters.AccountId) || !String.isBlank(queryStringFilters.RouteId)) {
				if(!String.isBlank(queryStringFilters.AccountId)) {
					system.debug('---------------------- InitByAccount');

					InitByAccount(queryStringFilters.AccountId, queryStringFilters.StartDate, queryStringFilters.EndDate);
				}
				else if(!String.isBlank(queryStringFilters.StatementId)) {
					system.debug('---------------------- InitByStatement');

					InitByStatement(queryStringFilters.StatementId);
				}
				else {
					system.debug('---------------------- InitByRoute');
					
					InitByRoute(queryStringFilters);
				}

				CompletePageItems();
			}
		}
		catch(Exception e) {
			system.debug('-------------------------------------- OutstandingStatementController ERROR !!! ' + e.getMessage());
			system.debug('-------------------------------------- OutstandingStatementController params: ');
			system.debug(params);
		}
	}

	private void InitByAccount(String accountId, Date startDate, Date endDate) {
		if(startDate == NULL) {
			Map<Id, Statement__c> previousStatements = StatementManager_Helper.GetPreviousStatement(new List<Id> {accountId}, startDate);

			if(previousStatements.get(accountId) != NULL)
				startDate = previousStatements.get(accountId).Period_End_Date__c;
			else
				startDate = Date.today().addYears(-10);//emulate beginning of times
		}

		ActivityStatement_Wrapper wrapper = new ActivityStatement_Wrapper(accountId, startDate, endDate);
		this.WrappersList.add(wrapper);
	}

	private void InitByRoute(StatementManager_Helper.StatementManagement_Filters queryStringFilters) {
		List<Account> accountsInRoute = StatementManager_Helper.LoadAccounts(queryStringFilters);

		List<Id> accountIds = new List<Id>();
		for(Account account : accountsInRoute) {
			accountIds.add(account.Id);
		}

		StatementBuilder_Activity builder = new StatementBuilder_Activity(accountIds, queryStringFilters.StartDate, queryStringFilters.EndDate, true);
		builder.generateActivityStatement(queryStringFilters.GenerateStatements);

		for(Account account : accountsInRoute) {
			Boolean includeAccount = !queryStringFilters.OnlyNoEmailReady || account.AcctSeed__Billing_Contact__c == NULL;

			if(includeAccount) {
				ActivityStatement_Wrapper wrapper = new ActivityStatement_Wrapper();
				
				wrapper.Account 		= account;
				wrapper.StatementDate 	= Date.today();
				wrapper.StatementStartDate 	= queryStringFilters.StartDate;
				wrapper.StatementEndDate 	= queryStringFilters.EndDate;
				wrapper.BalanceDue 		= builder.Balance.get(account.Id);
				wrapper.PurchaseActivity= builder.PurchaseActivity.get(account.Id);


				wrapper.Rows 			= builder.LineItems.get(account.Id);

				this.WrappersList.add(wrapper);
			}
		}
	}

	private void InitByStatement(String statementId) {
		system.debug('----------------- ActivityStatementController InitByStatement statementId = ' + statementId);

		if(!String.isBlank(statementId) && WrappersList.size() == 0) {
			Statement__c statement = [SELECT Id, Account__c, Period_Start_Date__c, Period_End_Date__c FROM Statement__c WHERE Id =: statementId];

			ActivityStatement_Wrapper wrapper = new ActivityStatement_Wrapper(statement.Account__c, statement.Period_Start_Date__c, statement.Period_End_Date__c);
			this.WrappersList.add(wrapper);

			CompletePageItems();
		}
	}

	private void CompletePageItems() {
		if(this.ItemsPerPage == NULL)
			return;//review because at this point properties ItemsPerPage and ItemsPerPage1 are not set yet

		for(ActivityStatement_Wrapper wrapper : this.WrappersList) {
			Integer rowsAmount = wrapper.Rows == NULL ? 1 : wrapper.Rows.size();

			if(wrapper.Rows == NULL)
				wrapper.Rows = new List<Statement_Row>();

			Integer module = Math.mod(rowsAmount, (Integer)this.ItemsPerPage);

			if(module > 0) {
				for(Integer i = 0; i < module; i++) {
					Statement_Row row = new Statement_Row();
					row.Reference = '&nbsp;';

					wrapper.Rows.add(row);
				}
			}
		}
	}

	private class ActivityStatementControllerException extends Exception { }

	public class ActivityStatement_Wrapper {
		public Date StatementDate { get; set; }
		public Date StatementStartDate { get; set; }
		public Date StatementEndDate { get; set; }

		public Decimal BalanceDue { get; set; }
		public Decimal PurchaseActivity { get; set; }
		public Account Account { get; set; }

		public List<Statement_Row> Rows { get; set; }

		public ActivityStatement_Wrapper() {}

		public ActivityStatement_Wrapper(Id accountId, Date startDate, Date endDate) {
			this.Account = [SELECT 
								Id,
								Name,
								Phone,
								Route__r.Name,
								ShippingStreet,
								ShippingCity,
								ShippingState,
								ShippingPostalCode,
								ShippingCountry
							FROM
								Account
							WHERE
								Id =: accountId
							];


			StatementBuilder_Activity builder = new StatementBuilder_Activity(new List<Id> { this.Account.Id }, startDate, endDate, true);
			builder.generateActivityStatement(false);

			this.StatementDate 		= Date.today();
			this.StatementStartDate = startDate;
			this.StatementEndDate 	= endDate;

			this.StatementStartDate = builder.StatementStartDate;
			this.StatementEndDate 	= builder.StatementEndDate;
			this.BalanceDue 		= builder.Balance.get(this.Account.Id);
			this.PurchaseActivity 	= builder.PurchaseActivity.get(this.Account.Id);
			this.Rows 			= builder.LineItems.get(this.Account.Id);
		}
	}
}