/**
* <p> 
* Version log:
* <ul>
* <li>1.00 10/16/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 10/16/2017
* @description 	 - Test class for classes StatementBuilder_Outstanding, StatementBuilder_Activity and StatementManager_Helper
*/

@isTest
public with sharing class StatementBuilder_Test {

	@testSetup
	static void setup() {
		TestClass_Utility.createCustomSettingsData();

		Route__c route 	= TestClass_Utility.createRoute('Test Route');
		insert route;

		Account account = TestClass_Utility.createAccount('Test Account', route);
		insert account;

		Opportunity opp = TestClass_Utility.createOpportunity(account);
		insert opp;

		Order order 			= TestClass_Utility.createOrder(account, opp, 'Draft');
		insert order;

		Product2 product1 		= TestClass_Utility.createProduct('Test Product 1');
		insert product1;

		Id standardPriceBookId = Test.getStandardPricebookId();
		PricebookEntry pbe 		= [SELECT Id FROM PricebookEntry WHERE Product2Id =: product1.Id AND PriceBook2Id =: standardPriceBookId];

		OrderItem orderProduct1 = TestClass_Utility.createOrderProduct(order, pbe);
		insert orderProduct1;

		TestClass_Utility.CreateDefaultDataForBillings(account, order, product1);

		//Mandatory setting for Accounting Seed
		AcctSeed__Billing_Format__c pdfFormat = TestClass_Utility.createPDFFormat('Test PDF Format', 'Billing', 'AccountingHome', 'OrderConfirmation');
		insert pdfFormat;

		order.Status = 'Prepared';
		update order;
		order.Status = 'Activated';
		update order;

		//reload order
		BillingManager.LoadCustomSettings();
		order = BillingManager.ReloadOrder(order.Id);

		AcctSeed__Billing__c billing = BillingManager.GenerateBillingFromOrder(order, true);
	}

	private static Account LoadAccount() {
		return [SELECT 
					Id,
					Statement_Frequency__c,
					Statement_Day__c,
					(
						SELECT
							Id,
							Balance__c,
							CreatedDate,
							Period_End_Date__c,
							Period_Start_Date__c,
							Type__c
						FROM
							Statements__r
					)
				FROM
					Account];
	}
	
	@isTest
	static void test_getStatementsMethods() {
		Test.startTest();

		Account account = LoadAccount();

		List<Statement__c> statements = TestClass_Utility.generateStatementsForAccount(account.Id, StatementManager_Helper.StatementType.Outstanding);
		insert statements;

		Test.stopTest();

		Date startDate = statements[0].Period_Start_Date__c;
		Date createdDate = (Date)statements[0].CreatedDate;
		Date endDate = Date.today().addYears(100);

		system.debug('------------- start date: ' + startDate);
		system.debug('------------- end date: ' + endDate);

		Map<Id, List<Statement__c>> all = StatementManager_Helper.GetAllStatements(new List<Id> { account.Id });
		system.assertEquals(statements.size(), all.get(account.Id).size());

		Map<Id, Statement__c> previous = StatementManager_Helper.GetPreviousStatement(new List<Id> { account.Id }, createdDate);
		system.assert(previous.get(account.Id) != NULL);

		Map<Id, List<Statement__c>> outstanding = StatementManager_Helper.GetStatementsBy_Type(new List<Id> { account.Id }, StatementManager_Helper.StatementType.Outstanding);
		system.assertEquals(all.get(account.Id).size(), outstanding.get(account.Id).size());

		Map<Id, List<Statement__c>> inPeriod = StatementManager_Helper.GetStatementsBy_Period(new List<Id> { account.Id }, startDate, endDate);
		system.assertEquals(all.get(account.Id).size(), inPeriod.get(account.Id).size());

		Map<Id, List<Statement__c>> typeInPeriod = StatementManager_Helper.GetStatementsBy_Type_Period(new List<Id> { account.Id }, StatementManager_Helper.StatementType.Outstanding, startDate, endDate);
		system.assertEquals(all.get(account.Id).size(), typeInPeriod.get(account.Id).size());


		//Test outstanding controllers
		Test.setCurrentPageReference(Page.OutstandingStatementPage);
		System.currentPageReference().getParameters().put('id', account.Id);
		System.currentPageReference().getParameters().put('rid', account.Id);
		System.currentPageReference().getParameters().put('st', 'Outstanding');
		System.currentPageReference().getParameters().put('edate', Date.today().year() + '/' + Date.today().month() + '/' + Date.today().day());

		ApexPages.StandardController sc = new ApexPages.StandardController(account);
		OutstandingStatementController oc = new OutstandingStatementController(sc);
	}

}