@isTest
private class SP_ChangeOrderStatus_Test
{

	@testSetup
	static void setup() {
		TestClass_Utility.createCustomSettingsData();

		Route__c route 	= TestClass_Utility.createRoute('Test Route');
		insert route;

		Account account = TestClass_Utility.createAccount('Test Account', route);
		insert account;

		Opportunity opp = TestClass_Utility.createOpportunity(account);
		insert opp;

		Order order 			= TestClass_Utility.createOrder(account, opp, 'Draft');
		order.EndDate = Date.today().addDays(1);
		insert order;

		Product2 product1 		= TestClass_Utility.createProduct('Test Product 1');
		insert product1;

		Id standardPriceBookId = Test.getStandardPricebookId();
		PricebookEntry pbe 		= [SELECT Id FROM PricebookEntry WHERE Product2Id =: product1.Id AND PriceBook2Id =: standardPriceBookId];

		OrderController.OrderProductRemover_Wrapper oprWrapper = OrderController.getOrderProducts(order.Id);
		system.assertEquals(OrderController.Error_NoProductFoundInOrder, oprWrapper.Error);

		OrderItem orderProduct1 = TestClass_Utility.createOrderProduct(order, pbe);
		insert orderProduct1;

		TestClass_Utility.CreateDefaultDataForBillings(account, order, product1);

		order.Status = 'Prepared';
		update order;

		order.Status = 'Activated';
		update order;
	}

	@isTest
	static void itShould()
	{
		Test.startTest();
		SP_ChangeOrderStatus_Schedule s = new SP_ChangeOrderStatus_Schedule();
		s.endDate = system.today().addDays(1);
		s.execute(null);
		Test.stopTest();

		Order order = [SELECT Status, Billing__r.AcctSeed__Status__c FROM Order];
		system.assertEquals('Delivered', order.Status);		
		system.assert(order.Billing__c != null, 'Order should have a billling.');		
		system.assertEquals('Posted', order.Billing__r.AcctSeed__Status__c, 'Billing should be Posted.');		
	}
}