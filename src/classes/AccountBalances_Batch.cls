/**
* <p>
* Version log:
* <ul>
* <li>1.00 06/28/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 06/28/2017
* @description 	 - This class represents a batch to calculate Balance and Past Due of Accounts
*/
 
global class AccountBalances_Batch implements Database.Batchable<sObject> {

 	global Database.QueryLocator start(Database.BatchableContext BC) {	 	
 		return Database.getQueryLocator('SELECT Id, ' + 
 		                                ' (SELECT Id FROM Orders) ' +
 										'FROM Account');
 	}

 	global void execute(Database.BatchableContext BC, List<Account> accounts) {
 		Set<Id> accountIds = new Set<Id>();
 		for(Account account : accounts) {
 			accountIds.add(account.Id);
 		}

 		AccountBalances_Utility.UpdateAccountBalances(accountIds);
 	}

  	global void finish(Database.BatchableContext BC) {

  	}
}