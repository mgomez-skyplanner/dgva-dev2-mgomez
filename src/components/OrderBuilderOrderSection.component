<!--
/**
 * Order builder order section component
 * @author	William Garcia
 * @date 	03/16/2017
 */
-->
<apex:component layout="none">
<div ng-style="{'flex-grow':1}"
	ng-class="{'ob-active': navigation.currentSection.name == 'orders'}"
	class="slds-is-relative">
	<div ng-style="{'position':'absolute',
			'top':0, 'bottom':0, 'left':0, 'right':0,
			'max-width': '100%'}"
		xmlns="http://www.w3.org/2000/svg" 
		xmlns:xlink="http://www.w3.org/1999/xlink" lang="en"
		class="slds-scrollable--y">
		<div ng-if="order.products.length">
			<div class="slds-grid slds-border--bottom
					slds-text-title--caps">
				<div class="slds-p-horizontal--x-small
						slds-p-vertical--x-small
						slds-size--1-of-12"
					title="Favorite?">&nbsp;</div>
				<div class="slds-p-horizontal--x-small
						slds-p-vertical--x-small
						slds-size--2-of-12"
					title="SKU">SKU</div>
				<div class="slds-p-horizontal--x-small
						slds-p-vertical--x-small
						slds-size--3-of-12"
					title="Product List">Product List</div>
				<div class="slds-p-horizontal--x-small
						slds-p-vertical--x-small
						slds-text-align--right
						slds-size--2-of-12" 
					title="Price">Price</div>
				<div class="slds-p-horizontal--x-small
						slds-p-vertical--x-small
						slds-text-align--right
						slds-size--1-of-12" 
					title="Qty">Units/Pk</div>
				<div class="slds-p-horizontal--x-small
						slds-p-vertical--x-small
						slds-text-align--right
						slds-size--1-of-12" 
					title="Qty">Qty</div>
				<div class="slds-p-horizontal--x-small
						slds-p-vertical--x-small
						slds-size--2-of-12" 
					title="Action"></div>
			</div>
			<div ng-repeat="prod in order.products"
				ng-click="navigation.selectSpecific('orders', $index)"
				ng-activate="navigation.currentSection.name == 'orders' &&
					navigation.currentSection.itemIndex == $index"
				ng-activated-on="addons.getCategories(prod)"
				ng-activated-off="prod.isOpen = false"
				class="sp-color-blue">
				<div class="slds-border--bottom">
					<div class="slds-grid slds-grid--vertical-align-center
							sp-text-bold">
						<div ng-style="{'white-space':'nowrap'}" 
							class="slds-p-horizontal--x-small
								slds-p-vertical--x-small
								slds-size--1-of-12"
							title="Favorite?">
							<span ng-click="preferences.toggleFavorite(prod)"
								class="slds-icon_container
									slds-m-right--xx-small">
								<svg ng-class="{
										'slds-icon-text-warning': 
											prod.isFavorite,
										'slds-icon-text-default': 
											!prod.isFavorite,
									}"
									title="Toggle: Favorite Product"
									class="slds-button__icon">
								<use xlink:href="{! 
									URLFOR($Resource.SalesforceLightningV222,
					'/assets/icons/utility-sprite/svg/symbols.svg#favorite') }"></use>
								</svg>
							</span>
							<span ng-click="preferences.toggleShowByDefault(prod)"
								class="slds-icon_container">
								<svg ng-class="{
										'slds-icon-text-warning': 
											prod.showByDefault,
										'slds-icon-text-default': 
											!prod.showByDefault,
									}"
									title="Toggle: Show in Last 90 Days"
									class="slds-button__icon">
								<use xlink:href="{! 
									URLFOR($Resource.SalesforceLightningV222,
					'/assets/icons/utility-sprite/svg/symbols.svg#preview') }"></use>
								</svg>
							</span>		
						</div>
						<div ng-bind="prod.sku" 
							class="slds-p-horizontal--x-small
								slds-p-vertical--x-small
								slds-size--2-of-12 slds-truncate"
							ng-attr-title="{{prod.sku}}"></div>
						<div ng-bind="prod.name"
							class="slds-p-horizontal--x-small
								slds-p-vertical--x-small
								slds-size--3-of-12 slds-truncate"
							ng-attr-title="{{prod.name}}"></div>
						<div ng-bind="prod.price | currency:'$':2" 
							class="slds-p-horizontal--x-small
								slds-p-vertical--x-small
								slds-text-align--right
								slds-size--2-of-12 slds-truncate" 
							ng-attr-title="{{prod.price | currency:'$':2}}"></div>
						<div ng-bind="prod.unitsPk"
							class="slds-p-horizontal--x-small
								slds-p-vertical--x-small
								slds-size--1-of-12 slds-truncate"
							ng-attr-title="{{prod.unitsPk}}"></div>
						<div class="slds-p-vertical--x-small
								slds-text-align--right
								slds-size--1-of-12 slds-truncate"
							ng-attr-title="{{prod.totalPieces}}">
							<input ng-model="prod.totalPieces" 
								ng-change="summarize()"
								ng-number="true"
								type="number"
								step="0.5"
								class="slds-input slds-p-horizontal--x-small"/>		
						</div>
						<div class="slds-p-horizontal--x-small
								slds-p-vertical--x-small
								slds-text-align--right
								slds-size--2-of-12 slds-truncate" 
							title="Action">
							<button ng-if="prod.supportAddons"
								ng-click="prod.isOpen = !prod.isOpen"
								type="button"
								class="slds-border--left
									slds-border--right
									slds-border--top
									slds-border--bottom
									slds-theme--default
									slds-p-vertical--xxx-small
									slds-p-horizontal--xx-small
									slds-m-right--xxx-small
									slds-text-title--caps">
								{! $Label.OrderBuilderCustomize }
							</button>
							<button ng-click="delProduct(prod, $index)"
								type="button"
								class="slds-button slds-button--icon">
								<svg aria-hidden="true" 
									title="Remove product from order"
									class="slds-button__icon">
									<use xlink:href="{!
										URLFOR($Resource.SalesforceLightningV222,
					'/assets/icons/utility-sprite/svg/symbols.svg#delete') }"></use>
								</svg>
							</button>	
						</div>
					</div>					
					<div ng-if="prod.supportAddons &&
							(navigation.currentSection.name != 'orders' ||
								navigation.currentSection.itemIndex != $index ||
									!prod.isOpen) &&
							!globals.helpers.isEmpty(prod.addons)"
						class="slds-grid">
						<div class="slds-size--1-of-12">&nbsp;</div>
						<div ng-style="{'top':-3}" 
							class="slds-size--11-of-12
								slds-p-horizontal--x-small
								slds-p-bottom--xx-small
								sp-color-gray slds-is-relative">
							<b ng-repeat-start="(id, a) in prod.addons">
								<span ng-bind="a.addonName"></span>
								(<span ng-bind="a.quantity"></span>
								&times; <span ng-bind="a.unitPrice | 
									currency:'$':2"></span>)
							</b>
							<span ng-repeat-end="true"
								ng-if="!$last">&#47;&nbsp;</span>
						</div>
					</div>
				</div>
				<div ng-if="prod.supportAddons &&
						navigation.currentSection.name == 'orders' &&
						navigation.currentSection.itemIndex == $index &&
						prod.isOpen"
					class="sp-bck-light-grayblue slds-p-around--small">
					<div class="slds-grid slds-m-bottom--x-small
							slds-grid--vertical-stretch">
						<div class="slds-size--4-of-12
								slds-text-title--caps
								slds-p-horizontal--x-small">
							{! $Label.OrderBuilderCategoriesHeader }
						</div>
						<div class="slds-size--4-of-12
								slds-text-title--caps
								slds-p-horizontal--x-small">
							{! $Label.OrderBuilderAddonsHeader }
						</div>
						<div class="slds-size--4-of-12
								slds-text-title--caps
								slds-p-horizontal--x-small">
							{! $Label.OrderBuilderSelectedHeader }
						</div>
					</div>					
					<div class="slds-grid slds-m-bottom--x-small">
						<div class="slds-size--4-of-12 
								slds-p-right--x-small">
							<div class="slds-theme--default sp-panel-height-120
									slds-scrollable--y slds-p-vertical--x-small
									sp-border-radius">
								<div ng-repeat="c in addons.categories">
									<button type="button"
										ng-click="addons.selectCategory(
											prod, $index)" 
										ng-class="{'slds-selected-row sp-text-bold':
											addons.selectedIndex == $index}"
										ng-attr-title="{{c.category}}"
										class="sp-no-border slds-text-title--caps
											slds-p-around--x-small slds-truncate
											slds-theme--default slds-size--3-of-3
											slds-text-align--left">
										<span ng-bind="c.category"></span>
										(<span ng-bind="c.addonCount"></span>)
									</button>
								</div>
							</div>
						</div>
						<div class="slds-size--4-of-12 
								slds-p-right--x-small">
							<div class="slds-theme--default sp-panel-height-120
									slds-scrollable--y slds-p-around--x-small
									sp-border-radius">
								<div ng-repeat="a in addons.addons"
									ng-if="!prod.addons[a.addonId]">
									<button ng-click="addons.add(prod, a)"
										ng-attr-title="{{a.name}} ({{a.price | 
											currency:'$':2}})"
										class="slds-badge sp-no-border
											slds-truncate slds-m-right--xx-small
											slds-m-bottom--xx-small">
										<span ng-bind="a.name"></span>
										(<span ng-bind="a.price | 
											currency:'$':2"></span>)
									</button>
								</div>
							</div>
						</div>
						<div class="slds-size--4-of-12">
							<div class="slds-theme--default sp-panel-height-120
									slds-scrollable--y slds-p-around--x-small
									sp-border-radius">
								<div ng-repeat="(id, a) in prod.addons">
									<button ng-class="{'slds-selected-row': 
											a.supportsMultiple}"
										class="slds-badge sp-no-border
											slds-truncate slds-m-right--xx-small
											slds-m-bottom--xx-small slds-p-right--large
											slds-is-relative">											
										<a ng-if="a.supportsMultiple"
											ng-click="addons.add(prod, a)"
											ng-attr-title="{{a.addonName}} ({{a.price | 
												currency:'$':2}})"
											class="slds-button--icon-inverse">
											<span ng-bind="a.addonName"></span>
											(<span ng-bind="a.quantity"></span>
											&times; <span ng-bind="a.unitPrice | 
												currency:'$':2"></span>)
										</a>

										<span ng-if="!a.supportsMultiple">
											<span ng-bind="a.addonName"></span>
											(<span ng-bind="a.quantity"></span> 
											&times; <span ng-bind="a.unitPrice | 
												currency:'$':2"></span>)
										</span>

										<a ng-click="addons.remove(prod, a)"
											class="slds-button sp-absolute
												sp-dock-top sp-dock-right
												slds-m-top--xxx-small
												slds-m-right--x-small
												slds-button--icon-inverse"
											type="button"
											title="Remove Add-On">
											<svg class="slds-button__icon">
											<use xlink:href="{!
										URLFOR($Resource.SalesforceLightningV222,
					'/assets/icons/utility-sprite/svg/symbols.svg#close') }"></use>
											</svg>
										</a>
									</button>	
								</div>
							</div>
						</div>
					</div>
					<div class="slds-form-element">
						<label class="slds-form-element__label
								slds-text-title--caps">Comments</label>
						<div class="slds-form-element__control">
							<textarea ng-model="prod.comments"
								ng-stop-propagation-on="keydown"
								class="slds-textarea"
								maxlength="255"></textarea>
						</div>
					</div>	
				</div>
			</div>
		</div>
		<div class="slds-grid slds-grid--align-end">
			<div class="slds-p-around--large slds-text-align--right">
				<p class="slds-text-title--caps slds-m-bottom--xx-small
						slds-line-height--reset">MIN REQUIRED</p>
				<h3 class="slds-text-heading--medium
						slds-line-height--reset
						slds-text-color--weak">
					<b ng-bind="(globals.account.Order_Minimum_Amount__c
							|| 0) | currency:'$':2"></b>
				</h3>
			</div>
			<div class="slds-p-around--large slds-text-align--right">
				<p class="slds-text-title--caps slds-m-bottom--xx-small
						slds-line-height--reset">DUE</p>
				<h3 class="slds-text-heading--medium
						slds-line-height--reset
						slds-text-color--weak">
					<b ng-bind="order.dueToMinimal | currency:'$':2"></b>
				</h3>
			</div>
			<div class="slds-p-around--large slds-text-align--right">
				<p class="slds-text-title--caps slds-m-bottom--xx-small
						slds-line-height--reset">TOTAL</p>
				<h1 class="slds-text-heading--large
						slds-line-height--reset">
					<b ng-bind="order.amount | currency:'$':2"></b>
				</h1>
			</div>
		</div>
		<div class="slds-p-around--large slds-text-align--right">
			<button ng-disabled="!order.productsAdded ||
					order.dueToMinimal || order.completed"
				ng-click="saveOrder()" 
				class="slds-button slds-button--brand 
					slds-p-horizontal--large slds-p-vertical--x-small 
					slds-m-top--large slds-text-heading--medium 
					sp-text-bold500">PLACE ORDER</button>
		</div>
	</div>
</div>
</apex:component>