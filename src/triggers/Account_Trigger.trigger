/**
* <p>
* Version log:
* <ul>
* <li>1.00 10/10/2017 Marcel Gomez: Initial version</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 10/10/2017
* @description 	 - Updates the next statement date of a list of Accounts
*/
 
trigger Account_Trigger on Account (before insert, before update) {

	if(TriggerControlClass.stopAccountTriggers)
		return;

	List<Account> changedAccounts = new List<Account>();
	for(Account account : Trigger.new) {
		Account oldAccount = Trigger.isUpdate ? Trigger.oldMap.get(account.Id) : NULL;

		if(Trigger.isInsert || account.Statement_Frequency__c != oldAccount.Statement_Frequency__c || account.Statement_Day__c != oldAccount.Statement_Day__c)
			changedAccounts.add(account);
	}

	StatementManager_Helper.updateNextStatementDate(changedAccounts, false);
}