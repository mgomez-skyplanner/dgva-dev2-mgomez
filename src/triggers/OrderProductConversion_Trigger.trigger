/**
 * @author    	 - Marcel Gomez
 * @date  	     - 06/08/2017
 * @description  - This trigger launches the sync process between an Order and a Sales Order object when an OrderItem changed if Order.Status = 'Activated'
*/
 
trigger OrderProductConversion_Trigger on OrderItem (after insert, after update, before delete) {
	
	system.debug('------------------------------------ trigger OrderProductConversion_Trigger');

	if(TriggerControlClass.stopOrderItemTriggers)
		return;
	
    Boolean executeTrigger = false;
	Trigger_Control_Setting__c setting = Trigger_Control_Setting__c.getInstance('OrderProductConversion_Trigger');

	if(setting != NULL) {
		executeTrigger = setting.Execute__c;
	}

	if(executeTrigger) {
		system.debug('------------------------------------ EXECUTING trigger OrderProductConversion_Trigger');

		List<Id> orderIds = new List<Id>();
		List<Id> salesOrderIds = new List<Id>();

		List<OrderItem> elements = Trigger.isDelete ? Trigger.old : Trigger.new;

		for(OrderItem orderItem : elements) {
			orderIds.add(orderItem.OrderId);
		}

		Map<Id, Order> orders = new Map<Id, Order>([SELECT 
														Id,
														Status,
														Sales_Order__c
													FROM 
														Order
													WHERE 
														Id IN: orderIds AND
														(Status = 'Activated' OR Status = 'Delivered')]);

		List<Order_Product_Conversion_Fields__c> fieldsList = Order_Product_Conversion_Fields__c.getall().values();

		for(OrderItem orderItem : elements) {

			OrderItem oldOrderItem = Trigger.isUpdate ? Trigger.oldMap.get(orderItem.Id) : NULL;

			if(orders.containsKey(orderItem.OrderId)) { //meanning that the Order of the Order Item is Activated!

				//sync Order Item and Sales Order Line only if one of the conversion fields (those in custom setting Order_Product_Conversion_Fields__c) changed in the last operation
				Boolean fieldChanged = Trigger.isInsert || OrderConversionUtility.OrderProduct_SalesOrderLine_SyncFieldsChanged(orderItem, oldOrderItem);

			  	if(fieldChanged) {
			  		Order currentOrder = orders.get(orderItem.OrderId);

			  		TriggerControlClass.stopOrderTriggers = true;
			  		TriggerControlClass.stopOrderItemTriggers = true;

			  		OrderConversionUtility.Sync_Order_SalesOrder(currentOrder);

			  		TriggerControlClass.stopOrderTriggers = false;
			  		TriggerControlClass.stopOrderItemTriggers = false;
			  	}
			}
		}
	}
}