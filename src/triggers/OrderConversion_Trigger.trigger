/**
* <p>
* Version log:
* <ul>
* <li>1.00 06/28/2017 Marcel Gomez: Initial version</li>
* <li>1.01 08/11/2017 Marcel Gomez: create the Billing when Order.Status = 'Activated' instead of 'Delivered'</li>
* </ul>
* <p>
* @author	     - Marcel Gomez
* @date  	     - 06/07/2017
* @description 	 - This trigger launches the sync process between an Order and a Sales Order object if Order.Status is 'Activated'
*/

trigger OrderConversion_Trigger on Order (after insert, after update) {
	
	system.debug('------------------------------------ trigger OrderConversion_Trigger');

	if(TriggerControlClass.stopOrderTriggers)
		return;

    Boolean executeTrigger = false;
	Trigger_Control_Setting__c setting = Trigger_Control_Setting__c.getInstance('OrderConversion_Trigger');

	if(setting != NULL) {
		executeTrigger = setting.Execute__c;
	}

	if(executeTrigger) {
		system.debug('------------------------------------ EXECUTING trigger OrderConversion_Trigger');

		List<Order_Conversion_Fields__c> fieldsList = Order_Conversion_Fields__c.getall().values();

		List<AcctSeedERP__Sales_Order__c> salesOrder2Cancel = new List<AcctSeedERP__Sales_Order__c>();

		for(Order order : Trigger.new) {

			Order oldOrder = Trigger.isUpdate ? Trigger.oldMap.get(order.Id) : NULL;

			//sync Order and Sales Order only if Order.Status is 'Activated'
			if(order.Status == 'Activated' || order.Status == 'Delivered') {
				system.debug('------------------------------------ ABOUT TO SYNC ORDER WITH SALES ORDER....');

				//sync Order and Sales Order only if one of the conversion fields (those in custom setting Order_Conversion_Fields__c) changed in the last operation
				Boolean fieldChanged = Trigger.isInsert || OrderConversionUtility.Order_SalesOrder_SyncFieldsChanged(order, oldOrder);

		  		TriggerControlClass.stopOrderTriggers = true;
		  		TriggerControlClass.stopOrderItemTriggers = true;

			  	if(fieldChanged) {
					system.debug('------------------------------------ ABOUT TO SYNC ORDER WITH SALES ORDER....');
					
			  		OrderConversionUtility.Sync_Order_SalesOrder(order);
			  	}

			  	if(order.Status == 'Delivered' && oldOrder != NULL && oldOrder.Status != order.Status) {
					system.debug('------------------------------------ ABOUT TO GENERATE BILLING....');

		  			BillingManager.GenerateBillingFromOrder(order, true);
			  	}

		  		TriggerControlClass.stopOrderTriggers = false;
		  		TriggerControlClass.stopOrderItemTriggers = false;
		  	}		  	
		  	//Cancel Sales Order if related Order was cancelled
		  	else if(order.Status == 'Cancelled' && oldOrder.Status != 'Cancelled' && order.Sales_Order__c != NULL) {

		  		//reload Sales Order
		  		AcctSeedERP__Sales_Order__c salesOrder = [SELECT
		  													Id,
		  													AcctSeedERP__Status__c
	  													FROM
	  														AcctSeedERP__Sales_Order__c
  														WHERE
  															Id =: order.Sales_Order__c];  															
		  		
		  		system.debug('---------------------------------- Sales Order:');
		  		system.debug(order.Sales_Order__c);
		  		system.debug('---------------------------------- Sales Order Status:');
		  		system.debug(order.Sales_Order__r.AcctSeedERP__Status__c);

		  		if(salesOrder.AcctSeedERP__Status__c != 'Cancelled') {
		  			salesOrder.AcctSeedERP__Status__c = 'Cancelled';
		  			salesOrder2Cancel.add(salesOrder);
		  		}
		  	}
		  	//Generate and post a Billing when related Order was delivered
		  	else if(order.Status == 'Delivered' && oldOrder.Status != 'Delivered') {
		  		OrderConversionUtility.LockOrder(order);
		  	}
		  	
		  	//Delete existing Billings when the Order is deactivated		  	
		  	if(order.StatusCode == 'D' && oldOrder != NULL && oldOrder.StatusCode != 'D') {
		  		OrderConversionUtility.deleteSalesOrder(order);
		  		BillingManager.deleteOrderBilling(order);
		  	}
		}

		//update cancelled Sales Orders
		if(salesOrder2Cancel.size() > 0) {
			try {
				update salesOrder2Cancel;

				system.debug('------------------------------------ Sales Order cancelled: ' + salesOrder2Cancel.size());
			}
			catch (Exception e) {
				system.debug('-----------------**********--------------- Error attempting to CANCEL Sales Orders!!!: ' + e.getMessage());
			}
		}
	}
}