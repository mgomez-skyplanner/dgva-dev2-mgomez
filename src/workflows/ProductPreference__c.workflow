<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>SetUniquenessIndicator</fullName>
        <field>UniquenessIndicator__c</field>
        <formula>Account__c &amp; &apos;:&apos; &amp; Product__c</formula>
        <name>Set Uniqueness Indicator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Uniqueness Indicator</fullName>
        <actions>
            <name>SetUniquenessIndicator</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR (   ISNEW(),   ISCHANGED( Account__c ),   ISCHANGED( Product__c ),   ISCHANGED( UniquenessIndicator__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
