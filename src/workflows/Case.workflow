<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Delayed_Route_Notification</fullName>
        <description>Delayed Route Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Customer_Service</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Notifications/Delayed_Route_Notification</template>
    </alerts>
    <tasks>
        <fullName>URGENT_Delayed_Route</fullName>
        <assignedTo>admin@protanosbakery.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>URGENT - Delayed Route</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>URGENT - Delayed Route</subject>
    </tasks>
</Workflow>
