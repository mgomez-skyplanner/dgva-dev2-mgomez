<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>SendOrderConfirmation</fullName>
        <description>Send Order Confirmation</description>
        <protected>false</protected>
        <recipients>
            <field>SendConfirmationEmailTo__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>orders@protanosbakery.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Orders/OrderConfirmation</template>
    </alerts>
    <alerts>
        <fullName>SendOrderConfirmationPlatinum</fullName>
        <description>Send Order Confirmation (Platinum Accounts)</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>orders@protanosbakery.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Orders/OrderConfirmationPlatinum</template>
    </alerts>
    <fieldUpdates>
        <fullName>ActivateOrder</fullName>
        <field>Status</field>
        <literalValue>Activated</literalValue>
        <name>Activate Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CopySendConfirmationEmail</fullName>
        <description>Copies the email from account to order (Send Confirmation Email To)</description>
        <field>SendConfirmationEmailTo__c</field>
        <formula>Account.SendConfirmationEmailTo__c</formula>
        <name>Copy Send Confirmation Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
