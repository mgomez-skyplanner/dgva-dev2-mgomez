<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_Activity_Statement</fullName>
        <description>Send Activity Statement</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Billing_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>accounting@protanosbakery.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Orders/Activity_Statement</template>
    </alerts>
    <alerts>
        <fullName>Send_Outstanding_Statement</fullName>
        <description>Send Outstanding Statement</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Billing_Contact__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>accounting@protanosbakery.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Orders/Outstanding_Statement</template>
    </alerts>
    <rules>
        <fullName>Send Statements via Email - Activity</fullName>
        <actions>
            <name>Send_Activity_Statement</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Statement__c.Resend__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Statement__c.Type__c</field>
            <operation>equals</operation>
            <value>Activity</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Statements via Email - Outstanding</fullName>
        <actions>
            <name>Send_Outstanding_Statement</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Statement__c.Resend__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Statement__c.Type__c</field>
            <operation>equals</operation>
            <value>Outstanding</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
