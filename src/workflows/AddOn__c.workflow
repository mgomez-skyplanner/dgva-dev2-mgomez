<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Uniqueness_Indicator</fullName>
        <field>UniqueId__c</field>
        <formula>IF(
 ISBLANK( Product__c ),
 Id,
 Product__c
)</formula>
        <name>Set Uniqueness Indicator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Uniqueness Indicator</fullName>
        <actions>
            <name>Set_Uniqueness_Indicator</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
