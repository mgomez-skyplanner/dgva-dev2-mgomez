({
	calculateBalances: function(component, event, helper) {
		$A.util.removeClass(component.find("cmp-spinner"), "hide");

        var action = component.get("c.UpdateAccountBalances");
        action.setParams({"accountId": component.get("v.accountId")});
        
        action.setCallback(this, function(response) {
        	var toastType = 'success';
            var resultMsg = 'Process done!';
            
            var state = response.getState();
            var result = response.getReturnValue();
            
            if (state === "ERROR") {
                var errors = response.getError();
                if (errors && errors[0] && errors[0].message) {
                    resultMsg = "Error message: " + errors[0].message;
                }
                else {
                    resultMsg = "Unknown error";
                }

                toastType = 'error';
            }
            else if (state === "SUCCESS") {
            	if(result != '1') {
                	toastType = 'error';
                	resultMsg = result;
            	}
            }
            
            // Display a "toast" status message
            var resultsToast = $A.get("e.force:showToast");
            resultsToast.setParams({
                message: resultMsg,
                type: toastType
            });
            resultsToast.fire();
            
            $A.util.addClass(component.find("cmp-spinner"), "hide");
            $A.get("e.force:closeQuickAction").fire();
        });       
        
        $A.enqueueAction(action);
	}
})