<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <brand>
        <headerColor>#0070D2</headerColor>
    </brand>
    <description>Manage your sales process with accounts, leads, opportunities, and more</description>
    <formFactors>Large</formFactors>
    <label>Sales</label>
    <navType>Standard</navType>
    <tab>standard-Task</tab>
    <tab>standard-Lead</tab>
    <tab>standard-Account</tab>
    <tab>standard-Order</tab>
    <tab>Order_Builder</tab>
    <tab>standard-Product2</tab>
    <tab>Route__c</tab>
    <tab>standard-Case</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Event</tab>
    <tab>Order_Description__c</tab>
    <tab>AddOn__c</tab>
    <uiType>Lightning</uiType>
</CustomApplication>
